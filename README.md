# Deltawatch - Unreal Engine

Created and designed a level for a shooting game called Deltawatch where the player plays as a covert op 
with the goal of eliminating all enemies at an Antarctic base.

The game has music and SFX as well as enemy AI that have detection, investigation, and shooting mechanics.

Game restarts after 5 seconds of winning or losing. (Win by eliminating all enemies)

### Deltawatch Gameplay

![](images/Deltawatch.gif)

## How to install?
1. Download the Build/Windows directory 
2. Extract contents into a folder
3. Open up the Build/Windows/DeltaWatch.exe

## How to play?
Move forward, backwards, left, and right with W (forward),  S (backwards), A (left) and D (right) keys.

Use SpaceBar to jump

Aim with the mouse and shoot with RMB
