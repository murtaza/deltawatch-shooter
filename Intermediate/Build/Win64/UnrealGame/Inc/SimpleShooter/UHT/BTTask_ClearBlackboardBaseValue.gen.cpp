// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SimpleShooter/BTTask_ClearBlackboardBaseValue.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBTTask_ClearBlackboardBaseValue() {}
// Cross Module References
	AIMODULE_API UClass* Z_Construct_UClass_UBTTask_BlackboardBase();
	SIMPLESHOOTER_API UClass* Z_Construct_UClass_UBTTask_ClearBlackboardBaseValue();
	SIMPLESHOOTER_API UClass* Z_Construct_UClass_UBTTask_ClearBlackboardBaseValue_NoRegister();
	UPackage* Z_Construct_UPackage__Script_SimpleShooter();
// End Cross Module References
	void UBTTask_ClearBlackboardBaseValue::StaticRegisterNativesUBTTask_ClearBlackboardBaseValue()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UBTTask_ClearBlackboardBaseValue);
	UClass* Z_Construct_UClass_UBTTask_ClearBlackboardBaseValue_NoRegister()
	{
		return UBTTask_ClearBlackboardBaseValue::StaticClass();
	}
	struct Z_Construct_UClass_UBTTask_ClearBlackboardBaseValue_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBTTask_ClearBlackboardBaseValue_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBTTask_BlackboardBase,
		(UObject* (*)())Z_Construct_UPackage__Script_SimpleShooter,
	};
	static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_UBTTask_ClearBlackboardBaseValue_Statics::DependentSingletons) < 16);
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBTTask_ClearBlackboardBaseValue_Statics::Class_MetaDataParams[] = {
#if !UE_BUILD_SHIPPING
		{ "Comment", "/**\n * \n */" },
#endif
		{ "IncludePath", "BTTask_ClearBlackboardBaseValue.h" },
		{ "ModuleRelativePath", "BTTask_ClearBlackboardBaseValue.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBTTask_ClearBlackboardBaseValue_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBTTask_ClearBlackboardBaseValue>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UBTTask_ClearBlackboardBaseValue_Statics::ClassParams = {
		&UBTTask_ClearBlackboardBaseValue::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_UBTTask_ClearBlackboardBaseValue_Statics::Class_MetaDataParams), Z_Construct_UClass_UBTTask_ClearBlackboardBaseValue_Statics::Class_MetaDataParams)
	};
	UClass* Z_Construct_UClass_UBTTask_ClearBlackboardBaseValue()
	{
		if (!Z_Registration_Info_UClass_UBTTask_ClearBlackboardBaseValue.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UBTTask_ClearBlackboardBaseValue.OuterSingleton, Z_Construct_UClass_UBTTask_ClearBlackboardBaseValue_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UBTTask_ClearBlackboardBaseValue.OuterSingleton;
	}
	template<> SIMPLESHOOTER_API UClass* StaticClass<UBTTask_ClearBlackboardBaseValue>()
	{
		return UBTTask_ClearBlackboardBaseValue::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBTTask_ClearBlackboardBaseValue);
	UBTTask_ClearBlackboardBaseValue::~UBTTask_ClearBlackboardBaseValue() {}
	struct Z_CompiledInDeferFile_FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UBTTask_ClearBlackboardBaseValue, UBTTask_ClearBlackboardBaseValue::StaticClass, TEXT("UBTTask_ClearBlackboardBaseValue"), &Z_Registration_Info_UClass_UBTTask_ClearBlackboardBaseValue, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UBTTask_ClearBlackboardBaseValue), 2779493168U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_1295898266(TEXT("/Script/SimpleShooter"),
		Z_CompiledInDeferFile_FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
