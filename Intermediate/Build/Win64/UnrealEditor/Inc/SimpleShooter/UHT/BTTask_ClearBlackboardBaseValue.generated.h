// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "BTTask_ClearBlackboardBaseValue.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SIMPLESHOOTER_BTTask_ClearBlackboardBaseValue_generated_h
#error "BTTask_ClearBlackboardBaseValue.generated.h already included, missing '#pragma once' in BTTask_ClearBlackboardBaseValue.h"
#endif
#define SIMPLESHOOTER_BTTask_ClearBlackboardBaseValue_generated_h

#define FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_15_SPARSE_DATA
#define FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_15_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_15_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_15_ACCESSORS
#define FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBTTask_ClearBlackboardBaseValue(); \
	friend struct Z_Construct_UClass_UBTTask_ClearBlackboardBaseValue_Statics; \
public: \
	DECLARE_CLASS(UBTTask_ClearBlackboardBaseValue, UBTTask_BlackboardBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SimpleShooter"), NO_API) \
	DECLARE_SERIALIZER(UBTTask_ClearBlackboardBaseValue)


#define FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTask_ClearBlackboardBaseValue(UBTTask_ClearBlackboardBaseValue&&); \
	NO_API UBTTask_ClearBlackboardBaseValue(const UBTTask_ClearBlackboardBaseValue&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTask_ClearBlackboardBaseValue); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTask_ClearBlackboardBaseValue); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBTTask_ClearBlackboardBaseValue) \
	NO_API virtual ~UBTTask_ClearBlackboardBaseValue();


#define FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_12_PROLOG
#define FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_15_SPARSE_DATA \
	FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_15_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_15_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_15_ACCESSORS \
	FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_15_INCLASS_NO_PURE_DECLS \
	FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SIMPLESHOOTER_API UClass* StaticClass<class UBTTask_ClearBlackboardBaseValue>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_UnrealEngine_SimpleShooter_Source_SimpleShooter_BTTask_ClearBlackboardBaseValue_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
