#pragma warning(disable : 3571) // pow() intrinsic suggested to be used with abs()
static uint _341 = 0u;
static float3 _367 = 0.0f.xxx;
static float4 _368 = 0.0f.xxxx;
static float4x4 _369 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
static bool4 _370 = bool4(false, false, false, false);
static float _372 = 0.0f;
static float4 _373 = 0.0f.xxxx;
static float2 _374 = 0.0f.xx;

cbuffer View
{
    row_major float4x4 View_View_TranslatedWorldToClip : packoffset(c0);
    row_major float4x4 View_View_RelativeWorldToClip : packoffset(c4);
    row_major float4x4 View_View_TranslatedWorldToView : packoffset(c12);
    row_major float4x4 View_View_ViewToClip : packoffset(c28);
    row_major float4x4 View_View_SVPositionToTranslatedWorld : packoffset(c44);
    float3 View_View_ViewTilePosition : packoffset(c60);
    float3 View_View_MatrixTilePosition : packoffset(c61);
    float3 View_View_ViewForward : packoffset(c62);
    float4 View_View_InvDeviceZToWorldZTransform : packoffset(c67);
    float4 View_View_ScreenPositionScaleBias : packoffset(c68);
    float3 View_View_RelativeWorldCameraOrigin : packoffset(c69);
    float3 View_View_TranslatedWorldCameraOrigin : packoffset(c70);
    float3 View_View_RelativePreViewTranslation : packoffset(c72);
    row_major float4x4 View_View_ClipToPrevClip : packoffset(c113);
    float4 View_View_ViewRectMin : packoffset(c124);
    float4 View_View_ViewSizeAndInvSize : packoffset(c125);
    float4 View_View_LightProbeSizeRatioAndInvSizeRatio : packoffset(c127);
    float View_View_PreExposure : packoffset(c132.z);
    float View_View_OneOverPreExposure : packoffset(c132.w);
    float4 View_View_DiffuseOverrideParameter : packoffset(c133);
    float4 View_View_SpecularOverrideParameter : packoffset(c134);
    float4 View_View_NormalOverrideParameter : packoffset(c135);
    float2 View_View_RoughnessOverrideParameter : packoffset(c136);
    float View_View_OutOfBoundsMask : packoffset(c137);
    float View_View_CullingSign : packoffset(c138.w);
    float View_View_MaterialTextureMipBias : packoffset(c140);
    uint View_View_StateFrameIndexMod8 : packoffset(c141.y);
    float View_View_CameraCut : packoffset(c142.y);
    float View_View_UnlitViewmodeMask : packoffset(c142.z);
    float3 View_View_PrecomputedIndirectLightingColorScale : packoffset(c155);
    float3 View_View_PrecomputedIndirectSpecularColorScale : packoffset(c156);
    float View_View_RenderingReflectionCaptureMask : packoffset(c179.w);
    float View_View_SkyLightApplyPrecomputedBentNormalShadowingFlag : packoffset(c182.y);
    float4 View_View_SkyLightColor : packoffset(c183);
    float View_View_ReflectionCubemapMaxMip : packoffset(c192.z);
    float3 View_View_ReflectionEnvironmentRoughnessMixingScaleBiasAndLargestWeight : packoffset(c194);
    int View_View_StereoPassIndex : packoffset(c194.w);
    float3 View_View_VolumetricFogInvGridSize : packoffset(c225);
    float3 View_View_VolumetricFogGridZParams : packoffset(c226);
    float2 View_View_VolumetricFogScreenToResourceUV : packoffset(c229);
    float2 View_View_VolumetricFogUVMax : packoffset(c229.z);
    float View_View_MinRoughness : packoffset(c243.z);
};

StructuredBuffer<float4> View_SkyIrradianceEnvironmentMap;
StructuredBuffer<float4> Scene_GPUScene_GPUScenePrimitiveSceneData;
StructuredBuffer<float4> Scene_GPUScene_GPUSceneLightmapData;
cbuffer TranslucentBasePass
{
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumLocalLights : packoffset(c0);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumReflectionCaptures : packoffset(c0.y);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_HasDirectionalLight : packoffset(c0.z);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumGridCells : packoffset(c0.w);
    int3 TranslucentBasePass_TranslucentBasePass_Shared_Forward_CulledGridSize : packoffset(c1);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridPixelSizeShift : packoffset(c2);
    float3 TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridZParams : packoffset(c3);
    float3 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDirection : packoffset(c4);
    float TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius : packoffset(c4.w);
    float3 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightColor : packoffset(c5);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowMapChannelMask : packoffset(c6);
    float2 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDistanceFadeMAD : packoffset(c6.z);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumDirectionalLightCascades : packoffset(c7);
    int TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightVSM : packoffset(c7.y);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_Forward_CascadeEndDepths : packoffset(c8);
    row_major float4x4 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightTranslatedWorldToShadowMatrix[4] : packoffset(c9);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapMinMax[4] : packoffset(c25);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapAtlasBufferSize : packoffset(c29);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightUseStaticShadowing : packoffset(c30.y);
    row_major float4x4 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightTranslatedWorldToStaticShadow : packoffset(c33);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectLightingShowFlag : packoffset(c37);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_Reflection_SkyLightParameters : packoffset(c90);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ReflectionPlane : packoffset(c95);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionOrigin : packoffset(c96);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionXAxis : packoffset(c97);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionYAxis : packoffset(c98);
    row_major float3x4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_InverseTransposeMirrorMatrix : packoffset(c99);
    float3 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters : packoffset(c102);
    float2 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters2 : packoffset(c103);
    row_major float4x4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ProjectionWithExtraFOV[2] : packoffset(c104);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenScaleBias[2] : packoffset(c112);
    float2 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenBound : packoffset(c114);
    uint TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_bIsStereo : packoffset(c114.z);
    float TranslucentBasePass_TranslucentBasePass_Shared_Fog_ApplyVolumetricFog : packoffset(c124.w);
    float TranslucentBasePass_TranslucentBasePass_Shared_Fog_VolumetricFogStartDistance : packoffset(c125);
    uint TranslucentBasePass_TranslucentBasePass_Shared_UseBasePassSkylight : packoffset(c140);
    float4 TranslucentBasePass_TranslucentBasePass_HZBUvFactorAndInvFactor : packoffset(c161);
    float4 TranslucentBasePass_TranslucentBasePass_PrevScreenPositionScaleBias : packoffset(c162);
    float2 TranslucentBasePass_TranslucentBasePass_PrevSceneColorBilinearUVMin : packoffset(c163);
    float2 TranslucentBasePass_TranslucentBasePass_PrevSceneColorBilinearUVMax : packoffset(c163.z);
    float TranslucentBasePass_TranslucentBasePass_PrevSceneColorPreExposureInv : packoffset(c164);
    int TranslucentBasePass_TranslucentBasePass_SSRQuality : packoffset(c164.y);
    float TranslucentBasePass_TranslucentBasePass_ReprojectionRadiusScale : packoffset(c171);
    float TranslucentBasePass_TranslucentBasePass_InvClipmapFadeSize : packoffset(c171.w);
    uint TranslucentBasePass_TranslucentBasePass_RadianceProbeClipmapResolution : packoffset(c172.z);
    uint TranslucentBasePass_TranslucentBasePass_NumRadianceProbeClipmaps : packoffset(c172.w);
    uint TranslucentBasePass_TranslucentBasePass_RadianceProbeResolution : packoffset(c173);
    uint TranslucentBasePass_TranslucentBasePass_FinalProbeResolution : packoffset(c173.y);
    uint TranslucentBasePass_TranslucentBasePass_FinalRadianceAtlasMaxMip : packoffset(c173.z);
    float4 TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[6] : packoffset(c178);
    float4 TranslucentBasePass_TranslucentBasePass_PaddedWorldPositionToRadianceProbeCoordBias[6] : packoffset(c184);
    float4 TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[6] : packoffset(c190);
    float2 TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution : packoffset(c196);
    uint TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask : packoffset(c198);
    uint TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionDivideShift : packoffset(c198.y);
    uint TranslucentBasePass_TranslucentBasePass_Enabled : packoffset(c200.z);
    float TranslucentBasePass_TranslucentBasePass_RelativeDepthThreshold : packoffset(c200.w);
    float TranslucentBasePass_TranslucentBasePass_SpecularScale : packoffset(c201);
    float TranslucentBasePass_TranslucentBasePass_Contrast : packoffset(c201.y);
    float3 TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridZParams : packoffset(c205);
    int3 TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridSize : packoffset(c206);
};

StructuredBuffer<float4> TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer;
StructuredBuffer<uint> TranslucentBasePass_Shared_Forward_NumCulledLightsGrid;
StructuredBuffer<float4> TranslucentBasePass_ProbeWorldOffset;
cbuffer ReflectionCaptureSM5
{
    float4 ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[341] : packoffset(c0);
    float4 ReflectionCaptureSM5_ReflectionCaptureSM5_TilePosition[341] : packoffset(c341);
    float4 ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureProperties[341] : packoffset(c682);
    float4 ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureOffsetAndAverageBrightness[341] : packoffset(c1023);
    row_major float4x4 ReflectionCaptureSM5_ReflectionCaptureSM5_BoxTransform[341] : packoffset(c1364);
    float4 ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[341] : packoffset(c2728);
};

ByteAddressBuffer VirtualShadowMap_ProjectionData;
StructuredBuffer<uint> VirtualShadowMap_PageTable;
cbuffer Material
{
    float4 Material_Material_PreshaderBuffer[28] : packoffset(c0);
};

SamplerState View_SharedPointClampedSampler;
SamplerState View_SharedBilinearClampedSampler;
Texture2D<float4> TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapAtlas;
SamplerState TranslucentBasePass_Shared_Forward_ShadowmapSampler;
Texture2D<float4> TranslucentBasePass_Shared_Forward_DirectionalLightStaticShadowmap;
SamplerState TranslucentBasePass_Shared_Forward_StaticShadowmapSampler;
Buffer<uint4> TranslucentBasePass_Shared_Forward_CulledLightDataGrid16Bit;
TextureCube<float4> TranslucentBasePass_Shared_Reflection_SkyLightCubemap;
SamplerState TranslucentBasePass_Shared_Reflection_SkyLightCubemapSampler;
TextureCubeArray<float4> TranslucentBasePass_Shared_Reflection_ReflectionCubemap;
SamplerState TranslucentBasePass_Shared_Reflection_ReflectionCubemapSampler;
Texture2D<float4> TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionTexture;
Texture3D<float4> TranslucentBasePass_Shared_Fog_IntegratedLightScattering;
Texture3D<uint4> TranslucentBasePass_RadianceProbeIndirectionTexture;
Texture2D<float4> TranslucentBasePass_RadianceCacheFinalRadianceAtlas;
Texture2D<float4> TranslucentBasePass_Radiance;
Texture2D<float4> TranslucentBasePass_SceneDepth;
Texture3D<float4> TranslucentBasePass_TranslucencyGIVolumeHistory0;
Texture3D<float4> TranslucentBasePass_TranslucencyGIVolumeHistory1;
SamplerState TranslucentBasePass_TranslucencyGIVolumeSampler;
Texture2D<float4> TranslucentBasePass_HZBTexture;
SamplerState TranslucentBasePass_HZBSampler;
Texture2D<float4> TranslucentBasePass_PrevSceneColor;
SamplerState TranslucentBasePass_PrevSceneColorSampler;
Texture2D<float4> LightmapResourceCluster_LightMapTexture;
Texture2D<float4> LightmapResourceCluster_SkyOcclusionTexture;
SamplerState LightmapResourceCluster_LightMapSampler;
Texture2DArray<uint4> VirtualShadowMap_PhysicalPagePool;
Texture2D<float4> Material_Texture2D_0;
SamplerState Material_Texture2D_0Sampler;
Texture2D<float4> Material_Texture2D_1;
SamplerState Material_Texture2D_1Sampler;
Texture2D<float4> Material_Texture2D_2;
SamplerState Material_Texture2D_2Sampler;
Texture2D<float4> Material_Texture2D_3;
SamplerState Material_Texture2D_3Sampler;
Texture2D<float4> Material_Texture2D_4;
SamplerState Material_Texture2D_4Sampler;
Texture2D<float4> Material_Texture2D_5;
SamplerState Material_Texture2D_5Sampler;
Texture2D<float4> Material_Texture2D_6;
SamplerState Material_Texture2D_6Sampler;

static float4 gl_FragCoord;
static bool gl_FrontFacing;
static float4 in_var_TEXCOORD10_centroid;
static float4 in_var_TEXCOORD11_centroid;
static float4 in_var_TEXCOORD0[1];
static float4 in_var_TEXCOORD4;
static uint in_var_PRIMITIVE_ID;
static uint in_var_LIGHTMAP_ID;
static float4 in_var_TEXCOORD7;
static float3 in_var_TEXCOORD9;
static float4 out_var_SV_Target0;

struct SPIRV_Cross_Input
{
    float4 in_var_TEXCOORD10_centroid : TEXCOORD10_centroid;
    float4 in_var_TEXCOORD11_centroid : TEXCOORD11_centroid;
    float4 in_var_TEXCOORD0[1] : TEXCOORD0;
    float4 in_var_TEXCOORD4 : TEXCOORD4;
    nointerpolation uint in_var_PRIMITIVE_ID : PRIMITIVE_ID;
    nointerpolation uint in_var_LIGHTMAP_ID : LIGHTMAP_ID;
    float4 in_var_TEXCOORD7 : TEXCOORD7;
    float3 in_var_TEXCOORD9 : TEXCOORD9;
    float4 gl_FragCoord : SV_Position;
    bool gl_FrontFacing : SV_IsFrontFace;
};

struct SPIRV_Cross_Output
{
    float4 out_var_SV_Target0 : SV_Target0;
};

uint spvPackHalf2x16(float2 value)
{
    uint2 Packed = f32tof16(value);
    return Packed.x | (Packed.y << 16);
}

float2 spvUnpackHalf2x16(uint value)
{
    return f16tof32(uint2(value & 0xffff, value >> 16));
}

void frag_main()
{
    float _424 = 1.0f / gl_FragCoord.w;
    float3 _462 = -View_View_MatrixTilePosition;
    float3 _463 = -View_View_ViewTilePosition;
    float3x3 _472 = float3x3(in_var_TEXCOORD10_centroid.xyz, cross(in_var_TEXCOORD11_centroid.xyz, in_var_TEXCOORD10_centroid.xyz) * in_var_TEXCOORD11_centroid.w, in_var_TEXCOORD11_centroid.xyz);
    float2 _475 = gl_FragCoord.xy - View_View_ViewRectMin.xy;
    float4 _482 = float4(mad(_475, View_View_ViewSizeAndInvSize.zw, (-0.5f).xx) * float2(2.0f, -2.0f), _372, 1.0f) * _424;
    float4 _487 = mul(float4(gl_FragCoord.xyz, 1.0f), View_View_SVPositionToTranslatedWorld);
    float3 _491 = _487.xyz / _487.w.xxx;
    float3 _492 = _491 - View_View_RelativePreViewTranslation;
    float _494 = _482.w;
    float2 _499 = mad(_482.xy / _494.xx, View_View_ScreenPositionScaleBias.xy, View_View_ScreenPositionScaleBias.wz);
    float3 _508 = 0.0f.xxx;
    if (View_View_ViewToClip[3].w >= 1.0f)
    {
        _508 = -View_View_ViewForward;
    }
    else
    {
        _508 = normalize(-_491);
    }
    uint _509 = in_var_PRIMITIVE_ID * 41u;
    float4 _526 = Material_Texture2D_0.SampleBias(Material_Texture2D_0Sampler, float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y), View_View_MaterialTextureMipBias);
    float _527 = _526.x;
    float2 _533 = mul(_472, _508).xy;
    float2 _535 = mad(_533, mad(0.0500000007450580596923828125f, _527 * Material_Material_PreshaderBuffer[1].x, -0.02500000037252902984619140625f).xx, float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y));
    float2 _541 = mad(Material_Texture2D_1.SampleBias(Material_Texture2D_1Sampler, _535, View_View_MaterialTextureMipBias).xy, 2.0f.xx, (-1.0f).xx);
    float2 _552 = float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y) * Material_Material_PreshaderBuffer[1].y.xx;
    float2 _555 = Material_Material_PreshaderBuffer[1].z.xx;
    float2 _562 = mad(Material_Texture2D_2.SampleBias(Material_Texture2D_2Sampler, _552 * _555, View_View_MaterialTextureMipBias).xy, 2.0f.xx, (-1.0f).xx);
    float2 _575 = mad(Material_Texture2D_2.SampleBias(Material_Texture2D_2Sampler, (_552 * 1.618000030517578125f.xx) * _555, View_View_MaterialTextureMipBias).xy, 2.0f.xx, (-1.0f).xx);
    float3 _602 = normalize(mul(normalize((lerp(float4(_541, sqrt(clamp(1.0f - dot(_541, _541), 0.0f, 1.0f)), 1.0f).xyz, (float4(_562, sqrt(clamp(1.0f - dot(_562, _562), 0.0f, 1.0f)), 1.0f).xyz + float4(_575, sqrt(clamp(1.0f - dot(_575, _575), 0.0f, 1.0f)), 1.0f).xyz) * float3(1.0f, 1.0f, 0.5f), Material_Texture2D_3.SampleBias(Material_Texture2D_3Sampler, float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y), View_View_MaterialTextureMipBias).x.xxx) * View_View_NormalOverrideParameter.w) + View_View_NormalOverrideParameter.xyz), _472)) * ((View_View_CullingSign * (((asuint(Scene_GPUScene_GPUScenePrimitiveSceneData[_509].x) & 64u) != 0u) ? (-1.0f) : 1.0f)) * float(gl_FrontFacing ? 1 : (-1)));
    float3 _630 = Material_Texture2D_5.SampleBias(Material_Texture2D_5Sampler, _535, View_View_MaterialTextureMipBias).xyz * Material_Material_PreshaderBuffer[4].w.xxx;
    float _633 = _630.x;
    float _637 = _630.y;
    float _641 = _630.z;
    float3 _645 = float3((_633 <= 0.0f) ? 0.0f : pow(_633, Material_Material_PreshaderBuffer[5].x), (_637 <= 0.0f) ? 0.0f : pow(_637, Material_Material_PreshaderBuffer[5].x), (_641 <= 0.0f) ? 0.0f : pow(_641, Material_Material_PreshaderBuffer[5].x));
    float2 _695 = float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y) * Material_Material_PreshaderBuffer[13].w.xx;
    float2 _701 = mad(_533, mad(0.0500000007450580596923828125f, _527 * Material_Material_PreshaderBuffer[14].x, -0.02500000037252902984619140625f).xx, _695);
    float3 _718 = Material_Texture2D_5.SampleBias(Material_Texture2D_5Sampler, _701, View_View_MaterialTextureMipBias).xyz * Material_Material_PreshaderBuffer[16].w.xxx;
    float _721 = _718.x;
    float _725 = _718.y;
    float _729 = _718.z;
    float3 _733 = float3((_721 <= 0.0f) ? 0.0f : pow(_721, Material_Material_PreshaderBuffer[17].x), (_725 <= 0.0f) ? 0.0f : pow(_725, Material_Material_PreshaderBuffer[17].x), (_729 <= 0.0f) ? 0.0f : pow(_729, Material_Material_PreshaderBuffer[17].x));
    float3 _794 = clamp(lerp(mad(Material_Texture2D_4.SampleBias(Material_Texture2D_4Sampler, _535, View_View_MaterialTextureMipBias).xyz * Material_Material_PreshaderBuffer[2].w.xxx, Material_Material_PreshaderBuffer[4].xyz, lerp(_645, dot(_645, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f)).xxx, Material_Material_PreshaderBuffer[5].y.xxx) * Material_Material_PreshaderBuffer[7].xyz) + mad(Material_Material_PreshaderBuffer[10].xyz, (Material_Texture2D_6.SampleBias(Material_Texture2D_6Sampler, mad(_533, mad(0.0500000007450580596923828125f, Material_Material_PreshaderBuffer[9].x, -0.02500000037252902984619140625f).xx, float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y)), View_View_MaterialTextureMipBias).y * Material_Material_PreshaderBuffer[9].y).xxx, Material_Material_PreshaderBuffer[13].xyz * (Material_Texture2D_6.SampleBias(Material_Texture2D_6Sampler, mad(_533, mad(0.0500000007450580596923828125f, Material_Material_PreshaderBuffer[12].x, -0.02500000037252902984619140625f).xx, float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y)), View_View_MaterialTextureMipBias).x * Material_Material_PreshaderBuffer[12].y).xxx), mad(Material_Texture2D_4.SampleBias(Material_Texture2D_4Sampler, _701, View_View_MaterialTextureMipBias).xyz * Material_Material_PreshaderBuffer[14].y.xxx, Material_Material_PreshaderBuffer[16].xyz, lerp(_733, dot(_733, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f)).xxx, Material_Material_PreshaderBuffer[17].y.xxx) * Material_Material_PreshaderBuffer[19].xyz) + mad(Material_Material_PreshaderBuffer[22].xyz, (Material_Texture2D_6.SampleBias(Material_Texture2D_6Sampler, mad(_533, mad(0.0500000007450580596923828125f, Material_Material_PreshaderBuffer[21].x, -0.02500000037252902984619140625f).xx, _695), View_View_MaterialTextureMipBias).y * Material_Material_PreshaderBuffer[21].y).xxx, Material_Material_PreshaderBuffer[25].xyz * (Material_Texture2D_6.SampleBias(Material_Texture2D_6Sampler, mad(_533, mad(0.0500000007450580596923828125f, Material_Material_PreshaderBuffer[24].x, -0.02500000037252902984619140625f).xx, _695), View_View_MaterialTextureMipBias).x * Material_Material_PreshaderBuffer[24].y).xxx), (length(((View_View_ViewTilePosition - View_View_ViewTilePosition) * 2097152.0f) + (_492 - View_View_RelativeWorldCameraOrigin)) * Material_Material_PreshaderBuffer[26].x).xxx), 0.0f.xxx, 1.0f.xxx);
    float _795 = clamp(Material_Material_PreshaderBuffer[26].y, 0.0f, 1.0f);
    float _800 = mad(clamp(Material_Material_PreshaderBuffer[26].w, 0.0f, 1.0f), View_View_RoughnessOverrideParameter.y, View_View_RoughnessOverrideParameter.x);
    float3 _813 = ((_794 - (_794 * _795)) * View_View_DiffuseOverrideParameter.w) + View_View_DiffuseOverrideParameter.xyz;
    float3 _820 = (lerp((0.07999999821186065673828125f * clamp(Material_Material_PreshaderBuffer[26].z, 0.0f, 1.0f)).xxx, _794, _795.xxx) * View_View_SpecularOverrideParameter.w) + View_View_SpecularOverrideParameter.xyz;
    bool _823 = View_View_RenderingReflectionCaptureMask != 0.0f;
    float3 _828 = 0.0f.xxx;
    if (_823)
    {
        _828 = _813 + (_820 * 0.449999988079071044921875f);
    }
    else
    {
        _828 = _813;
    }
    bool3 _829 = _823.xxx;
    float3 _830 = float3(_829.x ? 0.0f.xxx.x : _820.x, _829.y ? 0.0f.xxx.y : _820.y, _829.z ? 0.0f.xxx.z : _820.z);
    float4 _837 = LightmapResourceCluster_LightMapTexture.Sample(LightmapResourceCluster_LightMapSampler, in_var_TEXCOORD4.xy * float2(1.0f, 0.5f));
    float4 _839 = LightmapResourceCluster_LightMapTexture.Sample(LightmapResourceCluster_LightMapSampler, mad(in_var_TEXCOORD4.xy, float2(1.0f, 0.5f), float2(0.0f, 0.5f)));
    uint _844 = in_var_LIGHTMAP_ID * 15u;
    uint _845 = _844 + 4u;
    uint _849 = _844 + 6u;
    float3 _854 = _837.xyz;
    float _870 = _602.y;
    float3 _1046 = 0.0f.xxx;
    if (TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridSize.z > 0)
    {
        float4 _979 = mul(((float4(View_View_ViewTilePosition, 0.0f) + float4(_462, 0.0f)) * 2097152.0f) + float4(_492, 1.0f), View_View_RelativeWorldToClip);
        float _980 = _979.w;
        float3 _999 = float3(mad((_979.xy / _980.xx).xy, float2(0.5f, -0.5f), 0.5f.xx), (log2(mad(_980, TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridZParams.x, TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridZParams.y)) * TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridZParams.z) / float(TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridSize.z));
        float4 _1003 = TranslucentBasePass_TranslucencyGIVolumeHistory0.SampleLevel(TranslucentBasePass_TranslucencyGIVolumeSampler, _999, 0.0f);
        float3 _1004 = _1003.xyz;
        float3 _1008 = TranslucentBasePass_TranslucencyGIVolumeHistory1.SampleLevel(TranslucentBasePass_TranslucencyGIVolumeSampler, _999, 0.0f).xyz;
        float4 _1010 = 0.0f.xxxx;
        _1010.x = _1003.x;
        float4 _1012 = 0.0f.xxxx;
        _1012.x = _1003.y;
        float4 _1014 = 0.0f.xxxx;
        _1014.x = _1003.z;
        float3 _1018 = _1004 / (dot(_1004, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f)) + 9.9999997473787516355514526367188e-06f).xxx;
        float3 _1020 = _1008 * _1018.x;
        float3 _1023 = _1008 * _1018.y;
        float3 _1026 = _1008 * _1018.z;
        float4 _1029 = 0.0f.xxxx;
        _1029.y = (-0.48860299587249755859375f) * _870;
        _1029.z = 0.48860299587249755859375f * _602.z;
        _1029.w = (-0.48860299587249755859375f) * _602.x;
        _1029.x = 0.886227548122406005859375f;
        float3 _1036 = _1029.yzw * 2.094395160675048828125f;
        float4 _1037 = float4(_1029.x, _1036.x, _1036.y, _1036.z);
        float3 _1039 = 0.0f.xxx;
        _1039.x = dot(float4(_1010.x, _1020.x, _1020.y, _1020.z), _1037);
        _1039.y = dot(float4(_1012.x, _1023.x, _1023.y, _1023.z), _1037);
        _1039.z = dot(float4(_1014.x, _1026.x, _1026.y, _1026.z), _1037);
        _1046 = max(0.0f.xxx, _1039) * 0.3183098733425140380859375f.xxx;
    }
    else
    {
        float3 _963 = 0.0f.xxx;
        if (TranslucentBasePass_TranslucentBasePass_Shared_UseBasePassSkylight > 0u)
        {
            float _915 = 0.0f;
            float _916 = 0.0f;
            float3 _917 = 0.0f.xxx;
            [branch]
            if (View_View_SkyLightApplyPrecomputedBentNormalShadowingFlag != 0.0f)
            {
                float4 _898 = LightmapResourceCluster_SkyOcclusionTexture.Sample(LightmapResourceCluster_LightMapSampler, in_var_TEXCOORD4.xy);
                float _902 = _898.w;
                float3 _905 = normalize(((_898.xyz * 2.0f) - 1.0f.xxx).xyz);
                float _909 = mad(mad(_902, _902, -1.0f), mad(-_902, _902, 1.0f), 1.0f);
                _915 = lerp(clamp(dot(_905, _602), 0.0f, 1.0f), 1.0f, _909);
                _916 = _902 * _902;
                _917 = lerp(_905, _602, _909.xxx);
            }
            else
            {
                _915 = 1.0f;
                _916 = 1.0f;
                _917 = _602;
            }
            float4 _921 = float4(_917, 1.0f);
            float3 _925 = 0.0f.xxx;
            _925.x = dot(View_SkyIrradianceEnvironmentMap[0u], _921);
            _925.y = dot(View_SkyIrradianceEnvironmentMap[1u], _921);
            _925.z = dot(View_SkyIrradianceEnvironmentMap[2u], _921);
            float4 _936 = _921.xyzz * _921.yzzx;
            float3 _940 = 0.0f.xxx;
            _940.x = dot(View_SkyIrradianceEnvironmentMap[3u], _936);
            _940.y = dot(View_SkyIrradianceEnvironmentMap[4u], _936);
            _940.z = dot(View_SkyIrradianceEnvironmentMap[5u], _936);
            _963 = (max(0.0f.xxx, (_925 + _940) + (View_SkyIrradianceEnvironmentMap[6u].xyz * mad(_917.x, _917.x, -(_917.y * _917.y)))) * View_View_SkyLightColor.xyz) * (_916 * _915);
        }
        else
        {
            _963 = 0.0f.xxx;
        }
        _1046 = _963;
    }
    float3 _1047 = mad(mad(_854 * _854, Scene_GPUScene_GPUSceneLightmapData[_845].xyz, Scene_GPUScene_GPUSceneLightmapData[_849].xyz) * ((exp2(mad(_837.w + mad(_839.w, 0.0039215688593685626983642578125f, -0.00196078442968428134918212890625f), Scene_GPUScene_GPUSceneLightmapData[_845].w, Scene_GPUScene_GPUSceneLightmapData[_849].w)) - 0.0185813605785369873046875f) * max(0.0f, dot(mad(_839, Scene_GPUScene_GPUSceneLightmapData[_844 + 5u], Scene_GPUScene_GPUSceneLightmapData[_844 + 7u]), float4(_870, _602.zx, 1.0f)))), View_View_PrecomputedIndirectLightingColorScale, _1046);
    uint2 _1087 = uint2(_475 * View_View_LightProbeSizeRatioAndInvSizeRatio.zw) >> (TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridPixelSizeShift.xx & uint2(31u, 31u));
    uint _1097 = (((min(uint(max(0.0f, log2(mad(_424, TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridZParams.x, TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridZParams.y)) * TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridZParams.z)), uint(TranslucentBasePass_TranslucentBasePass_Shared_Forward_CulledGridSize.z - 1)) * uint(TranslucentBasePass_TranslucentBasePass_Shared_Forward_CulledGridSize.y)) + _1087.y) * uint(TranslucentBasePass_TranslucentBasePass_Shared_Forward_CulledGridSize.x)) + _1087.x;
    uint _1100 = asuint(Scene_GPUScene_GPUScenePrimitiveSceneData[_509].x);
    uint _1113 = (uint((_1100 & 2048u) != 0u) | (uint((_1100 & 4096u) != 0u) << 1u)) | (uint((_1100 & 8192u) != 0u) << 2u);
    float4 _2174 = 0.0f.xxxx;
    float4 _2175 = 0.0f.xxxx;
    float4 _2176 = 0.0f.xxxx;
    [branch]
    if (TranslucentBasePass_TranslucentBasePass_Shared_Forward_HasDirectionalLight != 0u)
    {
        float4 _1138 = float4(_372, float((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowMapChannelMask & 2u) >> 1u), float((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowMapChannelMask & 4u) >> 2u), float((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowMapChannelMask & 8u) >> 3u));
        _1138.x = 1.0f;
        float _1181 = 0.0f;
        [branch]
        if (TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightUseStaticShadowing > 0u)
        {
            float4 _1153 = mul(float4(_491, 1.0f), TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightTranslatedWorldToStaticShadow);
            float2 _1157 = _1153.xy / _1153.w.xx;
            bool2 _1158 = bool2(_1157.x >= 0.0f.xx.x, _1157.y >= 0.0f.xx.y);
            bool2 _1159 = bool2(_1157.x <= 1.0f.xx.x, _1157.y <= 1.0f.xx.y);
            float _1180 = 0.0f;
            if (all(bool2(_1158.x && _1159.x, _1158.y && _1159.y)))
            {
                float4 _1173 = TranslucentBasePass_Shared_Forward_DirectionalLightStaticShadowmap.SampleLevel(TranslucentBasePass_Shared_Forward_StaticShadowmapSampler, _1157, 0.0f);
                float _1174 = _1173.x;
                _1180 = float((_1153.z < _1174) || (_1174 > 0.9900000095367431640625f));
            }
            else
            {
                _1180 = 1.0f;
            }
            _1181 = _1180;
        }
        else
        {
            _1181 = 1.0f;
        }
        float4 _1182 = 0.0f.xxxx;
        _1182.x = _1181;
        float _1261 = 0.0f;
        if (TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumDirectionalLightCascades > 0u)
        {
            float4 _1190 = _494.xxxx;
            float4 _1192 = float4(bool4(_1190.x >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_CascadeEndDepths.x, _1190.y >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_CascadeEndDepths.y, _1190.z >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_CascadeEndDepths.z, _1190.w >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_CascadeEndDepths.w));
            uint _1200 = uint(((_1192.x + _1192.y) + _1192.z) + _1192.w);
            float _1260 = 0.0f;
            if (_1200 < TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumDirectionalLightCascades)
            {
                float4 _1210 = mul(float4(_491, 1.0f), TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightTranslatedWorldToShadowMatrix[_1200]);
                float2 _1214 = _1210.xy / _1210.w.xx;
                bool2 _1218 = bool2(_1214.x >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapMinMax[_1200].xy.x, _1214.y >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapMinMax[_1200].xy.y);
                bool2 _1220 = bool2(_1214.x <= TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapMinMax[_1200].zw.x, _1214.y <= TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapMinMax[_1200].zw.y);
                float _1259 = 0.0f;
                if (all(bool2(_1218.x && _1220.x, _1218.y && _1220.y)))
                {
                    float2 _1238 = mad(_1214, TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapAtlasBufferSize.xy, (-0.5f).xx);
                    float2 _1239 = frac(_1238);
                    float4 _1250 = clamp((TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapAtlas.GatherRed(TranslucentBasePass_Shared_Forward_ShadowmapSampler, (floor(_1238) + 1.0f.xx) * TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapAtlasBufferSize.zw) * 4000.0f) - mad(1.0f - _1210.z, 4000.0f, -1.0f).xxxx, 0.0f.xxxx, 1.0f.xxxx);
                    float2 _1254 = lerp(_1250.wx, _1250.zy, _1239.xx);
                    _1259 = lerp(_1254.x, _1254.y, _1239.y);
                }
                else
                {
                    _1259 = 1.0f;
                }
                _1260 = _1259;
            }
            else
            {
                _1260 = 1.0f;
            }
            _1261 = _1260;
        }
        else
        {
            _1261 = 1.0f;
        }
        float _1947 = 0.0f;
        [branch]
        if (true && (TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightVSM != (-1)))
        {
            float _1945 = 0.0f;
            do
            {
                float _1270 = max(0.0f, 0.0f);
                uint _1271 = uint(TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightVSM);
                uint _1272 = _1271 * 336u;
                uint _1274 = (_1272 + 96u) >> 2u;
                float4x4 _1288 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                _1288[2] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1274 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1274 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1274 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1274 + 3u) * 4 + 0)));
                uint _1290 = (_1272 + 128u) >> 2u;
                float4x4 _1304 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                _1304[0] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1290 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1290 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1290 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1290 + 3u) * 4 + 0)));
                uint _1306 = (_1272 + 144u) >> 2u;
                _1304[1] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1306 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1306 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1306 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1306 + 3u) * 4 + 0)));
                uint _1322 = (_1272 + 160u) >> 2u;
                _1304[2] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1322 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1322 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1322 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1322 + 3u) * 4 + 0)));
                uint _1338 = (_1272 + 176u) >> 2u;
                _1304[3] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1338 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1338 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1338 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1338 + 3u) * 4 + 0)));
                uint _1354 = (_1272 + 256u) >> 2u;
                float3 _1364 = asfloat(uint3(VirtualShadowMap_ProjectionData.Load(_1354 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1354 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1354 + 2u) * 4 + 0)));
                uint _1366 = (_1272 + 268u) >> 2u;
                uint _1370 = (_1272 + 272u) >> 2u;
                uint _1382 = (_1272 + 288u) >> 2u;
                if (VirtualShadowMap_ProjectionData.Load(_1366 * 4 + 0) == 0u)
                {
                    int _1617 = max(0, (int(floor(log2(length(((View_View_ViewTilePosition - (-_1364)) * 2097152.0f) + (_492 - (-asfloat(uint3(VirtualShadowMap_ProjectionData.Load(_1382 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1382 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1382 + 2u) * 4 + 0))))))) + asfloat(VirtualShadowMap_ProjectionData.Load(((_1272 + 300u) >> 2u) * 4 + 0)))) - int(VirtualShadowMap_ProjectionData.Load(((_1272 + 312u) >> 2u) * 4 + 0))));
                    if (_1617 < int(VirtualShadowMap_ProjectionData.Load(((_1272 + 316u) >> 2u) * 4 + 0)))
                    {
                        int _1621 = TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightVSM + _1617;
                        uint _1622 = uint(_1621);
                        uint _1623 = _1622 * 336u;
                        uint _1625 = (_1623 + 96u) >> 2u;
                        uint _1640 = (_1623 + 112u) >> 2u;
                        uint _1649 = (_1623 + 128u) >> 2u;
                        float4x4 _1663 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                        _1663[0] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1649 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1649 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1649 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1649 + 3u) * 4 + 0)));
                        uint _1665 = (_1623 + 144u) >> 2u;
                        _1663[1] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1665 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1665 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1665 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1665 + 3u) * 4 + 0)));
                        uint _1681 = (_1623 + 160u) >> 2u;
                        _1663[2] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1681 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1681 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1681 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1681 + 3u) * 4 + 0)));
                        uint _1697 = (_1623 + 176u) >> 2u;
                        _1663[3] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1697 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1697 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1697 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1697 + 3u) * 4 + 0)));
                        uint _1713 = (_1623 + 256u) >> 2u;
                        uint _1725 = (_1623 + 272u) >> 2u;
                        float4 _1744 = mul(float4(((View_View_ViewTilePosition + asfloat(uint3(VirtualShadowMap_ProjectionData.Load(_1713 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1713 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1713 + 2u) * 4 + 0)))) * 2097152.0f) + (_492 + asfloat(uint3(VirtualShadowMap_ProjectionData.Load(_1725 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1725 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1725 + 2u) * 4 + 0)))), 1.0f), _1663);
                        float2 _1745 = _1744.xy;
                        uint2 _1747 = uint2(_1745 * 128.0f);
                        uint _1763 = 0u;
                        do
                        {
                            if (uint(int(_1622)) < 8192u)
                            {
                                _1763 = _1622;
                                break;
                            }
                            _1763 = (8192u + ((_1622 - 8192u) * 21845u)) + (_1747.x + (_1747.y << 7u));
                            break;
                        } while(false);
                        uint _1771 = (VirtualShadowMap_PageTable[_1763] >> 20u) & 63u;
                        bool _1773 = (VirtualShadowMap_PageTable[_1763] & 134217728u) != 0u;
                        float _1936 = 0.0f;
                        bool _1937 = false;
                        if (_1773)
                        {
                            float _1912 = 0.0f;
                            float _1913 = 0.0f;
                            uint2 _1914 = uint2(0u, 0u);
                            uint2 _1915 = uint2(0u, 0u);
                            bool _1916 = false;
                            if (_1771 > 0u)
                            {
                                uint _1786 = (_1623 + 304u) >> 2u;
                                uint _1789 = _1786 + 1u;
                                uint _1794 = uint(int(_1622 + _1771));
                                uint _1797 = ((_1794 * 336u) + 304u) >> 2u;
                                int _1809 = int(_1771);
                                uint2 _1817 = ((_1747 - uint2(int2(32, 32) * int2(uint2(VirtualShadowMap_ProjectionData.Load(_1786 * 4 + 0), VirtualShadowMap_ProjectionData.Load(_1789 * 4 + 0))))) + uint2((int2(32, 32) * int2(uint2(VirtualShadowMap_ProjectionData.Load(_1797 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1797 + 1u) * 4 + 0)))) << (_1809.xx & int2(31, 31)))) >> (_1771.xx & uint2(31u, 31u));
                                uint2 _1818 = _1817 * uint2(128u, 128u);
                                uint _1832 = uint(_1621 + _1809) * 336u;
                                uint _1834 = (_1832 + 112u) >> 2u;
                                uint _1849 = (_1832 + 304u) >> 2u;
                                float _1870 = (_1809 >= 0) ? (1.0f / float(1u << (uint(_1809) & 31u))) : float(1u << (uint(-_1809) & 31u));
                                uint _1899 = 0u;
                                do
                                {
                                    if (uint(int(_1794)) < 8192u)
                                    {
                                        _1899 = _1794;
                                        break;
                                    }
                                    _1899 = (8192u + ((_1794 - 8192u) * 21845u)) + (_1817.x + (_1817.y << 7u));
                                    break;
                                } while(false);
                                _1912 = _1870;
                                _1913 = mad(-_1870, asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1640 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1640 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1640 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1640 + 3u) * 4 + 0))).z, asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1834 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1834 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1834 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1834 + 3u) * 4 + 0))).z);
                                _1914 = clamp(uint2(((_1745 * _1870) + ((float2(int2(uint2(VirtualShadowMap_ProjectionData.Load(_1849 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1849 + 1u) * 4 + 0)))) - (float2(int2(uint2(VirtualShadowMap_ProjectionData.Load(_1786 * 4 + 0), VirtualShadowMap_ProjectionData.Load(_1789 * 4 + 0)))) * _1870)) * 0.25f).xy) * 16384.0f), _1818, _1818 + uint2(127u, 127u));
                                _1915 = uint2(VirtualShadowMap_PageTable[_1899] & 1023u, (VirtualShadowMap_PageTable[_1899] >> 10u) & 1023u);
                                _1916 = ((VirtualShadowMap_PageTable[_1899] & 134217728u) != 0u) && (((VirtualShadowMap_PageTable[_1899] >> 20u) & 63u) == 0u);
                            }
                            else
                            {
                                _1912 = 1.0f;
                                _1913 = 0.0f;
                                _1914 = uint2(_1745 * 16384.0f);
                                _1915 = uint2(VirtualShadowMap_PageTable[_1763] & 1023u, (VirtualShadowMap_PageTable[_1763] >> 10u) & 1023u);
                                _1916 = _1773 && (_1771 == 0u);
                            }
                            float _1934 = 0.0f;
                            if (_1916)
                            {
                                int4 _1925 = int4(uint4((_1915 * uint2(128u, 128u)) + (_1914 & uint2(127u, 127u)), 0u, 0u));
                                _1934 = (asfloat(VirtualShadowMap_PhysicalPagePool.Load(int4(_1925.xyz, _1925.w)).x) - _1913) / _1912;
                            }
                            else
                            {
                                _1934 = 0.0f;
                            }
                            _1936 = _1934;
                            _1937 = _1916 ? true : false;
                        }
                        else
                        {
                            _1936 = 0.0f;
                            _1937 = false;
                        }
                        if (_1937)
                        {
                            _1945 = (mad(_1270, asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1625 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1625 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1625 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1625 + 3u) * 4 + 0))).z, _1936) > _1744.z) ? 0.0f : 1.0f;
                            break;
                        }
                    }
                }
                else
                {
                    float3 _1417 = ((View_View_ViewTilePosition + _1364) * 2097152.0f) + (_492 + asfloat(uint3(VirtualShadowMap_ProjectionData.Load(_1370 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1370 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1370 + 2u) * 4 + 0))));
                    float4x4 _1530 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                    int _1531 = 0;
                    float4x4 _1532 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                    if (VirtualShadowMap_ProjectionData.Load(_1366 * 4 + 0) != 2u)
                    {
                        uint _1445 = 0u;
                        do
                        {
                            float _1423 = _1417.x;
                            float _1424 = abs(_1423);
                            float _1425 = _1417.y;
                            float _1426 = abs(_1425);
                            float _1428 = _1417.z;
                            float _1429 = abs(_1428);
                            if ((_1424 >= _1426) && (_1424 >= _1429))
                            {
                                _1445 = (_1423 > 0.0f) ? 0u : 1u;
                                break;
                            }
                            else
                            {
                                if (_1426 > _1429)
                                {
                                    _1445 = (_1425 > 0.0f) ? 2u : 3u;
                                    break;
                                }
                                else
                                {
                                    _1445 = (_1428 > 0.0f) ? 4u : 5u;
                                    break;
                                }
                                break; // unreachable workaround
                            }
                            break; // unreachable workaround
                        } while(false);
                        int _1447 = int(_1271 + _1445);
                        uint _1449 = uint(_1447) * 336u;
                        uint _1451 = (_1449 + 96u) >> 2u;
                        float4x4 _1465 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                        _1465[2] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1451 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1451 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1451 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1451 + 3u) * 4 + 0)));
                        uint _1467 = (_1449 + 128u) >> 2u;
                        float4x4 _1481 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                        _1481[0] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1467 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1467 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1467 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1467 + 3u) * 4 + 0)));
                        uint _1483 = (_1449 + 144u) >> 2u;
                        _1481[1] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1483 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1483 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1483 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1483 + 3u) * 4 + 0)));
                        uint _1499 = (_1449 + 160u) >> 2u;
                        _1481[2] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1499 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1499 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1499 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1499 + 3u) * 4 + 0)));
                        uint _1515 = (_1449 + 176u) >> 2u;
                        _1481[3] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1515 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1515 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1515 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1515 + 3u) * 4 + 0)));
                        _1530 = _1465;
                        _1531 = _1447;
                        _1532 = _1481;
                    }
                    else
                    {
                        _1530 = _1288;
                        _1531 = TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightVSM;
                        _1532 = _1304;
                    }
                    float4 _1537 = mul(float4(_1417, 1.0f), _1532);
                    float _1538 = _1537.w;
                    float3 _1541 = _1537.xyz / _1538.xxx;
                    float2 _1542 = _1541.xy;
                    float _1595 = 0.0f;
                    bool _1596 = false;
                    do
                    {
                        bool _1552 = false;
                        uint _1545 = uint(_1531);
                        uint2 _1547 = uint2(_1542 * 128.0f);
                        uint _1563 = 0u;
                        do
                        {
                            _1552 = uint(int(_1545)) < 8192u;
                            if (_1552)
                            {
                                _1563 = _1545;
                                break;
                            }
                            _1563 = (8192u + ((_1545 - 8192u) * 21845u)) + (_1547.x + (_1547.y << 7u));
                            break;
                        } while(false);
                        if ((VirtualShadowMap_PageTable[_1563] & 134217728u) != 0u)
                        {
                            int4 _1588 = int4(uint4((uint2(VirtualShadowMap_PageTable[_1563] & 1023u, (VirtualShadowMap_PageTable[_1563] >> 10u) & 1023u) * uint2(128u, 128u)) + (uint2(_1542 * float(16384u >> ((_1552 ? 7u : ((VirtualShadowMap_PageTable[_1563] >> 20u) & 63u)) & 31u))) & uint2(127u, 127u)), 0u, 0u));
                            _1595 = asfloat(VirtualShadowMap_PhysicalPagePool.Load(int4(_1588.xyz, _1588.w)).x);
                            _1596 = true;
                            break;
                        }
                        _1595 = 0.0f;
                        _1596 = false;
                        break;
                    } while(false);
                    if (_1596)
                    {
                        _1945 = ((_1595 - (((-_1270) * _1530[2].z) / _1538)) > _1541.z) ? 0.0f : 1.0f;
                        break;
                    }
                }
                _1945 = 1.0f;
                break;
            } while(false);
            _1947 = _1261 * _1945;
        }
        else
        {
            _1947 = _1261;
        }
        float _1954 = clamp(mad(_494, TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDistanceFadeMAD.x, TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDistanceFadeMAD.y), 0.0f, 1.0f);
        float _1956 = lerp(_1947, lerp(1.0f, dot(_1182, _1138), dot(_1138, 1.0f.xxxx)), _1954 * _1954);
        float3 _2156 = 0.0f.xxx;
        float3 _2157 = 0.0f.xxx;
        [branch]
        if ((_1956 + min(_1956, 1.0f)) > 0.0f)
        {
            float _1964 = max(_800, View_View_MinRoughness);
            float _1965 = dot(TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDirection, TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDirection);
            float _1968 = rsqrt(_1965);
            float3 _1969 = TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDirection * _1968;
            float _1970 = dot(_602, _1969);
            float _1988 = 0.0f;
            if (TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius > 0.0f)
            {
                float _1977 = sqrt(clamp((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius * TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius) * (1.0f / (_1965 + 1.0f)), 0.0f, 1.0f));
                float _1987 = 0.0f;
                if (_1970 < _1977)
                {
                    float _1983 = _1977 + max(_1970, -_1977);
                    _1987 = (_1983 * _1983) / (4.0f * _1977);
                }
                else
                {
                    _1987 = _1970;
                }
                _1988 = _1987;
            }
            else
            {
                _1988 = _1970;
            }
            float _1989 = clamp(_1988, 0.0f, 1.0f);
            float _1990 = max(_1964, View_View_MinRoughness);
            float _1995 = clamp((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius * _1968) * mad(-_1990, _1990, 1.0f), 0.0f, 1.0f);
            float _2003 = dot(_602, _508);
            float _2004 = dot(_508, _1969);
            float _2006 = rsqrt(mad(2.0f, _2004, 2.0f));
            bool _2012 = _1995 > 0.0f;
            float _2091 = 0.0f;
            float _2092 = 0.0f;
            if (_2012)
            {
                float _2017 = sqrt(mad(-_1995, _1995, 1.0f));
                float _2018 = 2.0f * _1970;
                float _2019 = -_2004;
                float _2020 = mad(_2018, _2003, _2019);
                float _2089 = 0.0f;
                float _2090 = 0.0f;
                if (_2020 >= _2017)
                {
                    _2089 = 1.0f;
                    _2090 = abs(_2003);
                }
                else
                {
                    float _2025 = -_2020;
                    float _2028 = _1995 * rsqrt(mad(_2025, _2020, 1.0f));
                    float _2029 = mad(_2025, _1970, _2003);
                    float _2033 = mad(_2025, _2004, mad(2.0f * _2003, _2003, -1.0f));
                    float _2044 = _2028 * sqrt(clamp(mad(_2018 * _2003, _2004, mad(_2019, _2004, mad(-_2003, _2003, mad(-_1970, _1970, 1.0f)))), 0.0f, 1.0f));
                    float _2046 = (_2044 * 2.0f) * _2003;
                    float _2047 = mad(_1970, _2017, _2003);
                    float _2048 = mad(_2028, _2029, _2047);
                    float _2050 = mad(_2028, _2033, mad(_2004, _2017, 1.0f));
                    float _2051 = _2044 * _2050;
                    float _2052 = _2048 * _2050;
                    float _2057 = _2052 * mad(-0.5f, _2051, (0.25f * _2046) * _2048);
                    float _2067 = mad(_2048, mad(_2047, _2050 * _2050, _2052 * mad(-0.5f, mad(_2004, _2017, _2050), -0.5f)), mad(_2051, _2051, (_2046 * _2048) * mad(_2046, _2048, _2051 * (-2.0f))));
                    float _2071 = (2.0f * _2057) / mad(_2067, _2067, _2057 * _2057);
                    float _2072 = _2071 * _2067;
                    float _2074 = mad(-_2071, _2057, 1.0f);
                    float _2080 = mad(_2004, _2017, mad(_2074, _2028 * _2033, _2072 * _2046));
                    float _2082 = rsqrt(mad(2.0f, _2080, 2.0f));
                    _2089 = clamp((mad(_1970, _2017, mad(_2074, _2028 * _2029, _2072 * _2044)) + _2003) * _2082, 0.0f, 1.0f);
                    _2090 = clamp(mad(_2082, _2080, _2082), 0.0f, 1.0f);
                }
                _2091 = _2089;
                _2092 = _2090;
            }
            else
            {
                _2091 = clamp((_1970 + _2003) * _2006, 0.0f, 1.0f);
                _2092 = clamp(mad(_2006, _2004, _2006), 0.0f, 1.0f);
            }
            float _2095 = clamp(abs(_2003) + 9.9999997473787516355514526367188e-06f, 0.0f, 1.0f);
            float3 _2097 = 1.0f.xxx * _1989;
            float3 _2150 = 0.0f.xxx;
            if (((0u | (asuint(clamp(mad(-max(0.0f, TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius), 0.0500000007450580596923828125f, 1.0f), 0.0f, 1.0f)) << 1u)) & 1u) == 1u)
            {
                _2150 = 0.0f.xxx;
            }
            else
            {
                float _2104 = _1964 * _1964;
                float _2105 = _2104 * _2104;
                float _2119 = 0.0f;
                if (_2012)
                {
                    _2119 = _2105 / mad(_2104, _2104, ((0.25f * _1995) * mad(3.0f, asfloat(532487669 + (asint(_2105) >> 1)), _1995)) / (_2092 + 0.001000000047497451305389404296875f));
                }
                else
                {
                    _2119 = 1.0f;
                }
                float _2122 = mad(mad(_2091, _2105, -_2091), _2091, 1.0f);
                float _2127 = sqrt(_2105);
                float _2128 = 1.0f - _2127;
                float _2134 = 1.0f - _2092;
                float _2135 = _2134 * _2134;
                float _2136 = _2135 * _2135;
                _2150 = _2097 * (((clamp(50.0f * _830.y, 0.0f, 1.0f) * (_2136 * _2134)).xxx + (_830 * mad(-_2136, _2134, 1.0f))) * (((_2105 / ((3.1415927410125732421875f * _2122) * _2122)) * _2119) * (0.5f / mad(_1989, mad(_2095, _2128, _2127), _2095 * mad(_1989, _2128, _2127)))));
            }
            float3 _2153 = TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightColor * _1956;
            _2156 = mad(((_828 * 0.3183098733425140380859375f) * _2097) * 1.0f, _2153, 0.0f.xxx);
            _2157 = (_2150 * 1.0f) * _2153;
        }
        else
        {
            _2156 = 0.0f.xxx;
            _2157 = 0.0f.xxx;
        }
        float4 _2161 = float4(_2156, 0.0f);
        float4 _2165 = float4(_2157, 0.0f);
        float4 _2172 = 0.0f.xxxx;
        float4 _2173 = 0.0f.xxxx;
        [flatten]
        if ((((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowMapChannelMask >> 8u) & 7u) & _1113) != 0u)
        {
            _2172 = float4(_2161.x, _2161.y, _2161.z, _2161.w);
            _2173 = float4(_2165.x, _2165.y, _2165.z, _2165.w);
        }
        else
        {
            _2172 = 0.0f.xxxx;
            _2173 = 0.0f.xxxx;
        }
        _2174 = _1182;
        _2175 = _2172;
        _2176 = _2173;
    }
    else
    {
        _2174 = 0.0f.xxxx;
        _2175 = 0.0f.xxxx;
        _2176 = 0.0f.xxxx;
    }
    uint _2177 = _1097 * 2u;
    uint _2183 = _2177 + 1u;
    uint _2186 = min(min(TranslucentBasePass_Shared_Forward_NumCulledLightsGrid[_2177], TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumLocalLights), TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumLocalLights);
    float4 _2188 = 0.0f.xxxx;
    float4 _2191 = 0.0f.xxxx;
    _2188 = _2175;
    _2191 = _2176;
    float4 _2189 = 0.0f.xxxx;
    float4 _2192 = 0.0f.xxxx;
    [loop]
    for (uint _2193 = 0u; _2193 < _2186; _2188 = _2189, _2191 = _2192, _2193++)
    {
        uint _2202 = TranslucentBasePass_Shared_Forward_CulledLightDataGrid16Bit.Load(TranslucentBasePass_Shared_Forward_NumCulledLightsGrid[_2183] + _2193).x * 6u;
        uint _2205 = _2202 + 1u;
        uint _2208 = _2202 + 2u;
        uint _2211 = _2202 + 3u;
        uint _2214 = _2202 + 4u;
        uint _2218 = asuint(TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2208].w);
        uint _2224 = asuint(TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2205].y);
        uint _2240 = asuint(TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2211].z);
        float2 _2242 = spvUnpackHalf2x16(_2240 & 65535u);
        float _2243 = _2242.x;
        float2 _2246 = spvUnpackHalf2x16(asuint(TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2211].w));
        float _2247 = _2246.x;
        bool _2252 = TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2205].w == 0.0f;
        float4 _2273 = float4(float(_2218 & 1u), float((_2218 & 2u) >> 1u), float((_2218 & 4u) >> 2u), float((_2218 & 8u) >> 3u));
        uint _2274 = _2218 >> 4u;
        float3 _2290 = TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2202].xyz - _491;
        float _2291 = dot(_2290, _2290);
        float _2308 = 0.0f;
        if (_2252)
        {
            float _2303 = _2291 * (TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2202].w * TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2202].w);
            float _2306 = clamp(mad(-_2303, _2303, 1.0f), 0.0f, 1.0f);
            _2308 = _2306 * _2306;
        }
        else
        {
            float3 _2297 = _2290 * TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2202].w;
            _2308 = pow(1.0f - clamp(dot(_2297, _2297), 0.0f, 1.0f), TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2205].w);
        }
        float _2319 = 0.0f;
        if (((_2218 >> 16u) & 3u) == 2u)
        {
            float _2316 = clamp((dot(_2290 * rsqrt(_2291), TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2208].xyz) - TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2211].x) * TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2211].y, 0.0f, 1.0f);
            _2319 = _2308 * (_2316 * _2316);
        }
        else
        {
            _2319 = _2308;
        }
        float3 _2614 = 0.0f.xxx;
        float3 _2615 = 0.0f.xxx;
        [branch]
        if (_2319 > 0.0f)
        {
            float _2331 = 0.0f;
            [branch]
            if (uint((_2218 & 255u) != 0u) != 0u)
            {
                _2331 = dot(float4(float(_2274 & 1u), float((_2274 & 2u) >> 1u), float((_2274 & 4u) >> 2u), float((_2274 & 8u) >> 3u)), 1.0f.xxxx) * lerp(1.0f, dot(_2174, _2273), dot(_2273, 1.0f.xxxx));
            }
            else
            {
                _2331 = 1.0f;
            }
            float3 _2612 = 0.0f.xxx;
            float3 _2613 = 0.0f.xxx;
            [branch]
            if ((_2331 + _2331) > 0.0f)
            {
                float3 _2337 = TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2214].xyz * (0.5f * _2247);
                float3 _2338 = _2290 - _2337;
                float3 _2339 = _2290 + _2337;
                float _2342 = max(_800, View_View_MinRoughness);
                bool _2343 = _2247 > 0.0f;
                float _2368 = 0.0f;
                float _2369 = 0.0f;
                float _2370 = 0.0f;
                [branch]
                if (_2343)
                {
                    float _2355 = rsqrt(dot(_2338, _2338));
                    float _2356 = rsqrt(dot(_2339, _2339));
                    float _2357 = _2355 * _2356;
                    float _2359 = dot(_2338, _2339) * _2357;
                    _2368 = _2359;
                    _2369 = 0.5f * mad(dot(_602, _2338), _2355, dot(_602, _2339) * _2356);
                    _2370 = _2357 / mad(_2355, _2356, mad(_2359, 0.5f, 0.5f));
                }
                else
                {
                    float _2347 = dot(_2338, _2338);
                    _2368 = 1.0f;
                    _2369 = dot(_602, _2338 * rsqrt(_2347));
                    _2370 = 1.0f / (_2347 + 1.0f);
                }
                float _2388 = 0.0f;
                if (_2243 > 0.0f)
                {
                    float _2377 = sqrt(clamp((_2243 * _2243) * _2370, 0.0f, 1.0f));
                    float _2387 = 0.0f;
                    if (_2369 < _2377)
                    {
                        float _2383 = _2377 + max(_2369, -_2377);
                        _2387 = (_2383 * _2383) / (4.0f * _2377);
                    }
                    else
                    {
                        _2387 = _2369;
                    }
                    _2388 = _2387;
                }
                else
                {
                    _2388 = _2369;
                }
                float _2389 = clamp(_2388, 0.0f, 1.0f);
                float3 _2407 = 0.0f.xxx;
                if (_2343)
                {
                    float3 _2394 = reflect(-_508, _602);
                    float3 _2395 = _2339 - _2338;
                    float _2396 = dot(_2394, _2395);
                    _2407 = _2338 + (_2395 * clamp(dot(_2338, (_2394 * _2396) - _2395) / mad(_2247, _2247, -(_2396 * _2396)), 0.0f, 1.0f));
                }
                else
                {
                    _2407 = _2338;
                }
                float _2409 = rsqrt(dot(_2407, _2407));
                float3 _2410 = _2407 * _2409;
                float _2411 = max(_2342, View_View_MinRoughness);
                float _2416 = clamp((_2243 * _2409) * mad(-_2411, _2411, 1.0f), 0.0f, 1.0f);
                float _2418 = clamp(spvUnpackHalf2x16(_2240 >> 16u).x * _2409, 0.0f, 1.0f);
                float _2426 = dot(_602, _2410);
                float _2427 = dot(_602, _508);
                float _2428 = dot(_508, _2410);
                float _2430 = rsqrt(mad(2.0f, _2428, 2.0f));
                bool _2436 = _2416 > 0.0f;
                float _2515 = 0.0f;
                float _2516 = 0.0f;
                if (_2436)
                {
                    float _2441 = sqrt(mad(-_2416, _2416, 1.0f));
                    float _2442 = 2.0f * _2426;
                    float _2443 = -_2428;
                    float _2444 = mad(_2442, _2427, _2443);
                    float _2513 = 0.0f;
                    float _2514 = 0.0f;
                    if (_2444 >= _2441)
                    {
                        _2513 = 1.0f;
                        _2514 = abs(_2427);
                    }
                    else
                    {
                        float _2449 = -_2444;
                        float _2452 = _2416 * rsqrt(mad(_2449, _2444, 1.0f));
                        float _2453 = mad(_2449, _2426, _2427);
                        float _2457 = mad(_2449, _2428, mad(2.0f * _2427, _2427, -1.0f));
                        float _2468 = _2452 * sqrt(clamp(mad(_2442 * _2427, _2428, mad(_2443, _2428, mad(-_2427, _2427, mad(-_2426, _2426, 1.0f)))), 0.0f, 1.0f));
                        float _2470 = (_2468 * 2.0f) * _2427;
                        float _2471 = mad(_2426, _2441, _2427);
                        float _2472 = mad(_2452, _2453, _2471);
                        float _2474 = mad(_2452, _2457, mad(_2428, _2441, 1.0f));
                        float _2475 = _2468 * _2474;
                        float _2476 = _2472 * _2474;
                        float _2481 = _2476 * mad(-0.5f, _2475, (0.25f * _2470) * _2472);
                        float _2491 = mad(_2472, mad(_2471, _2474 * _2474, _2476 * mad(-0.5f, mad(_2428, _2441, _2474), -0.5f)), mad(_2475, _2475, (_2470 * _2472) * mad(_2470, _2472, _2475 * (-2.0f))));
                        float _2495 = (2.0f * _2481) / mad(_2491, _2491, _2481 * _2481);
                        float _2496 = _2495 * _2491;
                        float _2498 = mad(-_2495, _2481, 1.0f);
                        float _2504 = mad(_2428, _2441, mad(_2498, _2452 * _2457, _2496 * _2470));
                        float _2506 = rsqrt(mad(2.0f, _2504, 2.0f));
                        _2513 = clamp((mad(_2426, _2441, mad(_2498, _2452 * _2453, _2496 * _2468)) + _2427) * _2506, 0.0f, 1.0f);
                        _2514 = clamp(mad(_2506, _2504, _2506), 0.0f, 1.0f);
                    }
                    _2515 = _2513;
                    _2516 = _2514;
                }
                else
                {
                    _2515 = clamp((_2426 + _2427) * _2430, 0.0f, 1.0f);
                    _2516 = clamp(mad(_2430, _2428, _2430), 0.0f, 1.0f);
                }
                float _2519 = clamp(abs(_2427) + 9.9999997473787516355514526367188e-06f, 0.0f, 1.0f);
                float3 _2522 = 1.0f.xxx * ((_2252 ? _2370 : 1.0f) * _2389);
                float3 _2606 = 0.0f.xxx;
                if (((0u | (asuint(clamp(mad(-max(_2247, _2243), 0.0500000007450580596923828125f, 1.0f), 0.0f, 1.0f)) << 1u)) & 1u) == 1u)
                {
                    _2606 = 0.0f.xxx;
                }
                else
                {
                    float _2529 = _2342 * _2342;
                    float _2539 = 0.0f;
                    if (_2418 > 0.0f)
                    {
                        _2539 = clamp(mad(_2529, _2529, (_2418 * _2418) / mad(_2516, 3.599999904632568359375f, 0.4000000059604644775390625f)), 0.0f, 1.0f);
                    }
                    else
                    {
                        _2539 = _2529 * _2529;
                    }
                    float _2553 = 0.0f;
                    float _2554 = 0.0f;
                    if (_2436)
                    {
                        float _2551 = _2539 + (((0.25f * _2416) * mad(3.0f, asfloat(532487669 + (asint(_2539) >> 1)), _2416)) / (_2516 + 0.001000000047497451305389404296875f));
                        _2553 = _2539 / _2551;
                        _2554 = _2551;
                    }
                    else
                    {
                        _2553 = 1.0f;
                        _2554 = _2539;
                    }
                    float _2575 = 0.0f;
                    if (_2368 < 1.0f)
                    {
                        float _2561 = sqrt((1.00010001659393310546875f - _2368) / (1.0f + _2368));
                        _2575 = _2553 * sqrt(_2554 / (_2554 + (((0.25f * _2561) * mad(3.0f, asfloat(532487669 + (asint(_2554) >> 1)), _2561)) / (_2516 + 0.001000000047497451305389404296875f))));
                    }
                    else
                    {
                        _2575 = _2553;
                    }
                    float _2578 = mad(mad(_2515, _2539, -_2515), _2515, 1.0f);
                    float _2583 = sqrt(_2539);
                    float _2584 = 1.0f - _2583;
                    float _2590 = 1.0f - _2516;
                    float _2591 = _2590 * _2590;
                    float _2592 = _2591 * _2591;
                    _2606 = _2522 * (((clamp(50.0f * _830.y, 0.0f, 1.0f) * (_2592 * _2590)).xxx + (_830 * mad(-_2592, _2590, 1.0f))) * (((_2539 / ((3.1415927410125732421875f * _2578) * _2578)) * _2575) * (0.5f / mad(_2389, mad(_2519, _2584, _2583), _2519 * mad(_2389, _2584, _2583)))));
                }
                float3 _2609 = ((float3(float((_2224 >> 0u) & 1023u), float((_2224 >> 10u) & 1023u), float((_2224 >> 20u) & 1023u)) * TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2205].x) * _2319) * _2331;
                _2612 = mad(((_828 * 0.3183098733425140380859375f) * _2522) * 1.0f, _2609, 0.0f.xxx);
                _2613 = (_2606 * spvUnpackHalf2x16(asuint(TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2214].w) & 65535u).x) * _2609;
            }
            else
            {
                _2612 = 0.0f.xxx;
                _2613 = 0.0f.xxx;
            }
            _2614 = _2612;
            _2615 = _2613;
        }
        else
        {
            _2614 = 0.0f.xxx;
            _2615 = 0.0f.xxx;
        }
        [flatten]
        if ((((_2218 >> 8u) & 7u) & _1113) != 0u)
        {
            _2189 = _2188 + float4(_2614, 0.0f);
            _2192 = _2191 + float4(_2615, 0.0f);
        }
        else
        {
            _2189 = _2188;
            _2192 = _2191;
        }
    }
    bool4 _2633 = (TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectLightingShowFlag == 0u).xxxx;
    float3 _2642 = (_602 * (2.0f * dot(_508, _602))) - _508;
    bool _2681 = false;
    if (TranslucentBasePass_TranslucentBasePass_Enabled > 0u)
    {
        float _2674 = 0.0f;
        do
        {
            [flatten]
            if (asuint(View_View_ViewToClip[3].w) != 0u)
            {
                _2674 = mad(_494, View_View_ViewToClip[2u].z, View_View_ViewToClip[3u].z);
                break;
            }
            else
            {
                _2674 = 1.0f / ((_494 + View_View_InvDeviceZToWorldZTransform.w) * View_View_InvDeviceZToWorldZTransform.z);
                break;
            }
            break; // unreachable workaround
        } while(false);
        _2681 = (abs(TranslucentBasePass_SceneDepth.SampleLevel(View_SharedPointClampedSampler, _499, 0.0f).x - _2674) < TranslucentBasePass_TranslucentBasePass_RelativeDepthThreshold) ? true : false;
    }
    else
    {
        _2681 = false;
    }
    bool _2685 = !_2681;
    uint _2751 = 0u;
    bool _2752 = false;
    if ((TranslucentBasePass_TranslucentBasePass_FinalProbeResolution > 0u) && _2685)
    {
        uint _2707 = 0u;
        float _2697 = frac(52.98291778564453125f * frac(dot(gl_FragCoord.xy + (float2(32.66500091552734375f, 11.81499958038330078125f) * float(View_View_StateFrameIndexMod8)), float2(0.067110560834407806396484375f, 0.005837149918079376220703125f))));
        float3 _2699 = (View_View_ViewTilePosition * 2097152.0f) + _492;
        uint _2748 = 0u;
        do
        {
            uint _2745 = 0u;
            bool _2746 = false;
            uint _2703 = 0u;
            for (;;)
            {
                _2707 = TranslucentBasePass_TranslucentBasePass_NumRadianceProbeClipmaps;
                if (_2703 < _2707)
                {
                    float3 _2717 = (_2699 * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2703].y) + TranslucentBasePass_TranslucentBasePass_PaddedWorldPositionToRadianceProbeCoordBias[_2703].xyz;
                    float3 _2722 = clamp((_2717 - 0.5f.xxx) * TranslucentBasePass_TranslucentBasePass_InvClipmapFadeSize, 0.0f.xxx, 1.0f.xxx);
                    float3 _2730 = clamp(((float(TranslucentBasePass_TranslucentBasePass_RadianceProbeClipmapResolution).xxx - 0.5f.xxx) - _2717) * TranslucentBasePass_TranslucentBasePass_InvClipmapFadeSize, 0.0f.xxx, 1.0f.xxx);
                    if (min(min(_2722.x, min(_2722.y, _2722.z)), min(_2730.x, min(_2730.y, _2730.z))) > _2697)
                    {
                        _2745 = _2703;
                        _2746 = true;
                        break;
                    }
                    _2703++;
                    continue;
                }
                else
                {
                    _2745 = _341;
                    _2746 = false;
                    break;
                }
            }
            if (_2746)
            {
                _2748 = _2745;
                break;
            }
            _2748 = _2707;
            break;
        } while(false);
        _2751 = _2748;
        _2752 = (_2748 < _2707) ? true : false;
    }
    else
    {
        _2751 = 0u;
        _2752 = false;
    }
    float3 _3968 = 0.0f.xxx;
    if (_2681)
    {
        _3968 = (pow((TranslucentBasePass_Radiance.SampleLevel(View_SharedPointClampedSampler, _499, 0.0f).xyz * View_View_OneOverPreExposure) * 5.5555553436279296875f, TranslucentBasePass_TranslucentBasePass_Contrast.xxx) * 0.180000007152557373046875f) * TranslucentBasePass_TranslucentBasePass_SpecularScale;
    }
    else
    {
        float3 _3950 = 0.0f.xxx;
        if (_2752)
        {
            float3 _3042 = (View_View_ViewTilePosition * 2097152.0f) + _492;
            float3 _3050 = ((_3042 * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2751].y) + TranslucentBasePass_TranslucentBasePass_PaddedWorldPositionToRadianceProbeCoordBias[_2751].xyz) - 0.5f.xxx;
            int3 _3052 = int3(floor(_3050));
            float3 _3053 = frac(_3050);
            uint3 _3054 = uint3(_3052);
            uint _3060 = _2751 * TranslucentBasePass_TranslucentBasePass_RadianceProbeClipmapResolution;
            int4 _3065 = int4(uint4(_3054.x + _3060, _3054.yz, 0u));
            uint4 _3069 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3065.xyz, _3065.w));
            uint _3070 = _3069.x;
            float3 _3082 = ((float3(_3054) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2751].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2751].xyz) + TranslucentBasePass_ProbeWorldOffset[_3070].xyz;
            float _3085 = TranslucentBasePass_TranslucentBasePass_ReprojectionRadiusScale * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2751].x;
            float3 _3091 = _3042 - float4(_3082, _3085).xyz;
            float _3093 = dot(_2642, _2642);
            float _3094 = dot(_2642, _3091);
            float _3095 = 2.0f * _3094;
            float _3096 = -_3085;
            float _3098 = 4.0f * _3093;
            float _3101 = mad(_3095, _3095, -(_3098 * mad(_3096, _3085, dot(_3091, _3091))));
            float2 _3113 = 0.0f.xx;
            [flatten]
            if (_3101 >= 0.0f)
            {
                _3113 = ((_3094 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3101))) / (2.0f * _3093).xx;
            }
            else
            {
                _3113 = (-1.0f).xx;
            }
            float3 _3117 = (_3042 + (_2642 * _3113.y)) - _3082;
            float3 _3122 = normalize(_3117);
            float3 _3123 = abs(_3122);
            float _3126 = sqrt(1.0f - _3123.z);
            float _3127 = _3123.x;
            float _3128 = _3123.y;
            float _3132 = min(_3127, _3128) / (max(_3127, _3128) + 5.4210108624275221700372640043497e-20f);
            float _3138 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3132, 0.0419038832187652587890625f), _3132, 0.08817707002162933349609375f), _3132, -0.2473337352275848388671875f), _3132, 0.006157201714813709259033203125f), _3132, 0.63622653484344482421875f), _3132, 4.0675854506844189018011093139648e-06f);
            float _3141 = (_3127 < _3128) ? (1.0f - _3138) : _3138;
            float2 _3145 = float2(mad(-_3141, _3126, _3126), _3141 * _3126);
            bool2 _3148 = (_3122.z < 0.0f).xx;
            float2 _3150 = 1.0f.xx - _3145.yx;
            uint2 _3160 = TranslucentBasePass_TranslucentBasePass_FinalProbeResolution.xx;
            uint _3166 = TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionDivideShift & 31u;
            float _3172 = float(TranslucentBasePass_TranslucentBasePass_RadianceProbeResolution);
            float2 _3179 = float(1u << (TranslucentBasePass_TranslucentBasePass_FinalRadianceAtlasMaxMip & 31u)).xx;
            bool3 _3187 = (_3070 == 4294967295u).xxx;
            float3 _3192 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3160 * uint2(_3070 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3070 >> _3166)) + ((((asfloat(asuint(float2(_3148.x ? _3150.x : _3145.x, _3148.y ? _3150.y : _3145.y)) ^ (asuint(_3122.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3172) + _3179)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3196 = uint3(_3052 + int3(0, 0, 1));
            int4 _3202 = int4(uint4(_3196.x + _3060, _3196.yz, 0u));
            uint4 _3205 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3202.xyz, _3202.w));
            uint _3206 = _3205.x;
            float3 _3213 = ((float3(_3196) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2751].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2751].xyz) + TranslucentBasePass_ProbeWorldOffset[_3206].xyz;
            float3 _3219 = _3042 - float4(_3213, _3085).xyz;
            float _3221 = dot(_2642, _3219);
            float _3222 = 2.0f * _3221;
            float _3226 = mad(_3222, _3222, -(_3098 * mad(_3096, _3085, dot(_3219, _3219))));
            float2 _3238 = 0.0f.xx;
            [flatten]
            if (_3226 >= 0.0f)
            {
                _3238 = ((_3221 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3226))) / (2.0f * _3093).xx;
            }
            else
            {
                _3238 = (-1.0f).xx;
            }
            float3 _3242 = (_3042 + (_2642 * _3238.y)) - _3213;
            float3 _3247 = normalize(_3242);
            float3 _3248 = abs(_3247);
            float _3251 = sqrt(1.0f - _3248.z);
            float _3252 = _3248.x;
            float _3253 = _3248.y;
            float _3257 = min(_3252, _3253) / (max(_3252, _3253) + 5.4210108624275221700372640043497e-20f);
            float _3263 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3257, 0.0419038832187652587890625f), _3257, 0.08817707002162933349609375f), _3257, -0.2473337352275848388671875f), _3257, 0.006157201714813709259033203125f), _3257, 0.63622653484344482421875f), _3257, 4.0675854506844189018011093139648e-06f);
            float _3266 = (_3252 < _3253) ? (1.0f - _3263) : _3263;
            float2 _3270 = float2(mad(-_3266, _3251, _3251), _3266 * _3251);
            bool2 _3273 = (_3247.z < 0.0f).xx;
            float2 _3275 = 1.0f.xx - _3270.yx;
            bool3 _3295 = (_3206 == 4294967295u).xxx;
            float3 _3298 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3160 * uint2(_3206 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3206 >> _3166)) + ((((asfloat(asuint(float2(_3273.x ? _3275.x : _3270.x, _3273.y ? _3275.y : _3270.y)) ^ (asuint(_3247.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3172) + _3179)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3302 = uint3(_3052 + int3(0, 1, 0));
            int4 _3308 = int4(uint4(_3302.x + _3060, _3302.yz, 0u));
            uint4 _3311 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3308.xyz, _3308.w));
            uint _3312 = _3311.x;
            float3 _3319 = ((float3(_3302) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2751].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2751].xyz) + TranslucentBasePass_ProbeWorldOffset[_3312].xyz;
            float3 _3325 = _3042 - float4(_3319, _3085).xyz;
            float _3327 = dot(_2642, _3325);
            float _3328 = 2.0f * _3327;
            float _3332 = mad(_3328, _3328, -(_3098 * mad(_3096, _3085, dot(_3325, _3325))));
            float2 _3344 = 0.0f.xx;
            [flatten]
            if (_3332 >= 0.0f)
            {
                _3344 = ((_3327 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3332))) / (2.0f * _3093).xx;
            }
            else
            {
                _3344 = (-1.0f).xx;
            }
            float3 _3348 = (_3042 + (_2642 * _3344.y)) - _3319;
            float3 _3353 = normalize(_3348);
            float3 _3354 = abs(_3353);
            float _3357 = sqrt(1.0f - _3354.z);
            float _3358 = _3354.x;
            float _3359 = _3354.y;
            float _3363 = min(_3358, _3359) / (max(_3358, _3359) + 5.4210108624275221700372640043497e-20f);
            float _3369 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3363, 0.0419038832187652587890625f), _3363, 0.08817707002162933349609375f), _3363, -0.2473337352275848388671875f), _3363, 0.006157201714813709259033203125f), _3363, 0.63622653484344482421875f), _3363, 4.0675854506844189018011093139648e-06f);
            float _3372 = (_3358 < _3359) ? (1.0f - _3369) : _3369;
            float2 _3376 = float2(mad(-_3372, _3357, _3357), _3372 * _3357);
            bool2 _3379 = (_3353.z < 0.0f).xx;
            float2 _3381 = 1.0f.xx - _3376.yx;
            bool3 _3401 = (_3312 == 4294967295u).xxx;
            float3 _3404 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3160 * uint2(_3312 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3312 >> _3166)) + ((((asfloat(asuint(float2(_3379.x ? _3381.x : _3376.x, _3379.y ? _3381.y : _3376.y)) ^ (asuint(_3353.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3172) + _3179)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3408 = uint3(_3052 + int3(0, 1, 1));
            int4 _3414 = int4(uint4(_3408.x + _3060, _3408.yz, 0u));
            uint4 _3417 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3414.xyz, _3414.w));
            uint _3418 = _3417.x;
            float3 _3425 = ((float3(_3408) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2751].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2751].xyz) + TranslucentBasePass_ProbeWorldOffset[_3418].xyz;
            float3 _3431 = _3042 - float4(_3425, _3085).xyz;
            float _3433 = dot(_2642, _3431);
            float _3434 = 2.0f * _3433;
            float _3438 = mad(_3434, _3434, -(_3098 * mad(_3096, _3085, dot(_3431, _3431))));
            float2 _3450 = 0.0f.xx;
            [flatten]
            if (_3438 >= 0.0f)
            {
                _3450 = ((_3433 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3438))) / (2.0f * _3093).xx;
            }
            else
            {
                _3450 = (-1.0f).xx;
            }
            float3 _3454 = (_3042 + (_2642 * _3450.y)) - _3425;
            float3 _3459 = normalize(_3454);
            float3 _3460 = abs(_3459);
            float _3463 = sqrt(1.0f - _3460.z);
            float _3464 = _3460.x;
            float _3465 = _3460.y;
            float _3469 = min(_3464, _3465) / (max(_3464, _3465) + 5.4210108624275221700372640043497e-20f);
            float _3475 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3469, 0.0419038832187652587890625f), _3469, 0.08817707002162933349609375f), _3469, -0.2473337352275848388671875f), _3469, 0.006157201714813709259033203125f), _3469, 0.63622653484344482421875f), _3469, 4.0675854506844189018011093139648e-06f);
            float _3478 = (_3464 < _3465) ? (1.0f - _3475) : _3475;
            float2 _3482 = float2(mad(-_3478, _3463, _3463), _3478 * _3463);
            bool2 _3485 = (_3459.z < 0.0f).xx;
            float2 _3487 = 1.0f.xx - _3482.yx;
            bool3 _3507 = (_3418 == 4294967295u).xxx;
            float3 _3510 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3160 * uint2(_3418 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3418 >> _3166)) + ((((asfloat(asuint(float2(_3485.x ? _3487.x : _3482.x, _3485.y ? _3487.y : _3482.y)) ^ (asuint(_3459.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3172) + _3179)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3514 = uint3(_3052 + int3(1, 0, 0));
            int4 _3520 = int4(uint4(_3514.x + _3060, _3514.yz, 0u));
            uint4 _3523 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3520.xyz, _3520.w));
            uint _3524 = _3523.x;
            float3 _3531 = ((float3(_3514) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2751].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2751].xyz) + TranslucentBasePass_ProbeWorldOffset[_3524].xyz;
            float3 _3537 = _3042 - float4(_3531, _3085).xyz;
            float _3539 = dot(_2642, _3537);
            float _3540 = 2.0f * _3539;
            float _3544 = mad(_3540, _3540, -(_3098 * mad(_3096, _3085, dot(_3537, _3537))));
            float2 _3556 = 0.0f.xx;
            [flatten]
            if (_3544 >= 0.0f)
            {
                _3556 = ((_3539 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3544))) / (2.0f * _3093).xx;
            }
            else
            {
                _3556 = (-1.0f).xx;
            }
            float3 _3560 = (_3042 + (_2642 * _3556.y)) - _3531;
            float3 _3565 = normalize(_3560);
            float3 _3566 = abs(_3565);
            float _3569 = sqrt(1.0f - _3566.z);
            float _3570 = _3566.x;
            float _3571 = _3566.y;
            float _3575 = min(_3570, _3571) / (max(_3570, _3571) + 5.4210108624275221700372640043497e-20f);
            float _3581 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3575, 0.0419038832187652587890625f), _3575, 0.08817707002162933349609375f), _3575, -0.2473337352275848388671875f), _3575, 0.006157201714813709259033203125f), _3575, 0.63622653484344482421875f), _3575, 4.0675854506844189018011093139648e-06f);
            float _3584 = (_3570 < _3571) ? (1.0f - _3581) : _3581;
            float2 _3588 = float2(mad(-_3584, _3569, _3569), _3584 * _3569);
            bool2 _3591 = (_3565.z < 0.0f).xx;
            float2 _3593 = 1.0f.xx - _3588.yx;
            bool3 _3613 = (_3524 == 4294967295u).xxx;
            float3 _3616 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3160 * uint2(_3524 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3524 >> _3166)) + ((((asfloat(asuint(float2(_3591.x ? _3593.x : _3588.x, _3591.y ? _3593.y : _3588.y)) ^ (asuint(_3565.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3172) + _3179)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3620 = uint3(_3052 + int3(1, 0, 1));
            int4 _3626 = int4(uint4(_3620.x + _3060, _3620.yz, 0u));
            uint4 _3629 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3626.xyz, _3626.w));
            uint _3630 = _3629.x;
            float3 _3637 = ((float3(_3620) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2751].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2751].xyz) + TranslucentBasePass_ProbeWorldOffset[_3630].xyz;
            float3 _3643 = _3042 - float4(_3637, _3085).xyz;
            float _3645 = dot(_2642, _3643);
            float _3646 = 2.0f * _3645;
            float _3650 = mad(_3646, _3646, -(_3098 * mad(_3096, _3085, dot(_3643, _3643))));
            float2 _3662 = 0.0f.xx;
            [flatten]
            if (_3650 >= 0.0f)
            {
                _3662 = ((_3645 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3650))) / (2.0f * _3093).xx;
            }
            else
            {
                _3662 = (-1.0f).xx;
            }
            float3 _3666 = (_3042 + (_2642 * _3662.y)) - _3637;
            float3 _3671 = normalize(_3666);
            float3 _3672 = abs(_3671);
            float _3675 = sqrt(1.0f - _3672.z);
            float _3676 = _3672.x;
            float _3677 = _3672.y;
            float _3681 = min(_3676, _3677) / (max(_3676, _3677) + 5.4210108624275221700372640043497e-20f);
            float _3687 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3681, 0.0419038832187652587890625f), _3681, 0.08817707002162933349609375f), _3681, -0.2473337352275848388671875f), _3681, 0.006157201714813709259033203125f), _3681, 0.63622653484344482421875f), _3681, 4.0675854506844189018011093139648e-06f);
            float _3690 = (_3676 < _3677) ? (1.0f - _3687) : _3687;
            float2 _3694 = float2(mad(-_3690, _3675, _3675), _3690 * _3675);
            bool2 _3697 = (_3671.z < 0.0f).xx;
            float2 _3699 = 1.0f.xx - _3694.yx;
            bool3 _3719 = (_3630 == 4294967295u).xxx;
            float3 _3722 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3160 * uint2(_3630 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3630 >> _3166)) + ((((asfloat(asuint(float2(_3697.x ? _3699.x : _3694.x, _3697.y ? _3699.y : _3694.y)) ^ (asuint(_3671.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3172) + _3179)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3726 = uint3(_3052 + int3(1, 1, 0));
            int4 _3732 = int4(uint4(_3726.x + _3060, _3726.yz, 0u));
            uint4 _3735 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3732.xyz, _3732.w));
            uint _3736 = _3735.x;
            float3 _3743 = ((float3(_3726) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2751].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2751].xyz) + TranslucentBasePass_ProbeWorldOffset[_3736].xyz;
            float3 _3749 = _3042 - float4(_3743, _3085).xyz;
            float _3751 = dot(_2642, _3749);
            float _3752 = 2.0f * _3751;
            float _3756 = mad(_3752, _3752, -(_3098 * mad(_3096, _3085, dot(_3749, _3749))));
            float2 _3768 = 0.0f.xx;
            [flatten]
            if (_3756 >= 0.0f)
            {
                _3768 = ((_3751 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3756))) / (2.0f * _3093).xx;
            }
            else
            {
                _3768 = (-1.0f).xx;
            }
            float3 _3772 = (_3042 + (_2642 * _3768.y)) - _3743;
            float3 _3777 = normalize(_3772);
            float3 _3778 = abs(_3777);
            float _3781 = sqrt(1.0f - _3778.z);
            float _3782 = _3778.x;
            float _3783 = _3778.y;
            float _3787 = min(_3782, _3783) / (max(_3782, _3783) + 5.4210108624275221700372640043497e-20f);
            float _3793 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3787, 0.0419038832187652587890625f), _3787, 0.08817707002162933349609375f), _3787, -0.2473337352275848388671875f), _3787, 0.006157201714813709259033203125f), _3787, 0.63622653484344482421875f), _3787, 4.0675854506844189018011093139648e-06f);
            float _3796 = (_3782 < _3783) ? (1.0f - _3793) : _3793;
            float2 _3800 = float2(mad(-_3796, _3781, _3781), _3796 * _3781);
            bool2 _3803 = (_3777.z < 0.0f).xx;
            float2 _3805 = 1.0f.xx - _3800.yx;
            bool3 _3825 = (_3736 == 4294967295u).xxx;
            float3 _3828 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3160 * uint2(_3736 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3736 >> _3166)) + ((((asfloat(asuint(float2(_3803.x ? _3805.x : _3800.x, _3803.y ? _3805.y : _3800.y)) ^ (asuint(_3777.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3172) + _3179)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3832 = uint3(_3052 + int3(1, 1, 1));
            int4 _3838 = int4(uint4(_3832.x + _3060, _3832.yz, 0u));
            uint4 _3841 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3838.xyz, _3838.w));
            uint _3842 = _3841.x;
            float3 _3849 = ((float3(_3832) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2751].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2751].xyz) + TranslucentBasePass_ProbeWorldOffset[_3842].xyz;
            float3 _3855 = _3042 - float4(_3849, _3085).xyz;
            float _3857 = dot(_2642, _3855);
            float _3858 = 2.0f * _3857;
            float _3862 = mad(_3858, _3858, -(_3098 * mad(_3096, _3085, dot(_3855, _3855))));
            float2 _3874 = 0.0f.xx;
            [flatten]
            if (_3862 >= 0.0f)
            {
                _3874 = ((_3857 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3862))) / (2.0f * _3093).xx;
            }
            else
            {
                _3874 = (-1.0f).xx;
            }
            float3 _3878 = (_3042 + (_2642 * _3874.y)) - _3849;
            float3 _3883 = normalize(_3878);
            float3 _3884 = abs(_3883);
            float _3887 = sqrt(1.0f - _3884.z);
            float _3888 = _3884.x;
            float _3889 = _3884.y;
            float _3893 = min(_3888, _3889) / (max(_3888, _3889) + 5.4210108624275221700372640043497e-20f);
            float _3899 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3893, 0.0419038832187652587890625f), _3893, 0.08817707002162933349609375f), _3893, -0.2473337352275848388671875f), _3893, 0.006157201714813709259033203125f), _3893, 0.63622653484344482421875f), _3893, 4.0675854506844189018011093139648e-06f);
            float _3902 = (_3888 < _3889) ? (1.0f - _3899) : _3899;
            float2 _3906 = float2(mad(-_3902, _3887, _3887), _3902 * _3887);
            bool2 _3909 = (_3883.z < 0.0f).xx;
            float2 _3911 = 1.0f.xx - _3906.yx;
            bool3 _3931 = (_3842 == 4294967295u).xxx;
            float3 _3934 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3160 * uint2(_3842 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3842 >> _3166)) + ((((asfloat(asuint(float2(_3909.x ? _3911.x : _3906.x, _3909.y ? _3911.y : _3906.y)) ^ (asuint(_3883.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3172) + _3179)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            float3 _3938 = _3053.z.xxx;
            float3 _3944 = _3053.y.xxx;
            _3950 = lerp(lerp(lerp(float3(_3187.x ? 0.0f.xxx.x : _3192.x, _3187.y ? 0.0f.xxx.y : _3192.y, _3187.z ? 0.0f.xxx.z : _3192.z) * ((_3113.y * _3113.y) / (_3085 * dot(_3117, _2642))), float3(_3295.x ? 0.0f.xxx.x : _3298.x, _3295.y ? 0.0f.xxx.y : _3298.y, _3295.z ? 0.0f.xxx.z : _3298.z) * ((_3238.y * _3238.y) / (_3085 * dot(_3242, _2642))), _3938), lerp(float3(_3401.x ? 0.0f.xxx.x : _3404.x, _3401.y ? 0.0f.xxx.y : _3404.y, _3401.z ? 0.0f.xxx.z : _3404.z) * ((_3344.y * _3344.y) / (_3085 * dot(_3348, _2642))), float3(_3507.x ? 0.0f.xxx.x : _3510.x, _3507.y ? 0.0f.xxx.y : _3510.y, _3507.z ? 0.0f.xxx.z : _3510.z) * ((_3450.y * _3450.y) / (_3085 * dot(_3454, _2642))), _3938), _3944), lerp(lerp(float3(_3613.x ? 0.0f.xxx.x : _3616.x, _3613.y ? 0.0f.xxx.y : _3616.y, _3613.z ? 0.0f.xxx.z : _3616.z) * ((_3556.y * _3556.y) / (_3085 * dot(_3560, _2642))), float3(_3719.x ? 0.0f.xxx.x : _3722.x, _3719.y ? 0.0f.xxx.y : _3722.y, _3719.z ? 0.0f.xxx.z : _3722.z) * ((_3662.y * _3662.y) / (_3085 * dot(_3666, _2642))), _3938), lerp(float3(_3825.x ? 0.0f.xxx.x : _3828.x, _3825.y ? 0.0f.xxx.y : _3828.y, _3825.z ? 0.0f.xxx.z : _3828.z) * ((_3768.y * _3768.y) / (_3085 * dot(_3772, _2642))), float3(_3931.x ? 0.0f.xxx.x : _3934.x, _3931.y ? 0.0f.xxx.y : _3934.y, _3931.z ? 0.0f.xxx.z : _3934.z) * ((_3874.y * _3874.y) / (_3085 * dot(_3878, _2642))), _3938), _3944), _3053.x.xxx);
        }
        else
        {
            uint _2762 = (TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumGridCells + _1097) * 2u;
            uint _2767 = min(TranslucentBasePass_Shared_Forward_NumCulledLightsGrid[_2762], TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumReflectionCaptures);
            uint _2768 = _2762 + 1u;
            float _2775 = mad(-1.2000000476837158203125f, log2(max(_800, 0.001000000047497451305389404296875f)), 1.0f);
            float _2777 = (View_View_ReflectionCubemapMaxMip - 1.0f) - _2775;
            float2 _2779 = 0.0f.xx;
            float4 _2782 = 0.0f.xxxx;
            _2779 = float2(0.0f, 1.0f);
            _2782 = float4(0.0f, 0.0f, 0.0f, 1.0f);
            float2 _2780 = 0.0f.xx;
            float4 _2783 = 0.0f.xxxx;
            [loop]
            for (uint _2784 = 0u; _2784 < _2767; _2779 = _2780, _2782 = _2783, _2784++)
            {
                [branch]
                if (_2782.w < 0.001000000047497451305389404296875f)
                {
                    break;
                }
                uint4 _2795 = TranslucentBasePass_Shared_Forward_CulledLightDataGrid16Bit.Load(TranslucentBasePass_Shared_Forward_NumCulledLightsGrid[_2768] + _2784);
                uint _2796 = _2795.x;
                float3 _2806 = ((ReflectionCaptureSM5_ReflectionCaptureSM5_TilePosition[_2796].xyz + _463) * 2097152.0f) + (ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2796].xyz + View_View_RelativePreViewTranslation);
                float3 _2811 = _491 - _2806;
                float _2813 = sqrt(dot(_2811, _2811));
                [branch]
                if (_2813 < ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2796].w)
                {
                    float _2932 = 0.0f;
                    float3 _2933 = 0.0f.xxx;
                    [branch]
                    if (ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureProperties[_2796].z > 0.0f)
                    {
                        float3 _2864 = float4(_2806, ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2796].w).xyz;
                        float3 _2865 = _491 - _2864;
                        float3 _2871 = mul(float4(_2865, 1.0f), ReflectionCaptureSM5_ReflectionCaptureSM5_BoxTransform[_2796]).xyz;
                        float3 _2877 = mul(float4(_2642, 0.0f), ReflectionCaptureSM5_ReflectionCaptureSM5_BoxTransform[_2796]).xyz;
                        float3 _2878 = 1.0f.xxx / _2877;
                        float3 _2880 = -_2871;
                        float3 _2883 = max(mad(_2880, _2878, (-1.0f).xxx / _2877), mad(_2880, _2878, _2878));
                        float3 _2897 = ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2796].xyz - (0.5f * ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2796].w).xxx;
                        float3 _2898 = -_2897;
                        float3 _2899 = _2871 * ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2796].xyz;
                        bool3 _2900 = bool3(_2899.x < _2898.x, _2899.y < _2898.y, _2899.z < _2898.z);
                        float3 _2902 = abs(mad(_2871, ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2796].xyz, _2897));
                        bool3 _2913 = bool3(_2899.x > _2897.x, _2899.y > _2897.y, _2899.z > _2897.z);
                        float3 _2915 = abs(mad(_2871, ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2796].xyz, _2898));
                        _2932 = 1.0f - smoothstep(0.0f, 0.699999988079071044921875f * ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2796].w, dot(float3(_2900.x ? _2902.x : 0.0f, _2900.y ? _2902.y : 0.0f, _2900.z ? _2902.z : 0.0f), 1.0f.xxx) + dot(float3(_2913.x ? _2915.x : 0.0f, _2913.y ? _2915.y : 0.0f, _2913.z ? _2915.z : 0.0f), 1.0f.xxx));
                        _2933 = (_491 + (_2642 * min(_2883.x, min(_2883.y, _2883.z)))) - (_2864 + ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureOffsetAndAverageBrightness[_2796].xyz);
                    }
                    else
                    {
                        float3 _2832 = _491 - float4(_2806, ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2796].w).xyz;
                        float _2834 = dot(_2642, _2832);
                        float _2838 = mad(_2834, _2834, -mad(-ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2796].w, ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2796].w, dot(_2832, _2832)));
                        float _2853 = 0.0f;
                        float3 _2854 = 0.0f.xxx;
                        [flatten]
                        if (_2838 >= 0.0f)
                        {
                            float _2848 = clamp(mad(2.5f, clamp(_2813 / ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2796].w, 0.0f, 1.0f), -1.5f), 0.0f, 1.0f);
                            _2853 = mad(-(_2848 * _2848), mad(-2.0f, _2848, 3.0f), 1.0f);
                            _2854 = (_2832 + (_2642 * (sqrt(_2838) - _2834))) - ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureOffsetAndAverageBrightness[_2796].xyz;
                        }
                        else
                        {
                            _2853 = 0.0f;
                            _2854 = _2642;
                        }
                        _2932 = _2853;
                        _2933 = _2854;
                    }
                    float4 _2942 = TranslucentBasePass_Shared_Reflection_ReflectionCubemap.SampleLevel(TranslucentBasePass_Shared_Reflection_ReflectionCubemapSampler, float4(_2933, ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureProperties[_2796].y), _2777);
                    float3 _2945 = _2942.xyz * ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureProperties[_2796].x;
                    float4 _2947 = float4(_2945.x, _2945.y, _2945.z, _2942.w) * _2932;
                    float3 _2952 = _2782.xyz + ((_2947.xyz * _2782.w) * 1.0f);
                    float4 _2953 = float4(_2952.x, _2952.y, _2952.z, _2782.w);
                    _2953.w = _2782.w * (1.0f - _2947.w);
                    float2 _2963 = 0.0f.xx;
                    _2963.x = mad(ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureOffsetAndAverageBrightness[_2796].w * _2932, _2779.y, _2779.x);
                    _2963.y = _2779.y * (1.0f - _2932);
                    _2780 = _2963;
                    _2783 = _2953;
                }
                else
                {
                    _2780 = _2779;
                    _2783 = _2782;
                }
            }
            float3 _2970 = _2782.xyz * View_View_PrecomputedIndirectSpecularColorScale;
            float4 _2971 = float4(_2970.x, _2970.y, _2970.z, _2782.w);
            float _2974 = _2779.x * dot(View_View_PrecomputedIndirectSpecularColorScale, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f));
            float2 _2975 = 0.0f.xx;
            _2975.x = _2974;
            float4 _3017 = 0.0f.xxxx;
            float2 _3018 = 0.0f.xx;
            float3 _3019 = 0.0f.xxx;
            [branch]
            if ((TranslucentBasePass_TranslucentBasePass_Shared_Reflection_SkyLightParameters.y > 0.0f) && true)
            {
                float3 _2996 = TranslucentBasePass_Shared_Reflection_SkyLightCubemap.SampleLevel(TranslucentBasePass_Shared_Reflection_SkyLightCubemapSampler, _2642, (TranslucentBasePass_TranslucentBasePass_Shared_Reflection_SkyLightParameters.x - 1.0f) - _2775).xyz * View_View_SkyLightColor.xyz;
                float4 _3014 = 0.0f.xxxx;
                float2 _3015 = 0.0f.xx;
                float3 _3016 = 0.0f.xxx;
                [flatten]
                if ((TranslucentBasePass_TranslucentBasePass_Shared_Reflection_SkyLightParameters.z < 1.0f) && true)
                {
                    float3 _3009 = _2970.xyz + ((_2996 * _2782.w) * 1.0f);
                    float2 _3013 = 0.0f.xx;
                    _3013.x = mad(View_SkyIrradianceEnvironmentMap[7u].x * dot(View_View_SkyLightColor.xyz, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f)), _2779.y, _2974);
                    _3014 = float4(_3009.x, _3009.y, _3009.z, _2782.w);
                    _3015 = _3013;
                    _3016 = 0.0f.xxx;
                }
                else
                {
                    _3014 = _2971;
                    _3015 = _2975;
                    _3016 = _2996 * 1.0f;
                }
                _3017 = _3014;
                _3018 = _3015;
                _3019 = _3016;
            }
            else
            {
                _3017 = _2971;
                _3018 = _2975;
                _3019 = 0.0f.xxx;
            }
            _3950 = ((_3017.xyz * lerp(1.0f, min(dot(_1047, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f)) / max(_3018.x, 9.9999997473787516355514526367188e-05f), View_View_ReflectionEnvironmentRoughnessMixingScaleBiasAndLargestWeight.z), smoothstep(0.0f, 1.0f, clamp(mad(_800, View_View_ReflectionEnvironmentRoughnessMixingScaleBiasAndLargestWeight.x, View_View_ReflectionEnvironmentRoughnessMixingScaleBiasAndLargestWeight.y), 0.0f, 1.0f)))).xyz + (_3019 * _3017.w)).xyz;
        }
        _3968 = _3950;
    }
    float3 _4275 = 0.0f.xxx;
    if (((View_View_CameraCut == 0.0f) && (TranslucentBasePass_TranslucentBasePass_SSRQuality > 0)) && _2685)
    {
        float _3993 = min(_494, 1000000.0f);
        float4 _3998 = mul(float4(_2642, 0.0f), View_View_TranslatedWorldToView);
        float _3999 = _3998.z;
        float _4004 = (_3999 < 0.0f) ? min(((-0.949999988079071044921875f) * _494) / _3999, _3993) : _3993;
        float4 _4013 = mul(float4(_491, 1.0f), View_View_TranslatedWorldToClip);
        float4 _4018 = mul(float4(_491 + (_2642 * _4004), 1.0f), View_View_TranslatedWorldToClip);
        float3 _4022 = _4013.xyz * (1.0f / _4013.w);
        float4 _4029 = _4013 + mul(float4(0.0f, 0.0f, _4004, 0.0f), View_View_ViewToClip);
        float3 _4033 = _4029.xyz * (1.0f / _4029.w);
        float3 _4034 = (_4018.xyz * (1.0f / _4018.w)) - _4022;
        float2 _4035 = _4022.xy;
        float2 _4036 = _4034.xy;
        float _4038 = 0.5f * length(_4036);
        float2 _4047 = 1.0f.xx - (max(abs(_4036 + (_4035 * _4038)) - _4038.xx, 0.0f.xx) / abs(_4036));
        float3 _4052 = _4034 * (min(_4047.x, _4047.y) / _4038);
        float _4070 = 0.0f;
        if (asuint(View_View_ViewToClip[3].w) != 0u)
        {
            _4070 = max(0.0f, (_4022.z - _4033.z) * 4.0f);
        }
        else
        {
            _4070 = max(abs(_4052.z), (_4022.z - _4033.z) * 4.0f);
        }
        float _4085 = _4070 * 0.083333335816860198974609375f;
        float3 _4086 = float3((_4052.xy * float2(0.5f, -0.5f)) * TranslucentBasePass_TranslucentBasePass_HZBUvFactorAndInvFactor.xy, _4052.z) * 0.083333335816860198974609375f;
        float3 _4088 = float3(mad(_4035, float2(0.5f, -0.5f), 0.5f.xx) * TranslucentBasePass_TranslucentBasePass_HZBUvFactorAndInvFactor.xy, _4022.z) + (_4086 * (frac(52.98291778564453125f * frac(dot(gl_FragCoord.xy + (float2(32.66500091552734375f, 11.81499958038330078125f) * float(View_View_StateFrameIndexMod8)), float2(0.067110560834407806396484375f, 0.005837149918079376220703125f)))) - 0.5f));
        bool4 _4090 = bool4(false, false, false, false);
        float4 _4093 = 0.0f.xxxx;
        uint _4099 = 0u;
        float _4101 = 0.0f;
        _4090 = _370;
        _4093 = _368;
        _4099 = 0u;
        _4101 = 0.0f;
        bool4 _4091 = bool4(false, false, false, false);
        float4 _4094 = 0.0f.xxxx;
        bool _4096 = false;
        float _4098 = 0.0f;
        float _4102 = 0.0f;
        bool4 _4161 = bool4(false, false, false, false);
        float4 _4162 = 0.0f.xxxx;
        bool _4163 = false;
        bool _4095 = false;
        float _4097 = 1.0f;
        [loop]
        for (;;)
        {
            if (_4099 < 12u)
            {
                float2 _4106 = _4088.xy;
                float2 _4107 = _4086.xy;
                float _4108 = float(_4099);
                float _4109 = _4108 + 1.0f;
                float _4112 = _4088.z;
                float _4113 = _4086.z;
                float4 _4115 = 0.0f.xxxx;
                _4115.x = mad(_4109, _4113, _4112);
                float _4116 = _4108 + 2.0f;
                _4115.y = mad(_4116, _4113, _4112);
                float _4121 = _4108 + 3.0f;
                _4115.z = mad(_4121, _4113, _4112);
                float _4126 = _4108 + 4.0f;
                _4115.w = mad(_4126, _4113, _4112);
                float _4131 = mad(0.666666686534881591796875f, _800, _4097);
                _4098 = mad(0.666666686534881591796875f, _800, _4131);
                float4 _4135 = 0.0f.xxxx;
                _4135.x = TranslucentBasePass_HZBTexture.SampleLevel(TranslucentBasePass_HZBSampler, _4106 + (_4107 * _4109), _4097).x;
                _4135.y = TranslucentBasePass_HZBTexture.SampleLevel(TranslucentBasePass_HZBSampler, _4106 + (_4107 * _4116), _4097).x;
                _4135.z = TranslucentBasePass_HZBTexture.SampleLevel(TranslucentBasePass_HZBSampler, _4106 + (_4107 * _4121), _4131).x;
                _4135.w = TranslucentBasePass_HZBTexture.SampleLevel(TranslucentBasePass_HZBSampler, _4106 + (_4107 * _4126), _4131).x;
                _4094 = _4115 - _4135;
                float4 _4148 = _4085.xxxx;
                float4 _4150 = abs(_4094 + _4148);
                _4091 = bool4(_4150.x < _4148.x, _4150.y < _4148.y, _4150.z < _4148.z, _4150.w < _4148.w);
                _4096 = (((_4095 || _4091.x) || _4091.y) || _4091.z) || _4091.w;
                [branch]
                if (_4096 || false)
                {
                    _4161 = _4091;
                    _4162 = _4094;
                    _4163 = _4096;
                    break;
                }
                _4102 = _4094.w;
                _4090 = _4091;
                _4093 = _4094;
                _4095 = _4096;
                _4097 = _4098;
                _4099 += 4u;
                _4101 = _4102;
                continue;
            }
            else
            {
                _4161 = _4090;
                _4162 = _4093;
                _4163 = _4095;
                break;
            }
        }
        float3 _4202 = 0.0f.xxx;
        [branch]
        if (_4163)
        {
            float _4176 = 0.0f;
            [flatten]
            if (_4161.z)
            {
                _4176 = _4162.y;
            }
            else
            {
                _4176 = _4162.z;
            }
            float _4184 = 0.0f;
            float _4185 = 0.0f;
            [flatten]
            if (_4161.y)
            {
                _4184 = _4162.y;
                _4185 = _4162.x;
            }
            else
            {
                _4184 = _4161.z ? _4162.z : _4162.w;
                _4185 = _4176;
            }
            float _4191 = 0.0f;
            [flatten]
            if (_4161.x)
            {
                _4191 = _4162.x;
            }
            else
            {
                _4191 = _4184;
            }
            float _4192 = _4161.x ? _4101 : _4185;
            _4202 = _4088 + (_4086 * (((_4161.x ? 0.0f : (_4161.y ? 1.0f : (_4161.z ? 2.0f : 3.0f))) + float(_4099)) + clamp(_4192 / (_4192 - _4191), 0.0f, 1.0f)));
        }
        else
        {
            _4202 = _4088 + (_4086 * float(_4099));
        }
        float3 _4274 = 0.0f.xxx;
        [branch]
        if (_4163)
        {
            float2 _4216 = (mad(mad((_4202.xy * TranslucentBasePass_TranslucentBasePass_HZBUvFactorAndInvFactor.zw).xy, float2(2.0f, -2.0f), float2(-1.0f, 1.0f)).xy, View_View_ScreenPositionScaleBias.xy, View_View_ScreenPositionScaleBias.wz).xy - View_View_ScreenPositionScaleBias.wz) / View_View_ScreenPositionScaleBias.xy;
            float4 _4223 = mul(float4(_4216, _4202.z, 1.0f), View_View_ClipToPrevClip);
            float2 _4227 = _4223.xy / _4223.w.xx;
            float2 _4234 = clamp((abs(_4216) * 5.0f) - 4.0f.xx, 0.0f.xx, 1.0f.xx);
            float2 _4241 = clamp((abs(_4227) * 5.0f) - 4.0f.xx, 0.0f.xx, 1.0f.xx);
            float3 _4258 = -min(-TranslucentBasePass_PrevSceneColor.SampleLevel(TranslucentBasePass_PrevSceneColorSampler, clamp(mad(_4227, TranslucentBasePass_TranslucentBasePass_PrevScreenPositionScaleBias.xy, TranslucentBasePass_TranslucentBasePass_PrevScreenPositionScaleBias.zw), TranslucentBasePass_TranslucentBasePass_PrevSceneColorBilinearUVMin, TranslucentBasePass_TranslucentBasePass_PrevSceneColorBilinearUVMax), 0.0f).xyz, 0.0f.xxx);
            float4 _4259 = float4(_4258.x, _4258.y, _4258.z, _368.w);
            _4259.w = 1.0f;
            float4 _4264 = _4259 * (min(clamp(1.0f - dot(_4234, _4234), 0.0f, 1.0f), clamp(1.0f - dot(_4241, _4241), 0.0f, 1.0f)) * clamp(mad(-6.599999904632568359375f, _800, 2.0f), 0.0f, 1.0f));
            _4274 = (_3968 * (1.0f - _4264.w)) + (_4264.xyz * TranslucentBasePass_TranslucentBasePass_PrevSceneColorPreExposureInv).xyz;
        }
        else
        {
            _4274 = _3968;
        }
        _4275 = _4274;
    }
    else
    {
        _4275 = _3968;
    }
    float3 _4400 = 0.0f.xxx;
    [branch]
    if (abs(dot(TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ReflectionPlane.xyz, 1.0f.xxx)) > 9.9999997473787516355514526367188e-05f)
    {
        float3 _4301 = _491 - TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionOrigin.xyz;
        float _4334 = 1.0f - clamp((_800 - 0.20000000298023223876953125f) * 10.0f, 0.0f, 1.0f);
        float _4336 = (((1.0f - clamp(mad(abs(dot(TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ReflectionPlane, float4(_491, -1.0f))), TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters.x, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters.y), 0.0f, 1.0f)) * (clamp((TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionXAxis.w - abs(dot(_4301, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionXAxis.xyz))) * TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters.x, 0.0f, 1.0f) * clamp((TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionYAxis.w - abs(dot(_4301, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionYAxis.xyz))) * TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters.x, 0.0f, 1.0f))) * clamp(mad(dot(TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ReflectionPlane.xyz, _602), TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters2.x, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters2.y), 0.0f, 1.0f)) * _4334;
        float4 _4394 = 0.0f.xxxx;
        [branch]
        if (_4336 > 0.0f)
        {
            float4 _4364 = mul(float4(mul(float4(_491 + (reflect(reflect(normalize(_491 - View_View_TranslatedWorldCameraOrigin), -TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ReflectionPlane.xyz), mul(_602, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_InverseTransposeMirrorMatrix).xyz) * TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters.z), 1.0f), View_View_TranslatedWorldToView).xyz, 1.0f), TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ProjectionWithExtraFOV[View_View_StereoPassIndex]);
            uint _4371 = 0u;
            if (TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_bIsStereo != 0u)
            {
                _4371 = uint(View_View_StereoPassIndex);
            }
            else
            {
                _4371 = 0u;
            }
            float4 _4387 = TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionTexture.SampleLevel(TranslucentBasePass_Shared_Reflection_ReflectionCubemapSampler, mad(clamp(_4364.xy / _4364.w.xx, -TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenBound, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenBound), TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenScaleBias[_4371].xy, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenScaleBias[_4371].zw), 0.0f);
            float3 _4391 = _4387.xyz * _4334;
            float4 _4392 = float4(_4391.x, _4391.y, _4391.z, 0.0f.xxxx.w);
            _4392.w = _4336 * _4387.w;
            _4394 = _4392;
        }
        else
        {
            _4394 = 0.0f.xxxx;
        }
        _4400 = _4394.xyz + (_4275 * (1.0f - _4394.w));
    }
    else
    {
        _4400 = _4275;
    }
    float4 _4402 = (float4(-1.0f, -0.0274999998509883880615234375f, -0.572000026702880859375f, 0.02199999988079071044921875f) * _800) + float4(1.0f, 0.0425000004470348358154296875f, 1.03999996185302734375f, -0.039999999105930328369140625f);
    float _4403 = _4402.x;
    float2 _4412 = (float2(-1.03999996185302734375f, 1.03999996185302734375f) * mad(min(_4403 * _4403, exp2((-9.27999973297119140625f) * clamp(dot(_602, _508), 0.0f, 1.0f))), _4403, _4402.y)) + _4402.zw;
    bool _4439 = TranslucentBasePass_TranslucentBasePass_Shared_Fog_ApplyVolumetricFog > 0.0f;
    float4 _4522 = 0.0f.xxxx;
    if (_4439)
    {
        float4 _4457 = mul(((float4(View_View_ViewTilePosition, 0.0f) + float4(_462, 0.0f)) * 2097152.0f) + float4(_492, 1.0f), View_View_RelativeWorldToClip);
        float _4458 = _4457.w;
        float4 _4504 = 0.0f.xxxx;
        float _4505 = 0.0f;
        if (_4439)
        {
            float4 _4498 = TranslucentBasePass_Shared_Fog_IntegratedLightScattering.SampleLevel(View_SharedBilinearClampedSampler, min(float3(mad((_4457.xy / _4458.xx).xy, float2(0.5f, -0.5f), 0.5f.xx), (log2(mad(_4458, View_View_VolumetricFogGridZParams.x, View_View_VolumetricFogGridZParams.y)) * View_View_VolumetricFogGridZParams.z) * View_View_VolumetricFogInvGridSize.z) * float3(View_View_VolumetricFogScreenToResourceUV, 1.0f), float3(View_View_VolumetricFogUVMax, 1.0f)), 0.0f);
            float3 _4502 = _4498.xyz * View_View_OneOverPreExposure;
            _4504 = float4(_4502.x, _4502.y, _4502.z, _4498.w);
            _4505 = TranslucentBasePass_TranslucentBasePass_Shared_Fog_VolumetricFogStartDistance;
        }
        else
        {
            _4504 = float4(0.0f, 0.0f, 0.0f, 1.0f);
            _4505 = 0.0f;
        }
        float4 _4510 = lerp(float4(0.0f, 0.0f, 0.0f, 1.0f), _4504, clamp((_494 - _4505) * 100000000.0f, 0.0f, 1.0f).xxxx);
        float _4513 = _4510.w;
        _4522 = float4(_4510.xyz + (in_var_TEXCOORD7.xyz * _4513), _4513 * in_var_TEXCOORD7.w);
    }
    else
    {
        _4522 = in_var_TEXCOORD7;
    }
    float3 _4529 = max(lerp(0.0f.xxx, Material_Material_PreshaderBuffer[2].xyz, Material_Material_PreshaderBuffer[1].w.xxx), 0.0f.xxx);
    float _4614 = 0.0f;
    float3 _4615 = 0.0f.xxx;
    [branch]
    if (View_View_OutOfBoundsMask > 0.0f)
    {
        uint _4555 = _509 + 31u;
        float3 _4564 = abs(((View_View_ViewTilePosition - Scene_GPUScene_GPUScenePrimitiveSceneData[_509 + 1u].xyz) * 2097152.0f) + (_492 - Scene_GPUScene_GPUScenePrimitiveSceneData[_509 + 18u].xyz));
        float3 _4565 = float3(Scene_GPUScene_GPUScenePrimitiveSceneData[_509 + 17u].w, Scene_GPUScene_GPUScenePrimitiveSceneData[_509 + 24u].w, Scene_GPUScene_GPUScenePrimitiveSceneData[_509 + 25u].w) + 1.0f.xxx;
        float _4612 = 0.0f;
        float3 _4613 = 0.0f.xxx;
        if (any(bool3(_4564.x > _4565.x, _4564.y > _4565.y, _4564.z > _4565.z)))
        {
            float3 _4592 = View_View_ViewTilePosition * 0.57700002193450927734375f.xxx;
            float3 _4593 = _492 * 0.57700002193450927734375f.xxx;
            float3 _4608 = frac(mad((_4593.x + _4593.y) + _4593.z, 0.00200000009499490261077880859375f, frac(((_4592.x + _4592.y) + _4592.z) * 4194.30419921875f))).xxx;
            _4612 = 1.0f;
            _4613 = lerp(float3(1.0f, 1.0f, 0.0f), float3(0.0f, 1.0f, 1.0f), float3(bool3(_4608.x > 0.5f.xxx.x, _4608.y > 0.5f.xxx.y, _4608.z > 0.5f.xxx.z)));
        }
        else
        {
            float _4590 = 0.0f;
            float3 _4591 = 0.0f.xxx;
            if (Scene_GPUScene_GPUScenePrimitiveSceneData[_4555].x > 0.0f)
            {
                float3 _4575 = abs(_491 - in_var_TEXCOORD9);
                float _4585 = 1.0f - clamp(abs(max(_4575.x, max(_4575.y, _4575.z)) - Scene_GPUScene_GPUScenePrimitiveSceneData[_4555].x) * 20.0f, 0.0f, 1.0f);
                _4590 = float(int(sign(_4585)));
                _4591 = float3(1.0f, 0.0f, 1.0f) * _4585;
            }
            else
            {
                _4590 = 1.0f;
                _4591 = _4529;
            }
            _4612 = _4590;
            _4613 = _4591;
        }
        _4614 = _4612;
        _4615 = _4613;
    }
    else
    {
        _4614 = 1.0f;
        _4615 = _4529;
    }
    float4 _4625 = float4(((mad(_1047 * _828, max(1.0f.xxx, ((((((_794 * 2.040400028228759765625f) - 0.3323999941349029541015625f.xxx) * 1.0f) + ((_794 * (-4.79510021209716796875f)) + 0.6417000293731689453125f.xxx)) * 1.0f) + ((_794 * 2.755199909210205078125f) + 0.69029998779296875f.xxx)) * 1.0f), lerp(mad((_4400 * ((_830 * _4412.x) + (clamp(50.0f * _830.y, 0.0f, 1.0f) * _4412.y).xxx)) * 1.0f, max(1.0f.xxx, ((((((_830 * 2.040400028228759765625f) - 0.3323999941349029541015625f.xxx) * 1.0f) + ((_830 * (-4.79510021209716796875f)) + 0.6417000293731689453125f.xxx)) * 1.0f) + ((_830 * 2.755199909210205078125f) + 0.69029998779296875f.xxx)) * 1.0f), float4(_2633.x ? 0.0f.xxxx.x : _2188.x, _2633.y ? 0.0f.xxxx.y : _2188.y, _2633.z ? 0.0f.xxxx.z : _2188.z, _2633.w ? 0.0f.xxxx.w : _2188.w).xyz + float4(_2633.x ? 0.0f.xxxx.x : _2191.x, _2633.y ? 0.0f.xxxx.y : _2191.y, _2633.z ? 0.0f.xxxx.z : _2191.z, _2633.w ? 0.0f.xxxx.w : _2191.w).xyz), _828 + (_830 * 0.449999988079071044921875f), View_View_UnlitViewmodeMask.xxx)) + _4615) * _4522.w) + _4522.xyz, _4614);
    float3 _4631 = min((_4625.xyz * View_View_PreExposure).xyz, 32256.0f.xxx);
    out_var_SV_Target0 = float4(_4631.x, _4631.y, _4631.z, _4625.w);
}

SPIRV_Cross_Output main(SPIRV_Cross_Input stage_input)
{
    gl_FragCoord = stage_input.gl_FragCoord;
    gl_FragCoord.w = 1.0 / gl_FragCoord.w;
    gl_FrontFacing = stage_input.gl_FrontFacing;
    in_var_TEXCOORD10_centroid = stage_input.in_var_TEXCOORD10_centroid;
    in_var_TEXCOORD11_centroid = stage_input.in_var_TEXCOORD11_centroid;
    in_var_TEXCOORD0 = stage_input.in_var_TEXCOORD0;
    in_var_TEXCOORD4 = stage_input.in_var_TEXCOORD4;
    in_var_PRIMITIVE_ID = stage_input.in_var_PRIMITIVE_ID;
    in_var_LIGHTMAP_ID = stage_input.in_var_LIGHTMAP_ID;
    in_var_TEXCOORD7 = stage_input.in_var_TEXCOORD7;
    in_var_TEXCOORD9 = stage_input.in_var_TEXCOORD9;
    frag_main();
    SPIRV_Cross_Output stage_output;
    stage_output.out_var_SV_Target0 = out_var_SV_Target0;
    return stage_output;
}
