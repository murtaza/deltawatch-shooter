#pragma warning(disable : 3571) // pow() intrinsic suggested to be used with abs()
static uint _343 = 0u;
static float3 _369 = 0.0f.xxx;
static float4 _370 = 0.0f.xxxx;
static float4x4 _371 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
static bool4 _372 = bool4(false, false, false, false);
static float _374 = 0.0f;
static float4 _375 = 0.0f.xxxx;
static float2 _376 = 0.0f.xx;

cbuffer View
{
    row_major float4x4 View_View_TranslatedWorldToClip : packoffset(c0);
    row_major float4x4 View_View_RelativeWorldToClip : packoffset(c4);
    row_major float4x4 View_View_TranslatedWorldToView : packoffset(c12);
    row_major float4x4 View_View_ViewToClip : packoffset(c28);
    row_major float4x4 View_View_SVPositionToTranslatedWorld : packoffset(c44);
    float3 View_View_ViewTilePosition : packoffset(c60);
    float3 View_View_MatrixTilePosition : packoffset(c61);
    float3 View_View_ViewForward : packoffset(c62);
    float4 View_View_InvDeviceZToWorldZTransform : packoffset(c67);
    float4 View_View_ScreenPositionScaleBias : packoffset(c68);
    float3 View_View_RelativeWorldCameraOrigin : packoffset(c69);
    float3 View_View_TranslatedWorldCameraOrigin : packoffset(c70);
    float3 View_View_RelativePreViewTranslation : packoffset(c72);
    row_major float4x4 View_View_ClipToPrevClip : packoffset(c113);
    float4 View_View_ViewRectMin : packoffset(c124);
    float4 View_View_ViewSizeAndInvSize : packoffset(c125);
    float4 View_View_LightProbeSizeRatioAndInvSizeRatio : packoffset(c127);
    float View_View_PreExposure : packoffset(c132.z);
    float View_View_OneOverPreExposure : packoffset(c132.w);
    float4 View_View_DiffuseOverrideParameter : packoffset(c133);
    float4 View_View_SpecularOverrideParameter : packoffset(c134);
    float4 View_View_NormalOverrideParameter : packoffset(c135);
    float2 View_View_RoughnessOverrideParameter : packoffset(c136);
    float View_View_OutOfBoundsMask : packoffset(c137);
    float View_View_CullingSign : packoffset(c138.w);
    float View_View_MaterialTextureMipBias : packoffset(c140);
    uint View_View_StateFrameIndexMod8 : packoffset(c141.y);
    float View_View_CameraCut : packoffset(c142.y);
    float View_View_UnlitViewmodeMask : packoffset(c142.z);
    float3 View_View_PrecomputedIndirectLightingColorScale : packoffset(c155);
    float3 View_View_PrecomputedIndirectSpecularColorScale : packoffset(c156);
    float View_View_RenderingReflectionCaptureMask : packoffset(c179.w);
    float View_View_SkyLightApplyPrecomputedBentNormalShadowingFlag : packoffset(c182.y);
    float4 View_View_SkyLightColor : packoffset(c183);
    float View_View_ReflectionCubemapMaxMip : packoffset(c192.z);
    float3 View_View_ReflectionEnvironmentRoughnessMixingScaleBiasAndLargestWeight : packoffset(c194);
    int View_View_StereoPassIndex : packoffset(c194.w);
    float3 View_View_VolumetricFogInvGridSize : packoffset(c225);
    float3 View_View_VolumetricFogGridZParams : packoffset(c226);
    float2 View_View_VolumetricFogScreenToResourceUV : packoffset(c229);
    float2 View_View_VolumetricFogUVMax : packoffset(c229.z);
    float View_View_MinRoughness : packoffset(c243.z);
};

StructuredBuffer<float4> View_SkyIrradianceEnvironmentMap;
StructuredBuffer<float4> Scene_GPUScene_GPUScenePrimitiveSceneData;
StructuredBuffer<float4> Scene_GPUScene_GPUSceneLightmapData;
cbuffer TranslucentBasePass
{
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumLocalLights : packoffset(c0);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumReflectionCaptures : packoffset(c0.y);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_HasDirectionalLight : packoffset(c0.z);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumGridCells : packoffset(c0.w);
    int3 TranslucentBasePass_TranslucentBasePass_Shared_Forward_CulledGridSize : packoffset(c1);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridPixelSizeShift : packoffset(c2);
    float3 TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridZParams : packoffset(c3);
    float3 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDirection : packoffset(c4);
    float TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius : packoffset(c4.w);
    float3 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightColor : packoffset(c5);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowMapChannelMask : packoffset(c6);
    float2 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDistanceFadeMAD : packoffset(c6.z);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumDirectionalLightCascades : packoffset(c7);
    int TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightVSM : packoffset(c7.y);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_Forward_CascadeEndDepths : packoffset(c8);
    row_major float4x4 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightTranslatedWorldToShadowMatrix[4] : packoffset(c9);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapMinMax[4] : packoffset(c25);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapAtlasBufferSize : packoffset(c29);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightUseStaticShadowing : packoffset(c30.y);
    row_major float4x4 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightTranslatedWorldToStaticShadow : packoffset(c33);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectLightingShowFlag : packoffset(c37);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_Reflection_SkyLightParameters : packoffset(c90);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ReflectionPlane : packoffset(c95);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionOrigin : packoffset(c96);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionXAxis : packoffset(c97);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionYAxis : packoffset(c98);
    row_major float3x4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_InverseTransposeMirrorMatrix : packoffset(c99);
    float3 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters : packoffset(c102);
    float2 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters2 : packoffset(c103);
    row_major float4x4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ProjectionWithExtraFOV[2] : packoffset(c104);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenScaleBias[2] : packoffset(c112);
    float2 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenBound : packoffset(c114);
    uint TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_bIsStereo : packoffset(c114.z);
    float TranslucentBasePass_TranslucentBasePass_Shared_Fog_ApplyVolumetricFog : packoffset(c124.w);
    float TranslucentBasePass_TranslucentBasePass_Shared_Fog_VolumetricFogStartDistance : packoffset(c125);
    uint TranslucentBasePass_TranslucentBasePass_Shared_UseBasePassSkylight : packoffset(c140);
    float4 TranslucentBasePass_TranslucentBasePass_HZBUvFactorAndInvFactor : packoffset(c161);
    float4 TranslucentBasePass_TranslucentBasePass_PrevScreenPositionScaleBias : packoffset(c162);
    float2 TranslucentBasePass_TranslucentBasePass_PrevSceneColorBilinearUVMin : packoffset(c163);
    float2 TranslucentBasePass_TranslucentBasePass_PrevSceneColorBilinearUVMax : packoffset(c163.z);
    float TranslucentBasePass_TranslucentBasePass_PrevSceneColorPreExposureInv : packoffset(c164);
    int TranslucentBasePass_TranslucentBasePass_SSRQuality : packoffset(c164.y);
    float TranslucentBasePass_TranslucentBasePass_ReprojectionRadiusScale : packoffset(c171);
    float TranslucentBasePass_TranslucentBasePass_InvClipmapFadeSize : packoffset(c171.w);
    uint TranslucentBasePass_TranslucentBasePass_RadianceProbeClipmapResolution : packoffset(c172.z);
    uint TranslucentBasePass_TranslucentBasePass_NumRadianceProbeClipmaps : packoffset(c172.w);
    uint TranslucentBasePass_TranslucentBasePass_RadianceProbeResolution : packoffset(c173);
    uint TranslucentBasePass_TranslucentBasePass_FinalProbeResolution : packoffset(c173.y);
    uint TranslucentBasePass_TranslucentBasePass_FinalRadianceAtlasMaxMip : packoffset(c173.z);
    float4 TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[6] : packoffset(c178);
    float4 TranslucentBasePass_TranslucentBasePass_PaddedWorldPositionToRadianceProbeCoordBias[6] : packoffset(c184);
    float4 TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[6] : packoffset(c190);
    float2 TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution : packoffset(c196);
    uint TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask : packoffset(c198);
    uint TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionDivideShift : packoffset(c198.y);
    uint TranslucentBasePass_TranslucentBasePass_Enabled : packoffset(c200.z);
    float TranslucentBasePass_TranslucentBasePass_RelativeDepthThreshold : packoffset(c200.w);
    float TranslucentBasePass_TranslucentBasePass_SpecularScale : packoffset(c201);
    float TranslucentBasePass_TranslucentBasePass_Contrast : packoffset(c201.y);
    float3 TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridZParams : packoffset(c205);
    int3 TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridSize : packoffset(c206);
};

StructuredBuffer<float4> TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer;
StructuredBuffer<uint> TranslucentBasePass_Shared_Forward_NumCulledLightsGrid;
StructuredBuffer<float4> TranslucentBasePass_ProbeWorldOffset;
cbuffer ReflectionCaptureSM5
{
    float4 ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[341] : packoffset(c0);
    float4 ReflectionCaptureSM5_ReflectionCaptureSM5_TilePosition[341] : packoffset(c341);
    float4 ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureProperties[341] : packoffset(c682);
    float4 ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureOffsetAndAverageBrightness[341] : packoffset(c1023);
    row_major float4x4 ReflectionCaptureSM5_ReflectionCaptureSM5_BoxTransform[341] : packoffset(c1364);
    float4 ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[341] : packoffset(c2728);
};

ByteAddressBuffer VirtualShadowMap_ProjectionData;
StructuredBuffer<uint> VirtualShadowMap_PageTable;
cbuffer Material
{
    float4 Material_Material_PreshaderBuffer[28] : packoffset(c0);
};

SamplerState View_SharedPointClampedSampler;
SamplerState View_SharedBilinearClampedSampler;
Texture2D<float4> TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapAtlas;
SamplerState TranslucentBasePass_Shared_Forward_ShadowmapSampler;
Texture2D<float4> TranslucentBasePass_Shared_Forward_DirectionalLightStaticShadowmap;
SamplerState TranslucentBasePass_Shared_Forward_StaticShadowmapSampler;
Buffer<uint4> TranslucentBasePass_Shared_Forward_CulledLightDataGrid16Bit;
TextureCube<float4> TranslucentBasePass_Shared_Reflection_SkyLightCubemap;
SamplerState TranslucentBasePass_Shared_Reflection_SkyLightCubemapSampler;
TextureCubeArray<float4> TranslucentBasePass_Shared_Reflection_ReflectionCubemap;
SamplerState TranslucentBasePass_Shared_Reflection_ReflectionCubemapSampler;
Texture2D<float4> TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionTexture;
Texture3D<float4> TranslucentBasePass_Shared_Fog_IntegratedLightScattering;
Texture3D<uint4> TranslucentBasePass_RadianceProbeIndirectionTexture;
Texture2D<float4> TranslucentBasePass_RadianceCacheFinalRadianceAtlas;
Texture2D<float4> TranslucentBasePass_Radiance;
Texture2D<float4> TranslucentBasePass_SceneDepth;
Texture3D<float4> TranslucentBasePass_TranslucencyGIVolumeHistory0;
Texture3D<float4> TranslucentBasePass_TranslucencyGIVolumeHistory1;
SamplerState TranslucentBasePass_TranslucencyGIVolumeSampler;
Texture2D<float4> TranslucentBasePass_HZBTexture;
SamplerState TranslucentBasePass_HZBSampler;
Texture2D<float4> TranslucentBasePass_PrevSceneColor;
SamplerState TranslucentBasePass_PrevSceneColorSampler;
Texture2D<float4> LightmapResourceCluster_LightMapTexture;
Texture2D<float4> LightmapResourceCluster_SkyOcclusionTexture;
Texture2D<float4> LightmapResourceCluster_StaticShadowTexture;
SamplerState LightmapResourceCluster_LightMapSampler;
Texture2DArray<uint4> VirtualShadowMap_PhysicalPagePool;
Texture2D<float4> Material_Texture2D_0;
SamplerState Material_Texture2D_0Sampler;
Texture2D<float4> Material_Texture2D_1;
SamplerState Material_Texture2D_1Sampler;
Texture2D<float4> Material_Texture2D_2;
SamplerState Material_Texture2D_2Sampler;
Texture2D<float4> Material_Texture2D_3;
SamplerState Material_Texture2D_3Sampler;
Texture2D<float4> Material_Texture2D_4;
SamplerState Material_Texture2D_4Sampler;
Texture2D<float4> Material_Texture2D_5;
SamplerState Material_Texture2D_5Sampler;
Texture2D<float4> Material_Texture2D_6;
SamplerState Material_Texture2D_6Sampler;

static float4 gl_FragCoord;
static bool gl_FrontFacing;
static float4 in_var_TEXCOORD10_centroid;
static float4 in_var_TEXCOORD11_centroid;
static float4 in_var_TEXCOORD0[1];
static float4 in_var_TEXCOORD4;
static uint in_var_PRIMITIVE_ID;
static uint in_var_LIGHTMAP_ID;
static float4 in_var_TEXCOORD7;
static float3 in_var_TEXCOORD9;
static float4 out_var_SV_Target0;

struct SPIRV_Cross_Input
{
    float4 in_var_TEXCOORD10_centroid : TEXCOORD10_centroid;
    float4 in_var_TEXCOORD11_centroid : TEXCOORD11_centroid;
    float4 in_var_TEXCOORD0[1] : TEXCOORD0;
    float4 in_var_TEXCOORD4 : TEXCOORD4;
    nointerpolation uint in_var_PRIMITIVE_ID : PRIMITIVE_ID;
    nointerpolation uint in_var_LIGHTMAP_ID : LIGHTMAP_ID;
    float4 in_var_TEXCOORD7 : TEXCOORD7;
    float3 in_var_TEXCOORD9 : TEXCOORD9;
    float4 gl_FragCoord : SV_Position;
    bool gl_FrontFacing : SV_IsFrontFace;
};

struct SPIRV_Cross_Output
{
    float4 out_var_SV_Target0 : SV_Target0;
};

uint spvPackHalf2x16(float2 value)
{
    uint2 Packed = f32tof16(value);
    return Packed.x | (Packed.y << 16);
}

float2 spvUnpackHalf2x16(uint value)
{
    return f16tof32(uint2(value & 0xffff, value >> 16));
}

void frag_main()
{
    float _426 = 1.0f / gl_FragCoord.w;
    float3 _464 = -View_View_MatrixTilePosition;
    float3 _465 = -View_View_ViewTilePosition;
    float3x3 _474 = float3x3(in_var_TEXCOORD10_centroid.xyz, cross(in_var_TEXCOORD11_centroid.xyz, in_var_TEXCOORD10_centroid.xyz) * in_var_TEXCOORD11_centroid.w, in_var_TEXCOORD11_centroid.xyz);
    float2 _477 = gl_FragCoord.xy - View_View_ViewRectMin.xy;
    float4 _484 = float4(mad(_477, View_View_ViewSizeAndInvSize.zw, (-0.5f).xx) * float2(2.0f, -2.0f), _374, 1.0f) * _426;
    float4 _489 = mul(float4(gl_FragCoord.xyz, 1.0f), View_View_SVPositionToTranslatedWorld);
    float3 _493 = _489.xyz / _489.w.xxx;
    float3 _494 = _493 - View_View_RelativePreViewTranslation;
    float _496 = _484.w;
    float2 _501 = mad(_484.xy / _496.xx, View_View_ScreenPositionScaleBias.xy, View_View_ScreenPositionScaleBias.wz);
    float3 _510 = 0.0f.xxx;
    if (View_View_ViewToClip[3].w >= 1.0f)
    {
        _510 = -View_View_ViewForward;
    }
    else
    {
        _510 = normalize(-_493);
    }
    uint _511 = in_var_PRIMITIVE_ID * 41u;
    float4 _528 = Material_Texture2D_0.SampleBias(Material_Texture2D_0Sampler, float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y), View_View_MaterialTextureMipBias);
    float _529 = _528.x;
    float2 _535 = mul(_474, _510).xy;
    float2 _537 = mad(_535, mad(0.0500000007450580596923828125f, _529 * Material_Material_PreshaderBuffer[1].x, -0.02500000037252902984619140625f).xx, float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y));
    float2 _543 = mad(Material_Texture2D_1.SampleBias(Material_Texture2D_1Sampler, _537, View_View_MaterialTextureMipBias).xy, 2.0f.xx, (-1.0f).xx);
    float2 _554 = float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y) * Material_Material_PreshaderBuffer[1].y.xx;
    float2 _557 = Material_Material_PreshaderBuffer[1].z.xx;
    float2 _564 = mad(Material_Texture2D_2.SampleBias(Material_Texture2D_2Sampler, _554 * _557, View_View_MaterialTextureMipBias).xy, 2.0f.xx, (-1.0f).xx);
    float2 _577 = mad(Material_Texture2D_2.SampleBias(Material_Texture2D_2Sampler, (_554 * 1.618000030517578125f.xx) * _557, View_View_MaterialTextureMipBias).xy, 2.0f.xx, (-1.0f).xx);
    float3 _604 = normalize(mul(normalize((lerp(float4(_543, sqrt(clamp(1.0f - dot(_543, _543), 0.0f, 1.0f)), 1.0f).xyz, (float4(_564, sqrt(clamp(1.0f - dot(_564, _564), 0.0f, 1.0f)), 1.0f).xyz + float4(_577, sqrt(clamp(1.0f - dot(_577, _577), 0.0f, 1.0f)), 1.0f).xyz) * float3(1.0f, 1.0f, 0.5f), Material_Texture2D_3.SampleBias(Material_Texture2D_3Sampler, float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y), View_View_MaterialTextureMipBias).x.xxx) * View_View_NormalOverrideParameter.w) + View_View_NormalOverrideParameter.xyz), _474)) * ((View_View_CullingSign * (((asuint(Scene_GPUScene_GPUScenePrimitiveSceneData[_511].x) & 64u) != 0u) ? (-1.0f) : 1.0f)) * float(gl_FrontFacing ? 1 : (-1)));
    float3 _632 = Material_Texture2D_5.SampleBias(Material_Texture2D_5Sampler, _537, View_View_MaterialTextureMipBias).xyz * Material_Material_PreshaderBuffer[4].w.xxx;
    float _635 = _632.x;
    float _639 = _632.y;
    float _643 = _632.z;
    float3 _647 = float3((_635 <= 0.0f) ? 0.0f : pow(_635, Material_Material_PreshaderBuffer[5].x), (_639 <= 0.0f) ? 0.0f : pow(_639, Material_Material_PreshaderBuffer[5].x), (_643 <= 0.0f) ? 0.0f : pow(_643, Material_Material_PreshaderBuffer[5].x));
    float2 _697 = float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y) * Material_Material_PreshaderBuffer[13].w.xx;
    float2 _703 = mad(_535, mad(0.0500000007450580596923828125f, _529 * Material_Material_PreshaderBuffer[14].x, -0.02500000037252902984619140625f).xx, _697);
    float3 _720 = Material_Texture2D_5.SampleBias(Material_Texture2D_5Sampler, _703, View_View_MaterialTextureMipBias).xyz * Material_Material_PreshaderBuffer[16].w.xxx;
    float _723 = _720.x;
    float _727 = _720.y;
    float _731 = _720.z;
    float3 _735 = float3((_723 <= 0.0f) ? 0.0f : pow(_723, Material_Material_PreshaderBuffer[17].x), (_727 <= 0.0f) ? 0.0f : pow(_727, Material_Material_PreshaderBuffer[17].x), (_731 <= 0.0f) ? 0.0f : pow(_731, Material_Material_PreshaderBuffer[17].x));
    float3 _796 = clamp(lerp(mad(Material_Texture2D_4.SampleBias(Material_Texture2D_4Sampler, _537, View_View_MaterialTextureMipBias).xyz * Material_Material_PreshaderBuffer[2].w.xxx, Material_Material_PreshaderBuffer[4].xyz, lerp(_647, dot(_647, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f)).xxx, Material_Material_PreshaderBuffer[5].y.xxx) * Material_Material_PreshaderBuffer[7].xyz) + mad(Material_Material_PreshaderBuffer[10].xyz, (Material_Texture2D_6.SampleBias(Material_Texture2D_6Sampler, mad(_535, mad(0.0500000007450580596923828125f, Material_Material_PreshaderBuffer[9].x, -0.02500000037252902984619140625f).xx, float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y)), View_View_MaterialTextureMipBias).y * Material_Material_PreshaderBuffer[9].y).xxx, Material_Material_PreshaderBuffer[13].xyz * (Material_Texture2D_6.SampleBias(Material_Texture2D_6Sampler, mad(_535, mad(0.0500000007450580596923828125f, Material_Material_PreshaderBuffer[12].x, -0.02500000037252902984619140625f).xx, float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y)), View_View_MaterialTextureMipBias).x * Material_Material_PreshaderBuffer[12].y).xxx), mad(Material_Texture2D_4.SampleBias(Material_Texture2D_4Sampler, _703, View_View_MaterialTextureMipBias).xyz * Material_Material_PreshaderBuffer[14].y.xxx, Material_Material_PreshaderBuffer[16].xyz, lerp(_735, dot(_735, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f)).xxx, Material_Material_PreshaderBuffer[17].y.xxx) * Material_Material_PreshaderBuffer[19].xyz) + mad(Material_Material_PreshaderBuffer[22].xyz, (Material_Texture2D_6.SampleBias(Material_Texture2D_6Sampler, mad(_535, mad(0.0500000007450580596923828125f, Material_Material_PreshaderBuffer[21].x, -0.02500000037252902984619140625f).xx, _697), View_View_MaterialTextureMipBias).y * Material_Material_PreshaderBuffer[21].y).xxx, Material_Material_PreshaderBuffer[25].xyz * (Material_Texture2D_6.SampleBias(Material_Texture2D_6Sampler, mad(_535, mad(0.0500000007450580596923828125f, Material_Material_PreshaderBuffer[24].x, -0.02500000037252902984619140625f).xx, _697), View_View_MaterialTextureMipBias).x * Material_Material_PreshaderBuffer[24].y).xxx), (length(((View_View_ViewTilePosition - View_View_ViewTilePosition) * 2097152.0f) + (_494 - View_View_RelativeWorldCameraOrigin)) * Material_Material_PreshaderBuffer[26].x).xxx), 0.0f.xxx, 1.0f.xxx);
    float _797 = clamp(Material_Material_PreshaderBuffer[26].y, 0.0f, 1.0f);
    float _802 = mad(clamp(Material_Material_PreshaderBuffer[26].w, 0.0f, 1.0f), View_View_RoughnessOverrideParameter.y, View_View_RoughnessOverrideParameter.x);
    float4 _807 = LightmapResourceCluster_StaticShadowTexture.Sample(LightmapResourceCluster_LightMapSampler, in_var_TEXCOORD4.zw);
    uint _808 = in_var_LIGHTMAP_ID * 15u;
    uint _809 = _808 + 1u;
    float4 _815 = clamp(mad(_807, Scene_GPUScene_GPUSceneLightmapData[_809], (Scene_GPUScene_GPUSceneLightmapData[_809] * (-0.5f)) + 0.5f.xxxx), 0.0f.xxxx, 1.0f.xxxx);
    float4 _819 = (Scene_GPUScene_GPUSceneLightmapData[_808] * _815) * _815;
    float3 _832 = ((_796 - (_796 * _797)) * View_View_DiffuseOverrideParameter.w) + View_View_DiffuseOverrideParameter.xyz;
    float3 _839 = (lerp((0.07999999821186065673828125f * clamp(Material_Material_PreshaderBuffer[26].z, 0.0f, 1.0f)).xxx, _796, _797.xxx) * View_View_SpecularOverrideParameter.w) + View_View_SpecularOverrideParameter.xyz;
    bool _842 = View_View_RenderingReflectionCaptureMask != 0.0f;
    float3 _847 = 0.0f.xxx;
    if (_842)
    {
        _847 = _832 + (_839 * 0.449999988079071044921875f);
    }
    else
    {
        _847 = _832;
    }
    bool3 _848 = _842.xxx;
    float3 _849 = float3(_848.x ? 0.0f.xxx.x : _839.x, _848.y ? 0.0f.xxx.y : _839.y, _848.z ? 0.0f.xxx.z : _839.z);
    float4 _855 = LightmapResourceCluster_LightMapTexture.Sample(LightmapResourceCluster_LightMapSampler, in_var_TEXCOORD4.xy * float2(1.0f, 0.5f));
    float4 _857 = LightmapResourceCluster_LightMapTexture.Sample(LightmapResourceCluster_LightMapSampler, mad(in_var_TEXCOORD4.xy, float2(1.0f, 0.5f), float2(0.0f, 0.5f)));
    uint _862 = _808 + 4u;
    uint _866 = _808 + 6u;
    float3 _871 = _855.xyz;
    float _887 = _604.y;
    float3 _1063 = 0.0f.xxx;
    if (TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridSize.z > 0)
    {
        float4 _996 = mul(((float4(View_View_ViewTilePosition, 0.0f) + float4(_464, 0.0f)) * 2097152.0f) + float4(_494, 1.0f), View_View_RelativeWorldToClip);
        float _997 = _996.w;
        float3 _1016 = float3(mad((_996.xy / _997.xx).xy, float2(0.5f, -0.5f), 0.5f.xx), (log2(mad(_997, TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridZParams.x, TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridZParams.y)) * TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridZParams.z) / float(TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridSize.z));
        float4 _1020 = TranslucentBasePass_TranslucencyGIVolumeHistory0.SampleLevel(TranslucentBasePass_TranslucencyGIVolumeSampler, _1016, 0.0f);
        float3 _1021 = _1020.xyz;
        float3 _1025 = TranslucentBasePass_TranslucencyGIVolumeHistory1.SampleLevel(TranslucentBasePass_TranslucencyGIVolumeSampler, _1016, 0.0f).xyz;
        float4 _1027 = 0.0f.xxxx;
        _1027.x = _1020.x;
        float4 _1029 = 0.0f.xxxx;
        _1029.x = _1020.y;
        float4 _1031 = 0.0f.xxxx;
        _1031.x = _1020.z;
        float3 _1035 = _1021 / (dot(_1021, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f)) + 9.9999997473787516355514526367188e-06f).xxx;
        float3 _1037 = _1025 * _1035.x;
        float3 _1040 = _1025 * _1035.y;
        float3 _1043 = _1025 * _1035.z;
        float4 _1046 = 0.0f.xxxx;
        _1046.y = (-0.48860299587249755859375f) * _887;
        _1046.z = 0.48860299587249755859375f * _604.z;
        _1046.w = (-0.48860299587249755859375f) * _604.x;
        _1046.x = 0.886227548122406005859375f;
        float3 _1053 = _1046.yzw * 2.094395160675048828125f;
        float4 _1054 = float4(_1046.x, _1053.x, _1053.y, _1053.z);
        float3 _1056 = 0.0f.xxx;
        _1056.x = dot(float4(_1027.x, _1037.x, _1037.y, _1037.z), _1054);
        _1056.y = dot(float4(_1029.x, _1040.x, _1040.y, _1040.z), _1054);
        _1056.z = dot(float4(_1031.x, _1043.x, _1043.y, _1043.z), _1054);
        _1063 = max(0.0f.xxx, _1056) * 0.3183098733425140380859375f.xxx;
    }
    else
    {
        float3 _980 = 0.0f.xxx;
        if (TranslucentBasePass_TranslucentBasePass_Shared_UseBasePassSkylight > 0u)
        {
            float _932 = 0.0f;
            float _933 = 0.0f;
            float3 _934 = 0.0f.xxx;
            [branch]
            if (View_View_SkyLightApplyPrecomputedBentNormalShadowingFlag != 0.0f)
            {
                float4 _915 = LightmapResourceCluster_SkyOcclusionTexture.Sample(LightmapResourceCluster_LightMapSampler, in_var_TEXCOORD4.xy);
                float _919 = _915.w;
                float3 _922 = normalize(((_915.xyz * 2.0f) - 1.0f.xxx).xyz);
                float _926 = mad(mad(_919, _919, -1.0f), mad(-_919, _919, 1.0f), 1.0f);
                _932 = lerp(clamp(dot(_922, _604), 0.0f, 1.0f), 1.0f, _926);
                _933 = _919 * _919;
                _934 = lerp(_922, _604, _926.xxx);
            }
            else
            {
                _932 = 1.0f;
                _933 = 1.0f;
                _934 = _604;
            }
            float4 _938 = float4(_934, 1.0f);
            float3 _942 = 0.0f.xxx;
            _942.x = dot(View_SkyIrradianceEnvironmentMap[0u], _938);
            _942.y = dot(View_SkyIrradianceEnvironmentMap[1u], _938);
            _942.z = dot(View_SkyIrradianceEnvironmentMap[2u], _938);
            float4 _953 = _938.xyzz * _938.yzzx;
            float3 _957 = 0.0f.xxx;
            _957.x = dot(View_SkyIrradianceEnvironmentMap[3u], _953);
            _957.y = dot(View_SkyIrradianceEnvironmentMap[4u], _953);
            _957.z = dot(View_SkyIrradianceEnvironmentMap[5u], _953);
            _980 = (max(0.0f.xxx, (_942 + _957) + (View_SkyIrradianceEnvironmentMap[6u].xyz * mad(_934.x, _934.x, -(_934.y * _934.y)))) * View_View_SkyLightColor.xyz) * (_933 * _932);
        }
        else
        {
            _980 = 0.0f.xxx;
        }
        _1063 = _980;
    }
    float3 _1064 = mad(mad(_871 * _871, Scene_GPUScene_GPUSceneLightmapData[_862].xyz, Scene_GPUScene_GPUSceneLightmapData[_866].xyz) * ((exp2(mad(_855.w + mad(_857.w, 0.0039215688593685626983642578125f, -0.00196078442968428134918212890625f), Scene_GPUScene_GPUSceneLightmapData[_862].w, Scene_GPUScene_GPUSceneLightmapData[_866].w)) - 0.0185813605785369873046875f) * max(0.0f, dot(mad(_857, Scene_GPUScene_GPUSceneLightmapData[_808 + 5u], Scene_GPUScene_GPUSceneLightmapData[_808 + 7u]), float4(_887, _604.zx, 1.0f)))), View_View_PrecomputedIndirectLightingColorScale, _1063);
    uint2 _1104 = uint2(_477 * View_View_LightProbeSizeRatioAndInvSizeRatio.zw) >> (TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridPixelSizeShift.xx & uint2(31u, 31u));
    uint _1114 = (((min(uint(max(0.0f, log2(mad(_426, TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridZParams.x, TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridZParams.y)) * TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridZParams.z)), uint(TranslucentBasePass_TranslucentBasePass_Shared_Forward_CulledGridSize.z - 1)) * uint(TranslucentBasePass_TranslucentBasePass_Shared_Forward_CulledGridSize.y)) + _1104.y) * uint(TranslucentBasePass_TranslucentBasePass_Shared_Forward_CulledGridSize.x)) + _1104.x;
    uint _1117 = asuint(Scene_GPUScene_GPUScenePrimitiveSceneData[_511].x);
    uint _1130 = (uint((_1117 & 2048u) != 0u) | (uint((_1117 & 4096u) != 0u) << 1u)) | (uint((_1117 & 8192u) != 0u) << 2u);
    float4 _2191 = 0.0f.xxxx;
    float4 _2192 = 0.0f.xxxx;
    float4 _2193 = 0.0f.xxxx;
    [branch]
    if (TranslucentBasePass_TranslucentBasePass_Shared_Forward_HasDirectionalLight != 0u)
    {
        float4 _1155 = float4(_374, float((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowMapChannelMask & 2u) >> 1u), float((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowMapChannelMask & 4u) >> 2u), float((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowMapChannelMask & 8u) >> 3u));
        _1155.x = 1.0f;
        float _1198 = 0.0f;
        [branch]
        if (TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightUseStaticShadowing > 0u)
        {
            float4 _1170 = mul(float4(_493, 1.0f), TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightTranslatedWorldToStaticShadow);
            float2 _1174 = _1170.xy / _1170.w.xx;
            bool2 _1175 = bool2(_1174.x >= 0.0f.xx.x, _1174.y >= 0.0f.xx.y);
            bool2 _1176 = bool2(_1174.x <= 1.0f.xx.x, _1174.y <= 1.0f.xx.y);
            float _1197 = 0.0f;
            if (all(bool2(_1175.x && _1176.x, _1175.y && _1176.y)))
            {
                float4 _1190 = TranslucentBasePass_Shared_Forward_DirectionalLightStaticShadowmap.SampleLevel(TranslucentBasePass_Shared_Forward_StaticShadowmapSampler, _1174, 0.0f);
                float _1191 = _1190.x;
                _1197 = float((_1170.z < _1191) || (_1191 > 0.9900000095367431640625f));
            }
            else
            {
                _1197 = 1.0f;
            }
            _1198 = _1197;
        }
        else
        {
            _1198 = 1.0f;
        }
        float4 _1199 = _819;
        _1199.x = _1198;
        float _1278 = 0.0f;
        if (TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumDirectionalLightCascades > 0u)
        {
            float4 _1207 = _496.xxxx;
            float4 _1209 = float4(bool4(_1207.x >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_CascadeEndDepths.x, _1207.y >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_CascadeEndDepths.y, _1207.z >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_CascadeEndDepths.z, _1207.w >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_CascadeEndDepths.w));
            uint _1217 = uint(((_1209.x + _1209.y) + _1209.z) + _1209.w);
            float _1277 = 0.0f;
            if (_1217 < TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumDirectionalLightCascades)
            {
                float4 _1227 = mul(float4(_493, 1.0f), TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightTranslatedWorldToShadowMatrix[_1217]);
                float2 _1231 = _1227.xy / _1227.w.xx;
                bool2 _1235 = bool2(_1231.x >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapMinMax[_1217].xy.x, _1231.y >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapMinMax[_1217].xy.y);
                bool2 _1237 = bool2(_1231.x <= TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapMinMax[_1217].zw.x, _1231.y <= TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapMinMax[_1217].zw.y);
                float _1276 = 0.0f;
                if (all(bool2(_1235.x && _1237.x, _1235.y && _1237.y)))
                {
                    float2 _1255 = mad(_1231, TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapAtlasBufferSize.xy, (-0.5f).xx);
                    float2 _1256 = frac(_1255);
                    float4 _1267 = clamp((TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapAtlas.GatherRed(TranslucentBasePass_Shared_Forward_ShadowmapSampler, (floor(_1255) + 1.0f.xx) * TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapAtlasBufferSize.zw) * 4000.0f) - mad(1.0f - _1227.z, 4000.0f, -1.0f).xxxx, 0.0f.xxxx, 1.0f.xxxx);
                    float2 _1271 = lerp(_1267.wx, _1267.zy, _1256.xx);
                    _1276 = lerp(_1271.x, _1271.y, _1256.y);
                }
                else
                {
                    _1276 = 1.0f;
                }
                _1277 = _1276;
            }
            else
            {
                _1277 = 1.0f;
            }
            _1278 = _1277;
        }
        else
        {
            _1278 = 1.0f;
        }
        float _1964 = 0.0f;
        [branch]
        if (true && (TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightVSM != (-1)))
        {
            float _1962 = 0.0f;
            do
            {
                float _1287 = max(0.0f, 0.0f);
                uint _1288 = uint(TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightVSM);
                uint _1289 = _1288 * 336u;
                uint _1291 = (_1289 + 96u) >> 2u;
                float4x4 _1305 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                _1305[2] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1291 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1291 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1291 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1291 + 3u) * 4 + 0)));
                uint _1307 = (_1289 + 128u) >> 2u;
                float4x4 _1321 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                _1321[0] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1307 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1307 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1307 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1307 + 3u) * 4 + 0)));
                uint _1323 = (_1289 + 144u) >> 2u;
                _1321[1] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1323 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1323 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1323 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1323 + 3u) * 4 + 0)));
                uint _1339 = (_1289 + 160u) >> 2u;
                _1321[2] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1339 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1339 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1339 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1339 + 3u) * 4 + 0)));
                uint _1355 = (_1289 + 176u) >> 2u;
                _1321[3] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1355 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1355 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1355 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1355 + 3u) * 4 + 0)));
                uint _1371 = (_1289 + 256u) >> 2u;
                float3 _1381 = asfloat(uint3(VirtualShadowMap_ProjectionData.Load(_1371 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1371 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1371 + 2u) * 4 + 0)));
                uint _1383 = (_1289 + 268u) >> 2u;
                uint _1387 = (_1289 + 272u) >> 2u;
                uint _1399 = (_1289 + 288u) >> 2u;
                if (VirtualShadowMap_ProjectionData.Load(_1383 * 4 + 0) == 0u)
                {
                    int _1634 = max(0, (int(floor(log2(length(((View_View_ViewTilePosition - (-_1381)) * 2097152.0f) + (_494 - (-asfloat(uint3(VirtualShadowMap_ProjectionData.Load(_1399 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1399 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1399 + 2u) * 4 + 0))))))) + asfloat(VirtualShadowMap_ProjectionData.Load(((_1289 + 300u) >> 2u) * 4 + 0)))) - int(VirtualShadowMap_ProjectionData.Load(((_1289 + 312u) >> 2u) * 4 + 0))));
                    if (_1634 < int(VirtualShadowMap_ProjectionData.Load(((_1289 + 316u) >> 2u) * 4 + 0)))
                    {
                        int _1638 = TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightVSM + _1634;
                        uint _1639 = uint(_1638);
                        uint _1640 = _1639 * 336u;
                        uint _1642 = (_1640 + 96u) >> 2u;
                        uint _1657 = (_1640 + 112u) >> 2u;
                        uint _1666 = (_1640 + 128u) >> 2u;
                        float4x4 _1680 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                        _1680[0] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1666 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1666 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1666 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1666 + 3u) * 4 + 0)));
                        uint _1682 = (_1640 + 144u) >> 2u;
                        _1680[1] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1682 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1682 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1682 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1682 + 3u) * 4 + 0)));
                        uint _1698 = (_1640 + 160u) >> 2u;
                        _1680[2] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1698 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1698 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1698 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1698 + 3u) * 4 + 0)));
                        uint _1714 = (_1640 + 176u) >> 2u;
                        _1680[3] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1714 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1714 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1714 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1714 + 3u) * 4 + 0)));
                        uint _1730 = (_1640 + 256u) >> 2u;
                        uint _1742 = (_1640 + 272u) >> 2u;
                        float4 _1761 = mul(float4(((View_View_ViewTilePosition + asfloat(uint3(VirtualShadowMap_ProjectionData.Load(_1730 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1730 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1730 + 2u) * 4 + 0)))) * 2097152.0f) + (_494 + asfloat(uint3(VirtualShadowMap_ProjectionData.Load(_1742 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1742 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1742 + 2u) * 4 + 0)))), 1.0f), _1680);
                        float2 _1762 = _1761.xy;
                        uint2 _1764 = uint2(_1762 * 128.0f);
                        uint _1780 = 0u;
                        do
                        {
                            if (uint(int(_1639)) < 8192u)
                            {
                                _1780 = _1639;
                                break;
                            }
                            _1780 = (8192u + ((_1639 - 8192u) * 21845u)) + (_1764.x + (_1764.y << 7u));
                            break;
                        } while(false);
                        uint _1788 = (VirtualShadowMap_PageTable[_1780] >> 20u) & 63u;
                        bool _1790 = (VirtualShadowMap_PageTable[_1780] & 134217728u) != 0u;
                        float _1953 = 0.0f;
                        bool _1954 = false;
                        if (_1790)
                        {
                            float _1929 = 0.0f;
                            float _1930 = 0.0f;
                            uint2 _1931 = uint2(0u, 0u);
                            uint2 _1932 = uint2(0u, 0u);
                            bool _1933 = false;
                            if (_1788 > 0u)
                            {
                                uint _1803 = (_1640 + 304u) >> 2u;
                                uint _1806 = _1803 + 1u;
                                uint _1811 = uint(int(_1639 + _1788));
                                uint _1814 = ((_1811 * 336u) + 304u) >> 2u;
                                int _1826 = int(_1788);
                                uint2 _1834 = ((_1764 - uint2(int2(32, 32) * int2(uint2(VirtualShadowMap_ProjectionData.Load(_1803 * 4 + 0), VirtualShadowMap_ProjectionData.Load(_1806 * 4 + 0))))) + uint2((int2(32, 32) * int2(uint2(VirtualShadowMap_ProjectionData.Load(_1814 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1814 + 1u) * 4 + 0)))) << (_1826.xx & int2(31, 31)))) >> (_1788.xx & uint2(31u, 31u));
                                uint2 _1835 = _1834 * uint2(128u, 128u);
                                uint _1849 = uint(_1638 + _1826) * 336u;
                                uint _1851 = (_1849 + 112u) >> 2u;
                                uint _1866 = (_1849 + 304u) >> 2u;
                                float _1887 = (_1826 >= 0) ? (1.0f / float(1u << (uint(_1826) & 31u))) : float(1u << (uint(-_1826) & 31u));
                                uint _1916 = 0u;
                                do
                                {
                                    if (uint(int(_1811)) < 8192u)
                                    {
                                        _1916 = _1811;
                                        break;
                                    }
                                    _1916 = (8192u + ((_1811 - 8192u) * 21845u)) + (_1834.x + (_1834.y << 7u));
                                    break;
                                } while(false);
                                _1929 = _1887;
                                _1930 = mad(-_1887, asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1657 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1657 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1657 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1657 + 3u) * 4 + 0))).z, asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1851 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1851 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1851 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1851 + 3u) * 4 + 0))).z);
                                _1931 = clamp(uint2(((_1762 * _1887) + ((float2(int2(uint2(VirtualShadowMap_ProjectionData.Load(_1866 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1866 + 1u) * 4 + 0)))) - (float2(int2(uint2(VirtualShadowMap_ProjectionData.Load(_1803 * 4 + 0), VirtualShadowMap_ProjectionData.Load(_1806 * 4 + 0)))) * _1887)) * 0.25f).xy) * 16384.0f), _1835, _1835 + uint2(127u, 127u));
                                _1932 = uint2(VirtualShadowMap_PageTable[_1916] & 1023u, (VirtualShadowMap_PageTable[_1916] >> 10u) & 1023u);
                                _1933 = ((VirtualShadowMap_PageTable[_1916] & 134217728u) != 0u) && (((VirtualShadowMap_PageTable[_1916] >> 20u) & 63u) == 0u);
                            }
                            else
                            {
                                _1929 = 1.0f;
                                _1930 = 0.0f;
                                _1931 = uint2(_1762 * 16384.0f);
                                _1932 = uint2(VirtualShadowMap_PageTable[_1780] & 1023u, (VirtualShadowMap_PageTable[_1780] >> 10u) & 1023u);
                                _1933 = _1790 && (_1788 == 0u);
                            }
                            float _1951 = 0.0f;
                            if (_1933)
                            {
                                int4 _1942 = int4(uint4((_1932 * uint2(128u, 128u)) + (_1931 & uint2(127u, 127u)), 0u, 0u));
                                _1951 = (asfloat(VirtualShadowMap_PhysicalPagePool.Load(int4(_1942.xyz, _1942.w)).x) - _1930) / _1929;
                            }
                            else
                            {
                                _1951 = 0.0f;
                            }
                            _1953 = _1951;
                            _1954 = _1933 ? true : false;
                        }
                        else
                        {
                            _1953 = 0.0f;
                            _1954 = false;
                        }
                        if (_1954)
                        {
                            _1962 = (mad(_1287, asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1642 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1642 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1642 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1642 + 3u) * 4 + 0))).z, _1953) > _1761.z) ? 0.0f : 1.0f;
                            break;
                        }
                    }
                }
                else
                {
                    float3 _1434 = ((View_View_ViewTilePosition + _1381) * 2097152.0f) + (_494 + asfloat(uint3(VirtualShadowMap_ProjectionData.Load(_1387 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1387 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1387 + 2u) * 4 + 0))));
                    float4x4 _1547 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                    int _1548 = 0;
                    float4x4 _1549 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                    if (VirtualShadowMap_ProjectionData.Load(_1383 * 4 + 0) != 2u)
                    {
                        uint _1462 = 0u;
                        do
                        {
                            float _1440 = _1434.x;
                            float _1441 = abs(_1440);
                            float _1442 = _1434.y;
                            float _1443 = abs(_1442);
                            float _1445 = _1434.z;
                            float _1446 = abs(_1445);
                            if ((_1441 >= _1443) && (_1441 >= _1446))
                            {
                                _1462 = (_1440 > 0.0f) ? 0u : 1u;
                                break;
                            }
                            else
                            {
                                if (_1443 > _1446)
                                {
                                    _1462 = (_1442 > 0.0f) ? 2u : 3u;
                                    break;
                                }
                                else
                                {
                                    _1462 = (_1445 > 0.0f) ? 4u : 5u;
                                    break;
                                }
                                break; // unreachable workaround
                            }
                            break; // unreachable workaround
                        } while(false);
                        int _1464 = int(_1288 + _1462);
                        uint _1466 = uint(_1464) * 336u;
                        uint _1468 = (_1466 + 96u) >> 2u;
                        float4x4 _1482 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                        _1482[2] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1468 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1468 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1468 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1468 + 3u) * 4 + 0)));
                        uint _1484 = (_1466 + 128u) >> 2u;
                        float4x4 _1498 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                        _1498[0] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1484 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1484 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1484 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1484 + 3u) * 4 + 0)));
                        uint _1500 = (_1466 + 144u) >> 2u;
                        _1498[1] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1500 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1500 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1500 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1500 + 3u) * 4 + 0)));
                        uint _1516 = (_1466 + 160u) >> 2u;
                        _1498[2] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1516 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1516 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1516 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1516 + 3u) * 4 + 0)));
                        uint _1532 = (_1466 + 176u) >> 2u;
                        _1498[3] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1532 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1532 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1532 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1532 + 3u) * 4 + 0)));
                        _1547 = _1482;
                        _1548 = _1464;
                        _1549 = _1498;
                    }
                    else
                    {
                        _1547 = _1305;
                        _1548 = TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightVSM;
                        _1549 = _1321;
                    }
                    float4 _1554 = mul(float4(_1434, 1.0f), _1549);
                    float _1555 = _1554.w;
                    float3 _1558 = _1554.xyz / _1555.xxx;
                    float2 _1559 = _1558.xy;
                    float _1612 = 0.0f;
                    bool _1613 = false;
                    do
                    {
                        bool _1569 = false;
                        uint _1562 = uint(_1548);
                        uint2 _1564 = uint2(_1559 * 128.0f);
                        uint _1580 = 0u;
                        do
                        {
                            _1569 = uint(int(_1562)) < 8192u;
                            if (_1569)
                            {
                                _1580 = _1562;
                                break;
                            }
                            _1580 = (8192u + ((_1562 - 8192u) * 21845u)) + (_1564.x + (_1564.y << 7u));
                            break;
                        } while(false);
                        if ((VirtualShadowMap_PageTable[_1580] & 134217728u) != 0u)
                        {
                            int4 _1605 = int4(uint4((uint2(VirtualShadowMap_PageTable[_1580] & 1023u, (VirtualShadowMap_PageTable[_1580] >> 10u) & 1023u) * uint2(128u, 128u)) + (uint2(_1559 * float(16384u >> ((_1569 ? 7u : ((VirtualShadowMap_PageTable[_1580] >> 20u) & 63u)) & 31u))) & uint2(127u, 127u)), 0u, 0u));
                            _1612 = asfloat(VirtualShadowMap_PhysicalPagePool.Load(int4(_1605.xyz, _1605.w)).x);
                            _1613 = true;
                            break;
                        }
                        _1612 = 0.0f;
                        _1613 = false;
                        break;
                    } while(false);
                    if (_1613)
                    {
                        _1962 = ((_1612 - (((-_1287) * _1547[2].z) / _1555)) > _1558.z) ? 0.0f : 1.0f;
                        break;
                    }
                }
                _1962 = 1.0f;
                break;
            } while(false);
            _1964 = _1278 * _1962;
        }
        else
        {
            _1964 = _1278;
        }
        float _1971 = clamp(mad(_496, TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDistanceFadeMAD.x, TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDistanceFadeMAD.y), 0.0f, 1.0f);
        float _1973 = lerp(_1964, lerp(1.0f, dot(_1199, _1155), dot(_1155, 1.0f.xxxx)), _1971 * _1971);
        float3 _2173 = 0.0f.xxx;
        float3 _2174 = 0.0f.xxx;
        [branch]
        if ((_1973 + min(_1973, 1.0f)) > 0.0f)
        {
            float _1981 = max(_802, View_View_MinRoughness);
            float _1982 = dot(TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDirection, TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDirection);
            float _1985 = rsqrt(_1982);
            float3 _1986 = TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDirection * _1985;
            float _1987 = dot(_604, _1986);
            float _2005 = 0.0f;
            if (TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius > 0.0f)
            {
                float _1994 = sqrt(clamp((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius * TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius) * (1.0f / (_1982 + 1.0f)), 0.0f, 1.0f));
                float _2004 = 0.0f;
                if (_1987 < _1994)
                {
                    float _2000 = _1994 + max(_1987, -_1994);
                    _2004 = (_2000 * _2000) / (4.0f * _1994);
                }
                else
                {
                    _2004 = _1987;
                }
                _2005 = _2004;
            }
            else
            {
                _2005 = _1987;
            }
            float _2006 = clamp(_2005, 0.0f, 1.0f);
            float _2007 = max(_1981, View_View_MinRoughness);
            float _2012 = clamp((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius * _1985) * mad(-_2007, _2007, 1.0f), 0.0f, 1.0f);
            float _2020 = dot(_604, _510);
            float _2021 = dot(_510, _1986);
            float _2023 = rsqrt(mad(2.0f, _2021, 2.0f));
            bool _2029 = _2012 > 0.0f;
            float _2108 = 0.0f;
            float _2109 = 0.0f;
            if (_2029)
            {
                float _2034 = sqrt(mad(-_2012, _2012, 1.0f));
                float _2035 = 2.0f * _1987;
                float _2036 = -_2021;
                float _2037 = mad(_2035, _2020, _2036);
                float _2106 = 0.0f;
                float _2107 = 0.0f;
                if (_2037 >= _2034)
                {
                    _2106 = 1.0f;
                    _2107 = abs(_2020);
                }
                else
                {
                    float _2042 = -_2037;
                    float _2045 = _2012 * rsqrt(mad(_2042, _2037, 1.0f));
                    float _2046 = mad(_2042, _1987, _2020);
                    float _2050 = mad(_2042, _2021, mad(2.0f * _2020, _2020, -1.0f));
                    float _2061 = _2045 * sqrt(clamp(mad(_2035 * _2020, _2021, mad(_2036, _2021, mad(-_2020, _2020, mad(-_1987, _1987, 1.0f)))), 0.0f, 1.0f));
                    float _2063 = (_2061 * 2.0f) * _2020;
                    float _2064 = mad(_1987, _2034, _2020);
                    float _2065 = mad(_2045, _2046, _2064);
                    float _2067 = mad(_2045, _2050, mad(_2021, _2034, 1.0f));
                    float _2068 = _2061 * _2067;
                    float _2069 = _2065 * _2067;
                    float _2074 = _2069 * mad(-0.5f, _2068, (0.25f * _2063) * _2065);
                    float _2084 = mad(_2065, mad(_2064, _2067 * _2067, _2069 * mad(-0.5f, mad(_2021, _2034, _2067), -0.5f)), mad(_2068, _2068, (_2063 * _2065) * mad(_2063, _2065, _2068 * (-2.0f))));
                    float _2088 = (2.0f * _2074) / mad(_2084, _2084, _2074 * _2074);
                    float _2089 = _2088 * _2084;
                    float _2091 = mad(-_2088, _2074, 1.0f);
                    float _2097 = mad(_2021, _2034, mad(_2091, _2045 * _2050, _2089 * _2063));
                    float _2099 = rsqrt(mad(2.0f, _2097, 2.0f));
                    _2106 = clamp((mad(_1987, _2034, mad(_2091, _2045 * _2046, _2089 * _2061)) + _2020) * _2099, 0.0f, 1.0f);
                    _2107 = clamp(mad(_2099, _2097, _2099), 0.0f, 1.0f);
                }
                _2108 = _2106;
                _2109 = _2107;
            }
            else
            {
                _2108 = clamp((_1987 + _2020) * _2023, 0.0f, 1.0f);
                _2109 = clamp(mad(_2023, _2021, _2023), 0.0f, 1.0f);
            }
            float _2112 = clamp(abs(_2020) + 9.9999997473787516355514526367188e-06f, 0.0f, 1.0f);
            float3 _2114 = 1.0f.xxx * _2006;
            float3 _2167 = 0.0f.xxx;
            if (((0u | (asuint(clamp(mad(-max(0.0f, TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius), 0.0500000007450580596923828125f, 1.0f), 0.0f, 1.0f)) << 1u)) & 1u) == 1u)
            {
                _2167 = 0.0f.xxx;
            }
            else
            {
                float _2121 = _1981 * _1981;
                float _2122 = _2121 * _2121;
                float _2136 = 0.0f;
                if (_2029)
                {
                    _2136 = _2122 / mad(_2121, _2121, ((0.25f * _2012) * mad(3.0f, asfloat(532487669 + (asint(_2122) >> 1)), _2012)) / (_2109 + 0.001000000047497451305389404296875f));
                }
                else
                {
                    _2136 = 1.0f;
                }
                float _2139 = mad(mad(_2108, _2122, -_2108), _2108, 1.0f);
                float _2144 = sqrt(_2122);
                float _2145 = 1.0f - _2144;
                float _2151 = 1.0f - _2109;
                float _2152 = _2151 * _2151;
                float _2153 = _2152 * _2152;
                _2167 = _2114 * (((clamp(50.0f * _849.y, 0.0f, 1.0f) * (_2153 * _2151)).xxx + (_849 * mad(-_2153, _2151, 1.0f))) * (((_2122 / ((3.1415927410125732421875f * _2139) * _2139)) * _2136) * (0.5f / mad(_2006, mad(_2112, _2145, _2144), _2112 * mad(_2006, _2145, _2144)))));
            }
            float3 _2170 = TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightColor * _1973;
            _2173 = mad(((_847 * 0.3183098733425140380859375f) * _2114) * 1.0f, _2170, 0.0f.xxx);
            _2174 = (_2167 * 1.0f) * _2170;
        }
        else
        {
            _2173 = 0.0f.xxx;
            _2174 = 0.0f.xxx;
        }
        float4 _2178 = float4(_2173, 0.0f);
        float4 _2182 = float4(_2174, 0.0f);
        float4 _2189 = 0.0f.xxxx;
        float4 _2190 = 0.0f.xxxx;
        [flatten]
        if ((((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowMapChannelMask >> 8u) & 7u) & _1130) != 0u)
        {
            _2189 = float4(_2178.x, _2178.y, _2178.z, _2178.w);
            _2190 = float4(_2182.x, _2182.y, _2182.z, _2182.w);
        }
        else
        {
            _2189 = 0.0f.xxxx;
            _2190 = 0.0f.xxxx;
        }
        _2191 = _1199;
        _2192 = _2189;
        _2193 = _2190;
    }
    else
    {
        _2191 = _819;
        _2192 = 0.0f.xxxx;
        _2193 = 0.0f.xxxx;
    }
    uint _2194 = _1114 * 2u;
    uint _2200 = _2194 + 1u;
    uint _2203 = min(min(TranslucentBasePass_Shared_Forward_NumCulledLightsGrid[_2194], TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumLocalLights), TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumLocalLights);
    float4 _2205 = 0.0f.xxxx;
    float4 _2208 = 0.0f.xxxx;
    _2205 = _2192;
    _2208 = _2193;
    float4 _2206 = 0.0f.xxxx;
    float4 _2209 = 0.0f.xxxx;
    [loop]
    for (uint _2210 = 0u; _2210 < _2203; _2205 = _2206, _2208 = _2209, _2210++)
    {
        uint _2219 = TranslucentBasePass_Shared_Forward_CulledLightDataGrid16Bit.Load(TranslucentBasePass_Shared_Forward_NumCulledLightsGrid[_2200] + _2210).x * 6u;
        uint _2222 = _2219 + 1u;
        uint _2225 = _2219 + 2u;
        uint _2228 = _2219 + 3u;
        uint _2231 = _2219 + 4u;
        uint _2235 = asuint(TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2225].w);
        uint _2241 = asuint(TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2222].y);
        uint _2257 = asuint(TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2228].z);
        float2 _2259 = spvUnpackHalf2x16(_2257 & 65535u);
        float _2260 = _2259.x;
        float2 _2263 = spvUnpackHalf2x16(asuint(TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2228].w));
        float _2264 = _2263.x;
        bool _2269 = TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2222].w == 0.0f;
        float4 _2290 = float4(float(_2235 & 1u), float((_2235 & 2u) >> 1u), float((_2235 & 4u) >> 2u), float((_2235 & 8u) >> 3u));
        uint _2291 = _2235 >> 4u;
        float3 _2307 = TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2219].xyz - _493;
        float _2308 = dot(_2307, _2307);
        float _2325 = 0.0f;
        if (_2269)
        {
            float _2320 = _2308 * (TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2219].w * TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2219].w);
            float _2323 = clamp(mad(-_2320, _2320, 1.0f), 0.0f, 1.0f);
            _2325 = _2323 * _2323;
        }
        else
        {
            float3 _2314 = _2307 * TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2219].w;
            _2325 = pow(1.0f - clamp(dot(_2314, _2314), 0.0f, 1.0f), TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2222].w);
        }
        float _2336 = 0.0f;
        if (((_2235 >> 16u) & 3u) == 2u)
        {
            float _2333 = clamp((dot(_2307 * rsqrt(_2308), TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2225].xyz) - TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2228].x) * TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2228].y, 0.0f, 1.0f);
            _2336 = _2325 * (_2333 * _2333);
        }
        else
        {
            _2336 = _2325;
        }
        float3 _2631 = 0.0f.xxx;
        float3 _2632 = 0.0f.xxx;
        [branch]
        if (_2336 > 0.0f)
        {
            float _2348 = 0.0f;
            [branch]
            if (uint((_2235 & 255u) != 0u) != 0u)
            {
                _2348 = dot(float4(float(_2291 & 1u), float((_2291 & 2u) >> 1u), float((_2291 & 4u) >> 2u), float((_2291 & 8u) >> 3u)), 1.0f.xxxx) * lerp(1.0f, dot(_2191, _2290), dot(_2290, 1.0f.xxxx));
            }
            else
            {
                _2348 = 1.0f;
            }
            float3 _2629 = 0.0f.xxx;
            float3 _2630 = 0.0f.xxx;
            [branch]
            if ((_2348 + _2348) > 0.0f)
            {
                float3 _2354 = TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2231].xyz * (0.5f * _2264);
                float3 _2355 = _2307 - _2354;
                float3 _2356 = _2307 + _2354;
                float _2359 = max(_802, View_View_MinRoughness);
                bool _2360 = _2264 > 0.0f;
                float _2385 = 0.0f;
                float _2386 = 0.0f;
                float _2387 = 0.0f;
                [branch]
                if (_2360)
                {
                    float _2372 = rsqrt(dot(_2355, _2355));
                    float _2373 = rsqrt(dot(_2356, _2356));
                    float _2374 = _2372 * _2373;
                    float _2376 = dot(_2355, _2356) * _2374;
                    _2385 = _2376;
                    _2386 = 0.5f * mad(dot(_604, _2355), _2372, dot(_604, _2356) * _2373);
                    _2387 = _2374 / mad(_2372, _2373, mad(_2376, 0.5f, 0.5f));
                }
                else
                {
                    float _2364 = dot(_2355, _2355);
                    _2385 = 1.0f;
                    _2386 = dot(_604, _2355 * rsqrt(_2364));
                    _2387 = 1.0f / (_2364 + 1.0f);
                }
                float _2405 = 0.0f;
                if (_2260 > 0.0f)
                {
                    float _2394 = sqrt(clamp((_2260 * _2260) * _2387, 0.0f, 1.0f));
                    float _2404 = 0.0f;
                    if (_2386 < _2394)
                    {
                        float _2400 = _2394 + max(_2386, -_2394);
                        _2404 = (_2400 * _2400) / (4.0f * _2394);
                    }
                    else
                    {
                        _2404 = _2386;
                    }
                    _2405 = _2404;
                }
                else
                {
                    _2405 = _2386;
                }
                float _2406 = clamp(_2405, 0.0f, 1.0f);
                float3 _2424 = 0.0f.xxx;
                if (_2360)
                {
                    float3 _2411 = reflect(-_510, _604);
                    float3 _2412 = _2356 - _2355;
                    float _2413 = dot(_2411, _2412);
                    _2424 = _2355 + (_2412 * clamp(dot(_2355, (_2411 * _2413) - _2412) / mad(_2264, _2264, -(_2413 * _2413)), 0.0f, 1.0f));
                }
                else
                {
                    _2424 = _2355;
                }
                float _2426 = rsqrt(dot(_2424, _2424));
                float3 _2427 = _2424 * _2426;
                float _2428 = max(_2359, View_View_MinRoughness);
                float _2433 = clamp((_2260 * _2426) * mad(-_2428, _2428, 1.0f), 0.0f, 1.0f);
                float _2435 = clamp(spvUnpackHalf2x16(_2257 >> 16u).x * _2426, 0.0f, 1.0f);
                float _2443 = dot(_604, _2427);
                float _2444 = dot(_604, _510);
                float _2445 = dot(_510, _2427);
                float _2447 = rsqrt(mad(2.0f, _2445, 2.0f));
                bool _2453 = _2433 > 0.0f;
                float _2532 = 0.0f;
                float _2533 = 0.0f;
                if (_2453)
                {
                    float _2458 = sqrt(mad(-_2433, _2433, 1.0f));
                    float _2459 = 2.0f * _2443;
                    float _2460 = -_2445;
                    float _2461 = mad(_2459, _2444, _2460);
                    float _2530 = 0.0f;
                    float _2531 = 0.0f;
                    if (_2461 >= _2458)
                    {
                        _2530 = 1.0f;
                        _2531 = abs(_2444);
                    }
                    else
                    {
                        float _2466 = -_2461;
                        float _2469 = _2433 * rsqrt(mad(_2466, _2461, 1.0f));
                        float _2470 = mad(_2466, _2443, _2444);
                        float _2474 = mad(_2466, _2445, mad(2.0f * _2444, _2444, -1.0f));
                        float _2485 = _2469 * sqrt(clamp(mad(_2459 * _2444, _2445, mad(_2460, _2445, mad(-_2444, _2444, mad(-_2443, _2443, 1.0f)))), 0.0f, 1.0f));
                        float _2487 = (_2485 * 2.0f) * _2444;
                        float _2488 = mad(_2443, _2458, _2444);
                        float _2489 = mad(_2469, _2470, _2488);
                        float _2491 = mad(_2469, _2474, mad(_2445, _2458, 1.0f));
                        float _2492 = _2485 * _2491;
                        float _2493 = _2489 * _2491;
                        float _2498 = _2493 * mad(-0.5f, _2492, (0.25f * _2487) * _2489);
                        float _2508 = mad(_2489, mad(_2488, _2491 * _2491, _2493 * mad(-0.5f, mad(_2445, _2458, _2491), -0.5f)), mad(_2492, _2492, (_2487 * _2489) * mad(_2487, _2489, _2492 * (-2.0f))));
                        float _2512 = (2.0f * _2498) / mad(_2508, _2508, _2498 * _2498);
                        float _2513 = _2512 * _2508;
                        float _2515 = mad(-_2512, _2498, 1.0f);
                        float _2521 = mad(_2445, _2458, mad(_2515, _2469 * _2474, _2513 * _2487));
                        float _2523 = rsqrt(mad(2.0f, _2521, 2.0f));
                        _2530 = clamp((mad(_2443, _2458, mad(_2515, _2469 * _2470, _2513 * _2485)) + _2444) * _2523, 0.0f, 1.0f);
                        _2531 = clamp(mad(_2523, _2521, _2523), 0.0f, 1.0f);
                    }
                    _2532 = _2530;
                    _2533 = _2531;
                }
                else
                {
                    _2532 = clamp((_2443 + _2444) * _2447, 0.0f, 1.0f);
                    _2533 = clamp(mad(_2447, _2445, _2447), 0.0f, 1.0f);
                }
                float _2536 = clamp(abs(_2444) + 9.9999997473787516355514526367188e-06f, 0.0f, 1.0f);
                float3 _2539 = 1.0f.xxx * ((_2269 ? _2387 : 1.0f) * _2406);
                float3 _2623 = 0.0f.xxx;
                if (((0u | (asuint(clamp(mad(-max(_2264, _2260), 0.0500000007450580596923828125f, 1.0f), 0.0f, 1.0f)) << 1u)) & 1u) == 1u)
                {
                    _2623 = 0.0f.xxx;
                }
                else
                {
                    float _2546 = _2359 * _2359;
                    float _2556 = 0.0f;
                    if (_2435 > 0.0f)
                    {
                        _2556 = clamp(mad(_2546, _2546, (_2435 * _2435) / mad(_2533, 3.599999904632568359375f, 0.4000000059604644775390625f)), 0.0f, 1.0f);
                    }
                    else
                    {
                        _2556 = _2546 * _2546;
                    }
                    float _2570 = 0.0f;
                    float _2571 = 0.0f;
                    if (_2453)
                    {
                        float _2568 = _2556 + (((0.25f * _2433) * mad(3.0f, asfloat(532487669 + (asint(_2556) >> 1)), _2433)) / (_2533 + 0.001000000047497451305389404296875f));
                        _2570 = _2556 / _2568;
                        _2571 = _2568;
                    }
                    else
                    {
                        _2570 = 1.0f;
                        _2571 = _2556;
                    }
                    float _2592 = 0.0f;
                    if (_2385 < 1.0f)
                    {
                        float _2578 = sqrt((1.00010001659393310546875f - _2385) / (1.0f + _2385));
                        _2592 = _2570 * sqrt(_2571 / (_2571 + (((0.25f * _2578) * mad(3.0f, asfloat(532487669 + (asint(_2571) >> 1)), _2578)) / (_2533 + 0.001000000047497451305389404296875f))));
                    }
                    else
                    {
                        _2592 = _2570;
                    }
                    float _2595 = mad(mad(_2532, _2556, -_2532), _2532, 1.0f);
                    float _2600 = sqrt(_2556);
                    float _2601 = 1.0f - _2600;
                    float _2607 = 1.0f - _2533;
                    float _2608 = _2607 * _2607;
                    float _2609 = _2608 * _2608;
                    _2623 = _2539 * (((clamp(50.0f * _849.y, 0.0f, 1.0f) * (_2609 * _2607)).xxx + (_849 * mad(-_2609, _2607, 1.0f))) * (((_2556 / ((3.1415927410125732421875f * _2595) * _2595)) * _2592) * (0.5f / mad(_2406, mad(_2536, _2601, _2600), _2536 * mad(_2406, _2601, _2600)))));
                }
                float3 _2626 = ((float3(float((_2241 >> 0u) & 1023u), float((_2241 >> 10u) & 1023u), float((_2241 >> 20u) & 1023u)) * TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2222].x) * _2336) * _2348;
                _2629 = mad(((_847 * 0.3183098733425140380859375f) * _2539) * 1.0f, _2626, 0.0f.xxx);
                _2630 = (_2623 * spvUnpackHalf2x16(asuint(TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2231].w) & 65535u).x) * _2626;
            }
            else
            {
                _2629 = 0.0f.xxx;
                _2630 = 0.0f.xxx;
            }
            _2631 = _2629;
            _2632 = _2630;
        }
        else
        {
            _2631 = 0.0f.xxx;
            _2632 = 0.0f.xxx;
        }
        [flatten]
        if ((((_2235 >> 8u) & 7u) & _1130) != 0u)
        {
            _2206 = _2205 + float4(_2631, 0.0f);
            _2209 = _2208 + float4(_2632, 0.0f);
        }
        else
        {
            _2206 = _2205;
            _2209 = _2208;
        }
    }
    bool4 _2650 = (TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectLightingShowFlag == 0u).xxxx;
    float3 _2659 = (_604 * (2.0f * dot(_510, _604))) - _510;
    bool _2698 = false;
    if (TranslucentBasePass_TranslucentBasePass_Enabled > 0u)
    {
        float _2691 = 0.0f;
        do
        {
            [flatten]
            if (asuint(View_View_ViewToClip[3].w) != 0u)
            {
                _2691 = mad(_496, View_View_ViewToClip[2u].z, View_View_ViewToClip[3u].z);
                break;
            }
            else
            {
                _2691 = 1.0f / ((_496 + View_View_InvDeviceZToWorldZTransform.w) * View_View_InvDeviceZToWorldZTransform.z);
                break;
            }
            break; // unreachable workaround
        } while(false);
        _2698 = (abs(TranslucentBasePass_SceneDepth.SampleLevel(View_SharedPointClampedSampler, _501, 0.0f).x - _2691) < TranslucentBasePass_TranslucentBasePass_RelativeDepthThreshold) ? true : false;
    }
    else
    {
        _2698 = false;
    }
    bool _2702 = !_2698;
    uint _2768 = 0u;
    bool _2769 = false;
    if ((TranslucentBasePass_TranslucentBasePass_FinalProbeResolution > 0u) && _2702)
    {
        uint _2724 = 0u;
        float _2714 = frac(52.98291778564453125f * frac(dot(gl_FragCoord.xy + (float2(32.66500091552734375f, 11.81499958038330078125f) * float(View_View_StateFrameIndexMod8)), float2(0.067110560834407806396484375f, 0.005837149918079376220703125f))));
        float3 _2716 = (View_View_ViewTilePosition * 2097152.0f) + _494;
        uint _2765 = 0u;
        do
        {
            uint _2762 = 0u;
            bool _2763 = false;
            uint _2720 = 0u;
            for (;;)
            {
                _2724 = TranslucentBasePass_TranslucentBasePass_NumRadianceProbeClipmaps;
                if (_2720 < _2724)
                {
                    float3 _2734 = (_2716 * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2720].y) + TranslucentBasePass_TranslucentBasePass_PaddedWorldPositionToRadianceProbeCoordBias[_2720].xyz;
                    float3 _2739 = clamp((_2734 - 0.5f.xxx) * TranslucentBasePass_TranslucentBasePass_InvClipmapFadeSize, 0.0f.xxx, 1.0f.xxx);
                    float3 _2747 = clamp(((float(TranslucentBasePass_TranslucentBasePass_RadianceProbeClipmapResolution).xxx - 0.5f.xxx) - _2734) * TranslucentBasePass_TranslucentBasePass_InvClipmapFadeSize, 0.0f.xxx, 1.0f.xxx);
                    if (min(min(_2739.x, min(_2739.y, _2739.z)), min(_2747.x, min(_2747.y, _2747.z))) > _2714)
                    {
                        _2762 = _2720;
                        _2763 = true;
                        break;
                    }
                    _2720++;
                    continue;
                }
                else
                {
                    _2762 = _343;
                    _2763 = false;
                    break;
                }
            }
            if (_2763)
            {
                _2765 = _2762;
                break;
            }
            _2765 = _2724;
            break;
        } while(false);
        _2768 = _2765;
        _2769 = (_2765 < _2724) ? true : false;
    }
    else
    {
        _2768 = 0u;
        _2769 = false;
    }
    float3 _3985 = 0.0f.xxx;
    if (_2698)
    {
        _3985 = (pow((TranslucentBasePass_Radiance.SampleLevel(View_SharedPointClampedSampler, _501, 0.0f).xyz * View_View_OneOverPreExposure) * 5.5555553436279296875f, TranslucentBasePass_TranslucentBasePass_Contrast.xxx) * 0.180000007152557373046875f) * TranslucentBasePass_TranslucentBasePass_SpecularScale;
    }
    else
    {
        float3 _3967 = 0.0f.xxx;
        if (_2769)
        {
            float3 _3059 = (View_View_ViewTilePosition * 2097152.0f) + _494;
            float3 _3067 = ((_3059 * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2768].y) + TranslucentBasePass_TranslucentBasePass_PaddedWorldPositionToRadianceProbeCoordBias[_2768].xyz) - 0.5f.xxx;
            int3 _3069 = int3(floor(_3067));
            float3 _3070 = frac(_3067);
            uint3 _3071 = uint3(_3069);
            uint _3077 = _2768 * TranslucentBasePass_TranslucentBasePass_RadianceProbeClipmapResolution;
            int4 _3082 = int4(uint4(_3071.x + _3077, _3071.yz, 0u));
            uint4 _3086 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3082.xyz, _3082.w));
            uint _3087 = _3086.x;
            float3 _3099 = ((float3(_3071) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2768].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2768].xyz) + TranslucentBasePass_ProbeWorldOffset[_3087].xyz;
            float _3102 = TranslucentBasePass_TranslucentBasePass_ReprojectionRadiusScale * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2768].x;
            float3 _3108 = _3059 - float4(_3099, _3102).xyz;
            float _3110 = dot(_2659, _2659);
            float _3111 = dot(_2659, _3108);
            float _3112 = 2.0f * _3111;
            float _3113 = -_3102;
            float _3115 = 4.0f * _3110;
            float _3118 = mad(_3112, _3112, -(_3115 * mad(_3113, _3102, dot(_3108, _3108))));
            float2 _3130 = 0.0f.xx;
            [flatten]
            if (_3118 >= 0.0f)
            {
                _3130 = ((_3111 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3118))) / (2.0f * _3110).xx;
            }
            else
            {
                _3130 = (-1.0f).xx;
            }
            float3 _3134 = (_3059 + (_2659 * _3130.y)) - _3099;
            float3 _3139 = normalize(_3134);
            float3 _3140 = abs(_3139);
            float _3143 = sqrt(1.0f - _3140.z);
            float _3144 = _3140.x;
            float _3145 = _3140.y;
            float _3149 = min(_3144, _3145) / (max(_3144, _3145) + 5.4210108624275221700372640043497e-20f);
            float _3155 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3149, 0.0419038832187652587890625f), _3149, 0.08817707002162933349609375f), _3149, -0.2473337352275848388671875f), _3149, 0.006157201714813709259033203125f), _3149, 0.63622653484344482421875f), _3149, 4.0675854506844189018011093139648e-06f);
            float _3158 = (_3144 < _3145) ? (1.0f - _3155) : _3155;
            float2 _3162 = float2(mad(-_3158, _3143, _3143), _3158 * _3143);
            bool2 _3165 = (_3139.z < 0.0f).xx;
            float2 _3167 = 1.0f.xx - _3162.yx;
            uint2 _3177 = TranslucentBasePass_TranslucentBasePass_FinalProbeResolution.xx;
            uint _3183 = TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionDivideShift & 31u;
            float _3189 = float(TranslucentBasePass_TranslucentBasePass_RadianceProbeResolution);
            float2 _3196 = float(1u << (TranslucentBasePass_TranslucentBasePass_FinalRadianceAtlasMaxMip & 31u)).xx;
            bool3 _3204 = (_3087 == 4294967295u).xxx;
            float3 _3209 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3177 * uint2(_3087 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3087 >> _3183)) + ((((asfloat(asuint(float2(_3165.x ? _3167.x : _3162.x, _3165.y ? _3167.y : _3162.y)) ^ (asuint(_3139.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3189) + _3196)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3213 = uint3(_3069 + int3(0, 0, 1));
            int4 _3219 = int4(uint4(_3213.x + _3077, _3213.yz, 0u));
            uint4 _3222 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3219.xyz, _3219.w));
            uint _3223 = _3222.x;
            float3 _3230 = ((float3(_3213) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2768].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2768].xyz) + TranslucentBasePass_ProbeWorldOffset[_3223].xyz;
            float3 _3236 = _3059 - float4(_3230, _3102).xyz;
            float _3238 = dot(_2659, _3236);
            float _3239 = 2.0f * _3238;
            float _3243 = mad(_3239, _3239, -(_3115 * mad(_3113, _3102, dot(_3236, _3236))));
            float2 _3255 = 0.0f.xx;
            [flatten]
            if (_3243 >= 0.0f)
            {
                _3255 = ((_3238 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3243))) / (2.0f * _3110).xx;
            }
            else
            {
                _3255 = (-1.0f).xx;
            }
            float3 _3259 = (_3059 + (_2659 * _3255.y)) - _3230;
            float3 _3264 = normalize(_3259);
            float3 _3265 = abs(_3264);
            float _3268 = sqrt(1.0f - _3265.z);
            float _3269 = _3265.x;
            float _3270 = _3265.y;
            float _3274 = min(_3269, _3270) / (max(_3269, _3270) + 5.4210108624275221700372640043497e-20f);
            float _3280 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3274, 0.0419038832187652587890625f), _3274, 0.08817707002162933349609375f), _3274, -0.2473337352275848388671875f), _3274, 0.006157201714813709259033203125f), _3274, 0.63622653484344482421875f), _3274, 4.0675854506844189018011093139648e-06f);
            float _3283 = (_3269 < _3270) ? (1.0f - _3280) : _3280;
            float2 _3287 = float2(mad(-_3283, _3268, _3268), _3283 * _3268);
            bool2 _3290 = (_3264.z < 0.0f).xx;
            float2 _3292 = 1.0f.xx - _3287.yx;
            bool3 _3312 = (_3223 == 4294967295u).xxx;
            float3 _3315 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3177 * uint2(_3223 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3223 >> _3183)) + ((((asfloat(asuint(float2(_3290.x ? _3292.x : _3287.x, _3290.y ? _3292.y : _3287.y)) ^ (asuint(_3264.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3189) + _3196)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3319 = uint3(_3069 + int3(0, 1, 0));
            int4 _3325 = int4(uint4(_3319.x + _3077, _3319.yz, 0u));
            uint4 _3328 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3325.xyz, _3325.w));
            uint _3329 = _3328.x;
            float3 _3336 = ((float3(_3319) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2768].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2768].xyz) + TranslucentBasePass_ProbeWorldOffset[_3329].xyz;
            float3 _3342 = _3059 - float4(_3336, _3102).xyz;
            float _3344 = dot(_2659, _3342);
            float _3345 = 2.0f * _3344;
            float _3349 = mad(_3345, _3345, -(_3115 * mad(_3113, _3102, dot(_3342, _3342))));
            float2 _3361 = 0.0f.xx;
            [flatten]
            if (_3349 >= 0.0f)
            {
                _3361 = ((_3344 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3349))) / (2.0f * _3110).xx;
            }
            else
            {
                _3361 = (-1.0f).xx;
            }
            float3 _3365 = (_3059 + (_2659 * _3361.y)) - _3336;
            float3 _3370 = normalize(_3365);
            float3 _3371 = abs(_3370);
            float _3374 = sqrt(1.0f - _3371.z);
            float _3375 = _3371.x;
            float _3376 = _3371.y;
            float _3380 = min(_3375, _3376) / (max(_3375, _3376) + 5.4210108624275221700372640043497e-20f);
            float _3386 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3380, 0.0419038832187652587890625f), _3380, 0.08817707002162933349609375f), _3380, -0.2473337352275848388671875f), _3380, 0.006157201714813709259033203125f), _3380, 0.63622653484344482421875f), _3380, 4.0675854506844189018011093139648e-06f);
            float _3389 = (_3375 < _3376) ? (1.0f - _3386) : _3386;
            float2 _3393 = float2(mad(-_3389, _3374, _3374), _3389 * _3374);
            bool2 _3396 = (_3370.z < 0.0f).xx;
            float2 _3398 = 1.0f.xx - _3393.yx;
            bool3 _3418 = (_3329 == 4294967295u).xxx;
            float3 _3421 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3177 * uint2(_3329 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3329 >> _3183)) + ((((asfloat(asuint(float2(_3396.x ? _3398.x : _3393.x, _3396.y ? _3398.y : _3393.y)) ^ (asuint(_3370.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3189) + _3196)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3425 = uint3(_3069 + int3(0, 1, 1));
            int4 _3431 = int4(uint4(_3425.x + _3077, _3425.yz, 0u));
            uint4 _3434 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3431.xyz, _3431.w));
            uint _3435 = _3434.x;
            float3 _3442 = ((float3(_3425) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2768].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2768].xyz) + TranslucentBasePass_ProbeWorldOffset[_3435].xyz;
            float3 _3448 = _3059 - float4(_3442, _3102).xyz;
            float _3450 = dot(_2659, _3448);
            float _3451 = 2.0f * _3450;
            float _3455 = mad(_3451, _3451, -(_3115 * mad(_3113, _3102, dot(_3448, _3448))));
            float2 _3467 = 0.0f.xx;
            [flatten]
            if (_3455 >= 0.0f)
            {
                _3467 = ((_3450 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3455))) / (2.0f * _3110).xx;
            }
            else
            {
                _3467 = (-1.0f).xx;
            }
            float3 _3471 = (_3059 + (_2659 * _3467.y)) - _3442;
            float3 _3476 = normalize(_3471);
            float3 _3477 = abs(_3476);
            float _3480 = sqrt(1.0f - _3477.z);
            float _3481 = _3477.x;
            float _3482 = _3477.y;
            float _3486 = min(_3481, _3482) / (max(_3481, _3482) + 5.4210108624275221700372640043497e-20f);
            float _3492 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3486, 0.0419038832187652587890625f), _3486, 0.08817707002162933349609375f), _3486, -0.2473337352275848388671875f), _3486, 0.006157201714813709259033203125f), _3486, 0.63622653484344482421875f), _3486, 4.0675854506844189018011093139648e-06f);
            float _3495 = (_3481 < _3482) ? (1.0f - _3492) : _3492;
            float2 _3499 = float2(mad(-_3495, _3480, _3480), _3495 * _3480);
            bool2 _3502 = (_3476.z < 0.0f).xx;
            float2 _3504 = 1.0f.xx - _3499.yx;
            bool3 _3524 = (_3435 == 4294967295u).xxx;
            float3 _3527 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3177 * uint2(_3435 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3435 >> _3183)) + ((((asfloat(asuint(float2(_3502.x ? _3504.x : _3499.x, _3502.y ? _3504.y : _3499.y)) ^ (asuint(_3476.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3189) + _3196)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3531 = uint3(_3069 + int3(1, 0, 0));
            int4 _3537 = int4(uint4(_3531.x + _3077, _3531.yz, 0u));
            uint4 _3540 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3537.xyz, _3537.w));
            uint _3541 = _3540.x;
            float3 _3548 = ((float3(_3531) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2768].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2768].xyz) + TranslucentBasePass_ProbeWorldOffset[_3541].xyz;
            float3 _3554 = _3059 - float4(_3548, _3102).xyz;
            float _3556 = dot(_2659, _3554);
            float _3557 = 2.0f * _3556;
            float _3561 = mad(_3557, _3557, -(_3115 * mad(_3113, _3102, dot(_3554, _3554))));
            float2 _3573 = 0.0f.xx;
            [flatten]
            if (_3561 >= 0.0f)
            {
                _3573 = ((_3556 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3561))) / (2.0f * _3110).xx;
            }
            else
            {
                _3573 = (-1.0f).xx;
            }
            float3 _3577 = (_3059 + (_2659 * _3573.y)) - _3548;
            float3 _3582 = normalize(_3577);
            float3 _3583 = abs(_3582);
            float _3586 = sqrt(1.0f - _3583.z);
            float _3587 = _3583.x;
            float _3588 = _3583.y;
            float _3592 = min(_3587, _3588) / (max(_3587, _3588) + 5.4210108624275221700372640043497e-20f);
            float _3598 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3592, 0.0419038832187652587890625f), _3592, 0.08817707002162933349609375f), _3592, -0.2473337352275848388671875f), _3592, 0.006157201714813709259033203125f), _3592, 0.63622653484344482421875f), _3592, 4.0675854506844189018011093139648e-06f);
            float _3601 = (_3587 < _3588) ? (1.0f - _3598) : _3598;
            float2 _3605 = float2(mad(-_3601, _3586, _3586), _3601 * _3586);
            bool2 _3608 = (_3582.z < 0.0f).xx;
            float2 _3610 = 1.0f.xx - _3605.yx;
            bool3 _3630 = (_3541 == 4294967295u).xxx;
            float3 _3633 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3177 * uint2(_3541 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3541 >> _3183)) + ((((asfloat(asuint(float2(_3608.x ? _3610.x : _3605.x, _3608.y ? _3610.y : _3605.y)) ^ (asuint(_3582.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3189) + _3196)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3637 = uint3(_3069 + int3(1, 0, 1));
            int4 _3643 = int4(uint4(_3637.x + _3077, _3637.yz, 0u));
            uint4 _3646 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3643.xyz, _3643.w));
            uint _3647 = _3646.x;
            float3 _3654 = ((float3(_3637) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2768].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2768].xyz) + TranslucentBasePass_ProbeWorldOffset[_3647].xyz;
            float3 _3660 = _3059 - float4(_3654, _3102).xyz;
            float _3662 = dot(_2659, _3660);
            float _3663 = 2.0f * _3662;
            float _3667 = mad(_3663, _3663, -(_3115 * mad(_3113, _3102, dot(_3660, _3660))));
            float2 _3679 = 0.0f.xx;
            [flatten]
            if (_3667 >= 0.0f)
            {
                _3679 = ((_3662 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3667))) / (2.0f * _3110).xx;
            }
            else
            {
                _3679 = (-1.0f).xx;
            }
            float3 _3683 = (_3059 + (_2659 * _3679.y)) - _3654;
            float3 _3688 = normalize(_3683);
            float3 _3689 = abs(_3688);
            float _3692 = sqrt(1.0f - _3689.z);
            float _3693 = _3689.x;
            float _3694 = _3689.y;
            float _3698 = min(_3693, _3694) / (max(_3693, _3694) + 5.4210108624275221700372640043497e-20f);
            float _3704 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3698, 0.0419038832187652587890625f), _3698, 0.08817707002162933349609375f), _3698, -0.2473337352275848388671875f), _3698, 0.006157201714813709259033203125f), _3698, 0.63622653484344482421875f), _3698, 4.0675854506844189018011093139648e-06f);
            float _3707 = (_3693 < _3694) ? (1.0f - _3704) : _3704;
            float2 _3711 = float2(mad(-_3707, _3692, _3692), _3707 * _3692);
            bool2 _3714 = (_3688.z < 0.0f).xx;
            float2 _3716 = 1.0f.xx - _3711.yx;
            bool3 _3736 = (_3647 == 4294967295u).xxx;
            float3 _3739 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3177 * uint2(_3647 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3647 >> _3183)) + ((((asfloat(asuint(float2(_3714.x ? _3716.x : _3711.x, _3714.y ? _3716.y : _3711.y)) ^ (asuint(_3688.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3189) + _3196)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3743 = uint3(_3069 + int3(1, 1, 0));
            int4 _3749 = int4(uint4(_3743.x + _3077, _3743.yz, 0u));
            uint4 _3752 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3749.xyz, _3749.w));
            uint _3753 = _3752.x;
            float3 _3760 = ((float3(_3743) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2768].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2768].xyz) + TranslucentBasePass_ProbeWorldOffset[_3753].xyz;
            float3 _3766 = _3059 - float4(_3760, _3102).xyz;
            float _3768 = dot(_2659, _3766);
            float _3769 = 2.0f * _3768;
            float _3773 = mad(_3769, _3769, -(_3115 * mad(_3113, _3102, dot(_3766, _3766))));
            float2 _3785 = 0.0f.xx;
            [flatten]
            if (_3773 >= 0.0f)
            {
                _3785 = ((_3768 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3773))) / (2.0f * _3110).xx;
            }
            else
            {
                _3785 = (-1.0f).xx;
            }
            float3 _3789 = (_3059 + (_2659 * _3785.y)) - _3760;
            float3 _3794 = normalize(_3789);
            float3 _3795 = abs(_3794);
            float _3798 = sqrt(1.0f - _3795.z);
            float _3799 = _3795.x;
            float _3800 = _3795.y;
            float _3804 = min(_3799, _3800) / (max(_3799, _3800) + 5.4210108624275221700372640043497e-20f);
            float _3810 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3804, 0.0419038832187652587890625f), _3804, 0.08817707002162933349609375f), _3804, -0.2473337352275848388671875f), _3804, 0.006157201714813709259033203125f), _3804, 0.63622653484344482421875f), _3804, 4.0675854506844189018011093139648e-06f);
            float _3813 = (_3799 < _3800) ? (1.0f - _3810) : _3810;
            float2 _3817 = float2(mad(-_3813, _3798, _3798), _3813 * _3798);
            bool2 _3820 = (_3794.z < 0.0f).xx;
            float2 _3822 = 1.0f.xx - _3817.yx;
            bool3 _3842 = (_3753 == 4294967295u).xxx;
            float3 _3845 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3177 * uint2(_3753 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3753 >> _3183)) + ((((asfloat(asuint(float2(_3820.x ? _3822.x : _3817.x, _3820.y ? _3822.y : _3817.y)) ^ (asuint(_3794.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3189) + _3196)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3849 = uint3(_3069 + int3(1, 1, 1));
            int4 _3855 = int4(uint4(_3849.x + _3077, _3849.yz, 0u));
            uint4 _3858 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3855.xyz, _3855.w));
            uint _3859 = _3858.x;
            float3 _3866 = ((float3(_3849) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2768].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2768].xyz) + TranslucentBasePass_ProbeWorldOffset[_3859].xyz;
            float3 _3872 = _3059 - float4(_3866, _3102).xyz;
            float _3874 = dot(_2659, _3872);
            float _3875 = 2.0f * _3874;
            float _3879 = mad(_3875, _3875, -(_3115 * mad(_3113, _3102, dot(_3872, _3872))));
            float2 _3891 = 0.0f.xx;
            [flatten]
            if (_3879 >= 0.0f)
            {
                _3891 = ((_3874 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3879))) / (2.0f * _3110).xx;
            }
            else
            {
                _3891 = (-1.0f).xx;
            }
            float3 _3895 = (_3059 + (_2659 * _3891.y)) - _3866;
            float3 _3900 = normalize(_3895);
            float3 _3901 = abs(_3900);
            float _3904 = sqrt(1.0f - _3901.z);
            float _3905 = _3901.x;
            float _3906 = _3901.y;
            float _3910 = min(_3905, _3906) / (max(_3905, _3906) + 5.4210108624275221700372640043497e-20f);
            float _3916 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3910, 0.0419038832187652587890625f), _3910, 0.08817707002162933349609375f), _3910, -0.2473337352275848388671875f), _3910, 0.006157201714813709259033203125f), _3910, 0.63622653484344482421875f), _3910, 4.0675854506844189018011093139648e-06f);
            float _3919 = (_3905 < _3906) ? (1.0f - _3916) : _3916;
            float2 _3923 = float2(mad(-_3919, _3904, _3904), _3919 * _3904);
            bool2 _3926 = (_3900.z < 0.0f).xx;
            float2 _3928 = 1.0f.xx - _3923.yx;
            bool3 _3948 = (_3859 == 4294967295u).xxx;
            float3 _3951 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3177 * uint2(_3859 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3859 >> _3183)) + ((((asfloat(asuint(float2(_3926.x ? _3928.x : _3923.x, _3926.y ? _3928.y : _3923.y)) ^ (asuint(_3900.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3189) + _3196)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            float3 _3955 = _3070.z.xxx;
            float3 _3961 = _3070.y.xxx;
            _3967 = lerp(lerp(lerp(float3(_3204.x ? 0.0f.xxx.x : _3209.x, _3204.y ? 0.0f.xxx.y : _3209.y, _3204.z ? 0.0f.xxx.z : _3209.z) * ((_3130.y * _3130.y) / (_3102 * dot(_3134, _2659))), float3(_3312.x ? 0.0f.xxx.x : _3315.x, _3312.y ? 0.0f.xxx.y : _3315.y, _3312.z ? 0.0f.xxx.z : _3315.z) * ((_3255.y * _3255.y) / (_3102 * dot(_3259, _2659))), _3955), lerp(float3(_3418.x ? 0.0f.xxx.x : _3421.x, _3418.y ? 0.0f.xxx.y : _3421.y, _3418.z ? 0.0f.xxx.z : _3421.z) * ((_3361.y * _3361.y) / (_3102 * dot(_3365, _2659))), float3(_3524.x ? 0.0f.xxx.x : _3527.x, _3524.y ? 0.0f.xxx.y : _3527.y, _3524.z ? 0.0f.xxx.z : _3527.z) * ((_3467.y * _3467.y) / (_3102 * dot(_3471, _2659))), _3955), _3961), lerp(lerp(float3(_3630.x ? 0.0f.xxx.x : _3633.x, _3630.y ? 0.0f.xxx.y : _3633.y, _3630.z ? 0.0f.xxx.z : _3633.z) * ((_3573.y * _3573.y) / (_3102 * dot(_3577, _2659))), float3(_3736.x ? 0.0f.xxx.x : _3739.x, _3736.y ? 0.0f.xxx.y : _3739.y, _3736.z ? 0.0f.xxx.z : _3739.z) * ((_3679.y * _3679.y) / (_3102 * dot(_3683, _2659))), _3955), lerp(float3(_3842.x ? 0.0f.xxx.x : _3845.x, _3842.y ? 0.0f.xxx.y : _3845.y, _3842.z ? 0.0f.xxx.z : _3845.z) * ((_3785.y * _3785.y) / (_3102 * dot(_3789, _2659))), float3(_3948.x ? 0.0f.xxx.x : _3951.x, _3948.y ? 0.0f.xxx.y : _3951.y, _3948.z ? 0.0f.xxx.z : _3951.z) * ((_3891.y * _3891.y) / (_3102 * dot(_3895, _2659))), _3955), _3961), _3070.x.xxx);
        }
        else
        {
            uint _2779 = (TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumGridCells + _1114) * 2u;
            uint _2784 = min(TranslucentBasePass_Shared_Forward_NumCulledLightsGrid[_2779], TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumReflectionCaptures);
            uint _2785 = _2779 + 1u;
            float _2792 = mad(-1.2000000476837158203125f, log2(max(_802, 0.001000000047497451305389404296875f)), 1.0f);
            float _2794 = (View_View_ReflectionCubemapMaxMip - 1.0f) - _2792;
            float2 _2796 = 0.0f.xx;
            float4 _2799 = 0.0f.xxxx;
            _2796 = float2(0.0f, 1.0f);
            _2799 = float4(0.0f, 0.0f, 0.0f, 1.0f);
            float2 _2797 = 0.0f.xx;
            float4 _2800 = 0.0f.xxxx;
            [loop]
            for (uint _2801 = 0u; _2801 < _2784; _2796 = _2797, _2799 = _2800, _2801++)
            {
                [branch]
                if (_2799.w < 0.001000000047497451305389404296875f)
                {
                    break;
                }
                uint4 _2812 = TranslucentBasePass_Shared_Forward_CulledLightDataGrid16Bit.Load(TranslucentBasePass_Shared_Forward_NumCulledLightsGrid[_2785] + _2801);
                uint _2813 = _2812.x;
                float3 _2823 = ((ReflectionCaptureSM5_ReflectionCaptureSM5_TilePosition[_2813].xyz + _465) * 2097152.0f) + (ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2813].xyz + View_View_RelativePreViewTranslation);
                float3 _2828 = _493 - _2823;
                float _2830 = sqrt(dot(_2828, _2828));
                [branch]
                if (_2830 < ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2813].w)
                {
                    float _2949 = 0.0f;
                    float3 _2950 = 0.0f.xxx;
                    [branch]
                    if (ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureProperties[_2813].z > 0.0f)
                    {
                        float3 _2881 = float4(_2823, ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2813].w).xyz;
                        float3 _2882 = _493 - _2881;
                        float3 _2888 = mul(float4(_2882, 1.0f), ReflectionCaptureSM5_ReflectionCaptureSM5_BoxTransform[_2813]).xyz;
                        float3 _2894 = mul(float4(_2659, 0.0f), ReflectionCaptureSM5_ReflectionCaptureSM5_BoxTransform[_2813]).xyz;
                        float3 _2895 = 1.0f.xxx / _2894;
                        float3 _2897 = -_2888;
                        float3 _2900 = max(mad(_2897, _2895, (-1.0f).xxx / _2894), mad(_2897, _2895, _2895));
                        float3 _2914 = ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2813].xyz - (0.5f * ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2813].w).xxx;
                        float3 _2915 = -_2914;
                        float3 _2916 = _2888 * ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2813].xyz;
                        bool3 _2917 = bool3(_2916.x < _2915.x, _2916.y < _2915.y, _2916.z < _2915.z);
                        float3 _2919 = abs(mad(_2888, ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2813].xyz, _2914));
                        bool3 _2930 = bool3(_2916.x > _2914.x, _2916.y > _2914.y, _2916.z > _2914.z);
                        float3 _2932 = abs(mad(_2888, ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2813].xyz, _2915));
                        _2949 = 1.0f - smoothstep(0.0f, 0.699999988079071044921875f * ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2813].w, dot(float3(_2917.x ? _2919.x : 0.0f, _2917.y ? _2919.y : 0.0f, _2917.z ? _2919.z : 0.0f), 1.0f.xxx) + dot(float3(_2930.x ? _2932.x : 0.0f, _2930.y ? _2932.y : 0.0f, _2930.z ? _2932.z : 0.0f), 1.0f.xxx));
                        _2950 = (_493 + (_2659 * min(_2900.x, min(_2900.y, _2900.z)))) - (_2881 + ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureOffsetAndAverageBrightness[_2813].xyz);
                    }
                    else
                    {
                        float3 _2849 = _493 - float4(_2823, ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2813].w).xyz;
                        float _2851 = dot(_2659, _2849);
                        float _2855 = mad(_2851, _2851, -mad(-ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2813].w, ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2813].w, dot(_2849, _2849)));
                        float _2870 = 0.0f;
                        float3 _2871 = 0.0f.xxx;
                        [flatten]
                        if (_2855 >= 0.0f)
                        {
                            float _2865 = clamp(mad(2.5f, clamp(_2830 / ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2813].w, 0.0f, 1.0f), -1.5f), 0.0f, 1.0f);
                            _2870 = mad(-(_2865 * _2865), mad(-2.0f, _2865, 3.0f), 1.0f);
                            _2871 = (_2849 + (_2659 * (sqrt(_2855) - _2851))) - ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureOffsetAndAverageBrightness[_2813].xyz;
                        }
                        else
                        {
                            _2870 = 0.0f;
                            _2871 = _2659;
                        }
                        _2949 = _2870;
                        _2950 = _2871;
                    }
                    float4 _2959 = TranslucentBasePass_Shared_Reflection_ReflectionCubemap.SampleLevel(TranslucentBasePass_Shared_Reflection_ReflectionCubemapSampler, float4(_2950, ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureProperties[_2813].y), _2794);
                    float3 _2962 = _2959.xyz * ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureProperties[_2813].x;
                    float4 _2964 = float4(_2962.x, _2962.y, _2962.z, _2959.w) * _2949;
                    float3 _2969 = _2799.xyz + ((_2964.xyz * _2799.w) * 1.0f);
                    float4 _2970 = float4(_2969.x, _2969.y, _2969.z, _2799.w);
                    _2970.w = _2799.w * (1.0f - _2964.w);
                    float2 _2980 = 0.0f.xx;
                    _2980.x = mad(ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureOffsetAndAverageBrightness[_2813].w * _2949, _2796.y, _2796.x);
                    _2980.y = _2796.y * (1.0f - _2949);
                    _2797 = _2980;
                    _2800 = _2970;
                }
                else
                {
                    _2797 = _2796;
                    _2800 = _2799;
                }
            }
            float3 _2987 = _2799.xyz * View_View_PrecomputedIndirectSpecularColorScale;
            float4 _2988 = float4(_2987.x, _2987.y, _2987.z, _2799.w);
            float _2991 = _2796.x * dot(View_View_PrecomputedIndirectSpecularColorScale, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f));
            float2 _2992 = 0.0f.xx;
            _2992.x = _2991;
            float4 _3034 = 0.0f.xxxx;
            float2 _3035 = 0.0f.xx;
            float3 _3036 = 0.0f.xxx;
            [branch]
            if ((TranslucentBasePass_TranslucentBasePass_Shared_Reflection_SkyLightParameters.y > 0.0f) && true)
            {
                float3 _3013 = TranslucentBasePass_Shared_Reflection_SkyLightCubemap.SampleLevel(TranslucentBasePass_Shared_Reflection_SkyLightCubemapSampler, _2659, (TranslucentBasePass_TranslucentBasePass_Shared_Reflection_SkyLightParameters.x - 1.0f) - _2792).xyz * View_View_SkyLightColor.xyz;
                float4 _3031 = 0.0f.xxxx;
                float2 _3032 = 0.0f.xx;
                float3 _3033 = 0.0f.xxx;
                [flatten]
                if ((TranslucentBasePass_TranslucentBasePass_Shared_Reflection_SkyLightParameters.z < 1.0f) && true)
                {
                    float3 _3026 = _2987.xyz + ((_3013 * _2799.w) * 1.0f);
                    float2 _3030 = 0.0f.xx;
                    _3030.x = mad(View_SkyIrradianceEnvironmentMap[7u].x * dot(View_View_SkyLightColor.xyz, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f)), _2796.y, _2991);
                    _3031 = float4(_3026.x, _3026.y, _3026.z, _2799.w);
                    _3032 = _3030;
                    _3033 = 0.0f.xxx;
                }
                else
                {
                    _3031 = _2988;
                    _3032 = _2992;
                    _3033 = _3013 * 1.0f;
                }
                _3034 = _3031;
                _3035 = _3032;
                _3036 = _3033;
            }
            else
            {
                _3034 = _2988;
                _3035 = _2992;
                _3036 = 0.0f.xxx;
            }
            _3967 = ((_3034.xyz * lerp(1.0f, min(dot(_1064, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f)) / max(_3035.x, 9.9999997473787516355514526367188e-05f), View_View_ReflectionEnvironmentRoughnessMixingScaleBiasAndLargestWeight.z), smoothstep(0.0f, 1.0f, clamp(mad(_802, View_View_ReflectionEnvironmentRoughnessMixingScaleBiasAndLargestWeight.x, View_View_ReflectionEnvironmentRoughnessMixingScaleBiasAndLargestWeight.y), 0.0f, 1.0f)))).xyz + (_3036 * _3034.w)).xyz;
        }
        _3985 = _3967;
    }
    float3 _4292 = 0.0f.xxx;
    if (((View_View_CameraCut == 0.0f) && (TranslucentBasePass_TranslucentBasePass_SSRQuality > 0)) && _2702)
    {
        float _4010 = min(_496, 1000000.0f);
        float4 _4015 = mul(float4(_2659, 0.0f), View_View_TranslatedWorldToView);
        float _4016 = _4015.z;
        float _4021 = (_4016 < 0.0f) ? min(((-0.949999988079071044921875f) * _496) / _4016, _4010) : _4010;
        float4 _4030 = mul(float4(_493, 1.0f), View_View_TranslatedWorldToClip);
        float4 _4035 = mul(float4(_493 + (_2659 * _4021), 1.0f), View_View_TranslatedWorldToClip);
        float3 _4039 = _4030.xyz * (1.0f / _4030.w);
        float4 _4046 = _4030 + mul(float4(0.0f, 0.0f, _4021, 0.0f), View_View_ViewToClip);
        float3 _4050 = _4046.xyz * (1.0f / _4046.w);
        float3 _4051 = (_4035.xyz * (1.0f / _4035.w)) - _4039;
        float2 _4052 = _4039.xy;
        float2 _4053 = _4051.xy;
        float _4055 = 0.5f * length(_4053);
        float2 _4064 = 1.0f.xx - (max(abs(_4053 + (_4052 * _4055)) - _4055.xx, 0.0f.xx) / abs(_4053));
        float3 _4069 = _4051 * (min(_4064.x, _4064.y) / _4055);
        float _4087 = 0.0f;
        if (asuint(View_View_ViewToClip[3].w) != 0u)
        {
            _4087 = max(0.0f, (_4039.z - _4050.z) * 4.0f);
        }
        else
        {
            _4087 = max(abs(_4069.z), (_4039.z - _4050.z) * 4.0f);
        }
        float _4102 = _4087 * 0.083333335816860198974609375f;
        float3 _4103 = float3((_4069.xy * float2(0.5f, -0.5f)) * TranslucentBasePass_TranslucentBasePass_HZBUvFactorAndInvFactor.xy, _4069.z) * 0.083333335816860198974609375f;
        float3 _4105 = float3(mad(_4052, float2(0.5f, -0.5f), 0.5f.xx) * TranslucentBasePass_TranslucentBasePass_HZBUvFactorAndInvFactor.xy, _4039.z) + (_4103 * (frac(52.98291778564453125f * frac(dot(gl_FragCoord.xy + (float2(32.66500091552734375f, 11.81499958038330078125f) * float(View_View_StateFrameIndexMod8)), float2(0.067110560834407806396484375f, 0.005837149918079376220703125f)))) - 0.5f));
        bool4 _4107 = bool4(false, false, false, false);
        float4 _4110 = 0.0f.xxxx;
        uint _4116 = 0u;
        float _4118 = 0.0f;
        _4107 = _372;
        _4110 = _370;
        _4116 = 0u;
        _4118 = 0.0f;
        bool4 _4108 = bool4(false, false, false, false);
        float4 _4111 = 0.0f.xxxx;
        bool _4113 = false;
        float _4115 = 0.0f;
        float _4119 = 0.0f;
        bool4 _4178 = bool4(false, false, false, false);
        float4 _4179 = 0.0f.xxxx;
        bool _4180 = false;
        bool _4112 = false;
        float _4114 = 1.0f;
        [loop]
        for (;;)
        {
            if (_4116 < 12u)
            {
                float2 _4123 = _4105.xy;
                float2 _4124 = _4103.xy;
                float _4125 = float(_4116);
                float _4126 = _4125 + 1.0f;
                float _4129 = _4105.z;
                float _4130 = _4103.z;
                float4 _4132 = 0.0f.xxxx;
                _4132.x = mad(_4126, _4130, _4129);
                float _4133 = _4125 + 2.0f;
                _4132.y = mad(_4133, _4130, _4129);
                float _4138 = _4125 + 3.0f;
                _4132.z = mad(_4138, _4130, _4129);
                float _4143 = _4125 + 4.0f;
                _4132.w = mad(_4143, _4130, _4129);
                float _4148 = mad(0.666666686534881591796875f, _802, _4114);
                _4115 = mad(0.666666686534881591796875f, _802, _4148);
                float4 _4152 = 0.0f.xxxx;
                _4152.x = TranslucentBasePass_HZBTexture.SampleLevel(TranslucentBasePass_HZBSampler, _4123 + (_4124 * _4126), _4114).x;
                _4152.y = TranslucentBasePass_HZBTexture.SampleLevel(TranslucentBasePass_HZBSampler, _4123 + (_4124 * _4133), _4114).x;
                _4152.z = TranslucentBasePass_HZBTexture.SampleLevel(TranslucentBasePass_HZBSampler, _4123 + (_4124 * _4138), _4148).x;
                _4152.w = TranslucentBasePass_HZBTexture.SampleLevel(TranslucentBasePass_HZBSampler, _4123 + (_4124 * _4143), _4148).x;
                _4111 = _4132 - _4152;
                float4 _4165 = _4102.xxxx;
                float4 _4167 = abs(_4111 + _4165);
                _4108 = bool4(_4167.x < _4165.x, _4167.y < _4165.y, _4167.z < _4165.z, _4167.w < _4165.w);
                _4113 = (((_4112 || _4108.x) || _4108.y) || _4108.z) || _4108.w;
                [branch]
                if (_4113 || false)
                {
                    _4178 = _4108;
                    _4179 = _4111;
                    _4180 = _4113;
                    break;
                }
                _4119 = _4111.w;
                _4107 = _4108;
                _4110 = _4111;
                _4112 = _4113;
                _4114 = _4115;
                _4116 += 4u;
                _4118 = _4119;
                continue;
            }
            else
            {
                _4178 = _4107;
                _4179 = _4110;
                _4180 = _4112;
                break;
            }
        }
        float3 _4219 = 0.0f.xxx;
        [branch]
        if (_4180)
        {
            float _4193 = 0.0f;
            [flatten]
            if (_4178.z)
            {
                _4193 = _4179.y;
            }
            else
            {
                _4193 = _4179.z;
            }
            float _4201 = 0.0f;
            float _4202 = 0.0f;
            [flatten]
            if (_4178.y)
            {
                _4201 = _4179.y;
                _4202 = _4179.x;
            }
            else
            {
                _4201 = _4178.z ? _4179.z : _4179.w;
                _4202 = _4193;
            }
            float _4208 = 0.0f;
            [flatten]
            if (_4178.x)
            {
                _4208 = _4179.x;
            }
            else
            {
                _4208 = _4201;
            }
            float _4209 = _4178.x ? _4118 : _4202;
            _4219 = _4105 + (_4103 * (((_4178.x ? 0.0f : (_4178.y ? 1.0f : (_4178.z ? 2.0f : 3.0f))) + float(_4116)) + clamp(_4209 / (_4209 - _4208), 0.0f, 1.0f)));
        }
        else
        {
            _4219 = _4105 + (_4103 * float(_4116));
        }
        float3 _4291 = 0.0f.xxx;
        [branch]
        if (_4180)
        {
            float2 _4233 = (mad(mad((_4219.xy * TranslucentBasePass_TranslucentBasePass_HZBUvFactorAndInvFactor.zw).xy, float2(2.0f, -2.0f), float2(-1.0f, 1.0f)).xy, View_View_ScreenPositionScaleBias.xy, View_View_ScreenPositionScaleBias.wz).xy - View_View_ScreenPositionScaleBias.wz) / View_View_ScreenPositionScaleBias.xy;
            float4 _4240 = mul(float4(_4233, _4219.z, 1.0f), View_View_ClipToPrevClip);
            float2 _4244 = _4240.xy / _4240.w.xx;
            float2 _4251 = clamp((abs(_4233) * 5.0f) - 4.0f.xx, 0.0f.xx, 1.0f.xx);
            float2 _4258 = clamp((abs(_4244) * 5.0f) - 4.0f.xx, 0.0f.xx, 1.0f.xx);
            float3 _4275 = -min(-TranslucentBasePass_PrevSceneColor.SampleLevel(TranslucentBasePass_PrevSceneColorSampler, clamp(mad(_4244, TranslucentBasePass_TranslucentBasePass_PrevScreenPositionScaleBias.xy, TranslucentBasePass_TranslucentBasePass_PrevScreenPositionScaleBias.zw), TranslucentBasePass_TranslucentBasePass_PrevSceneColorBilinearUVMin, TranslucentBasePass_TranslucentBasePass_PrevSceneColorBilinearUVMax), 0.0f).xyz, 0.0f.xxx);
            float4 _4276 = float4(_4275.x, _4275.y, _4275.z, _370.w);
            _4276.w = 1.0f;
            float4 _4281 = _4276 * (min(clamp(1.0f - dot(_4251, _4251), 0.0f, 1.0f), clamp(1.0f - dot(_4258, _4258), 0.0f, 1.0f)) * clamp(mad(-6.599999904632568359375f, _802, 2.0f), 0.0f, 1.0f));
            _4291 = (_3985 * (1.0f - _4281.w)) + (_4281.xyz * TranslucentBasePass_TranslucentBasePass_PrevSceneColorPreExposureInv).xyz;
        }
        else
        {
            _4291 = _3985;
        }
        _4292 = _4291;
    }
    else
    {
        _4292 = _3985;
    }
    float3 _4417 = 0.0f.xxx;
    [branch]
    if (abs(dot(TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ReflectionPlane.xyz, 1.0f.xxx)) > 9.9999997473787516355514526367188e-05f)
    {
        float3 _4318 = _493 - TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionOrigin.xyz;
        float _4351 = 1.0f - clamp((_802 - 0.20000000298023223876953125f) * 10.0f, 0.0f, 1.0f);
        float _4353 = (((1.0f - clamp(mad(abs(dot(TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ReflectionPlane, float4(_493, -1.0f))), TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters.x, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters.y), 0.0f, 1.0f)) * (clamp((TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionXAxis.w - abs(dot(_4318, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionXAxis.xyz))) * TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters.x, 0.0f, 1.0f) * clamp((TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionYAxis.w - abs(dot(_4318, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionYAxis.xyz))) * TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters.x, 0.0f, 1.0f))) * clamp(mad(dot(TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ReflectionPlane.xyz, _604), TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters2.x, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters2.y), 0.0f, 1.0f)) * _4351;
        float4 _4411 = 0.0f.xxxx;
        [branch]
        if (_4353 > 0.0f)
        {
            float4 _4381 = mul(float4(mul(float4(_493 + (reflect(reflect(normalize(_493 - View_View_TranslatedWorldCameraOrigin), -TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ReflectionPlane.xyz), mul(_604, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_InverseTransposeMirrorMatrix).xyz) * TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters.z), 1.0f), View_View_TranslatedWorldToView).xyz, 1.0f), TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ProjectionWithExtraFOV[View_View_StereoPassIndex]);
            uint _4388 = 0u;
            if (TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_bIsStereo != 0u)
            {
                _4388 = uint(View_View_StereoPassIndex);
            }
            else
            {
                _4388 = 0u;
            }
            float4 _4404 = TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionTexture.SampleLevel(TranslucentBasePass_Shared_Reflection_ReflectionCubemapSampler, mad(clamp(_4381.xy / _4381.w.xx, -TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenBound, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenBound), TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenScaleBias[_4388].xy, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenScaleBias[_4388].zw), 0.0f);
            float3 _4408 = _4404.xyz * _4351;
            float4 _4409 = float4(_4408.x, _4408.y, _4408.z, 0.0f.xxxx.w);
            _4409.w = _4353 * _4404.w;
            _4411 = _4409;
        }
        else
        {
            _4411 = 0.0f.xxxx;
        }
        _4417 = _4411.xyz + (_4292 * (1.0f - _4411.w));
    }
    else
    {
        _4417 = _4292;
    }
    float4 _4419 = (float4(-1.0f, -0.0274999998509883880615234375f, -0.572000026702880859375f, 0.02199999988079071044921875f) * _802) + float4(1.0f, 0.0425000004470348358154296875f, 1.03999996185302734375f, -0.039999999105930328369140625f);
    float _4420 = _4419.x;
    float2 _4429 = (float2(-1.03999996185302734375f, 1.03999996185302734375f) * mad(min(_4420 * _4420, exp2((-9.27999973297119140625f) * clamp(dot(_604, _510), 0.0f, 1.0f))), _4420, _4419.y)) + _4419.zw;
    bool _4456 = TranslucentBasePass_TranslucentBasePass_Shared_Fog_ApplyVolumetricFog > 0.0f;
    float4 _4539 = 0.0f.xxxx;
    if (_4456)
    {
        float4 _4474 = mul(((float4(View_View_ViewTilePosition, 0.0f) + float4(_464, 0.0f)) * 2097152.0f) + float4(_494, 1.0f), View_View_RelativeWorldToClip);
        float _4475 = _4474.w;
        float4 _4521 = 0.0f.xxxx;
        float _4522 = 0.0f;
        if (_4456)
        {
            float4 _4515 = TranslucentBasePass_Shared_Fog_IntegratedLightScattering.SampleLevel(View_SharedBilinearClampedSampler, min(float3(mad((_4474.xy / _4475.xx).xy, float2(0.5f, -0.5f), 0.5f.xx), (log2(mad(_4475, View_View_VolumetricFogGridZParams.x, View_View_VolumetricFogGridZParams.y)) * View_View_VolumetricFogGridZParams.z) * View_View_VolumetricFogInvGridSize.z) * float3(View_View_VolumetricFogScreenToResourceUV, 1.0f), float3(View_View_VolumetricFogUVMax, 1.0f)), 0.0f);
            float3 _4519 = _4515.xyz * View_View_OneOverPreExposure;
            _4521 = float4(_4519.x, _4519.y, _4519.z, _4515.w);
            _4522 = TranslucentBasePass_TranslucentBasePass_Shared_Fog_VolumetricFogStartDistance;
        }
        else
        {
            _4521 = float4(0.0f, 0.0f, 0.0f, 1.0f);
            _4522 = 0.0f;
        }
        float4 _4527 = lerp(float4(0.0f, 0.0f, 0.0f, 1.0f), _4521, clamp((_496 - _4522) * 100000000.0f, 0.0f, 1.0f).xxxx);
        float _4530 = _4527.w;
        _4539 = float4(_4527.xyz + (in_var_TEXCOORD7.xyz * _4530), _4530 * in_var_TEXCOORD7.w);
    }
    else
    {
        _4539 = in_var_TEXCOORD7;
    }
    float3 _4546 = max(lerp(0.0f.xxx, Material_Material_PreshaderBuffer[2].xyz, Material_Material_PreshaderBuffer[1].w.xxx), 0.0f.xxx);
    float _4631 = 0.0f;
    float3 _4632 = 0.0f.xxx;
    [branch]
    if (View_View_OutOfBoundsMask > 0.0f)
    {
        uint _4572 = _511 + 31u;
        float3 _4581 = abs(((View_View_ViewTilePosition - Scene_GPUScene_GPUScenePrimitiveSceneData[_511 + 1u].xyz) * 2097152.0f) + (_494 - Scene_GPUScene_GPUScenePrimitiveSceneData[_511 + 18u].xyz));
        float3 _4582 = float3(Scene_GPUScene_GPUScenePrimitiveSceneData[_511 + 17u].w, Scene_GPUScene_GPUScenePrimitiveSceneData[_511 + 24u].w, Scene_GPUScene_GPUScenePrimitiveSceneData[_511 + 25u].w) + 1.0f.xxx;
        float _4629 = 0.0f;
        float3 _4630 = 0.0f.xxx;
        if (any(bool3(_4581.x > _4582.x, _4581.y > _4582.y, _4581.z > _4582.z)))
        {
            float3 _4609 = View_View_ViewTilePosition * 0.57700002193450927734375f.xxx;
            float3 _4610 = _494 * 0.57700002193450927734375f.xxx;
            float3 _4625 = frac(mad((_4610.x + _4610.y) + _4610.z, 0.00200000009499490261077880859375f, frac(((_4609.x + _4609.y) + _4609.z) * 4194.30419921875f))).xxx;
            _4629 = 1.0f;
            _4630 = lerp(float3(1.0f, 1.0f, 0.0f), float3(0.0f, 1.0f, 1.0f), float3(bool3(_4625.x > 0.5f.xxx.x, _4625.y > 0.5f.xxx.y, _4625.z > 0.5f.xxx.z)));
        }
        else
        {
            float _4607 = 0.0f;
            float3 _4608 = 0.0f.xxx;
            if (Scene_GPUScene_GPUScenePrimitiveSceneData[_4572].x > 0.0f)
            {
                float3 _4592 = abs(_493 - in_var_TEXCOORD9);
                float _4602 = 1.0f - clamp(abs(max(_4592.x, max(_4592.y, _4592.z)) - Scene_GPUScene_GPUScenePrimitiveSceneData[_4572].x) * 20.0f, 0.0f, 1.0f);
                _4607 = float(int(sign(_4602)));
                _4608 = float3(1.0f, 0.0f, 1.0f) * _4602;
            }
            else
            {
                _4607 = 1.0f;
                _4608 = _4546;
            }
            _4629 = _4607;
            _4630 = _4608;
        }
        _4631 = _4629;
        _4632 = _4630;
    }
    else
    {
        _4631 = 1.0f;
        _4632 = _4546;
    }
    float4 _4642 = float4(((mad(_1064 * _847, max(1.0f.xxx, ((((((_796 * 2.040400028228759765625f) - 0.3323999941349029541015625f.xxx) * 1.0f) + ((_796 * (-4.79510021209716796875f)) + 0.6417000293731689453125f.xxx)) * 1.0f) + ((_796 * 2.755199909210205078125f) + 0.69029998779296875f.xxx)) * 1.0f), lerp(mad((_4417 * ((_849 * _4429.x) + (clamp(50.0f * _849.y, 0.0f, 1.0f) * _4429.y).xxx)) * 1.0f, max(1.0f.xxx, ((((((_849 * 2.040400028228759765625f) - 0.3323999941349029541015625f.xxx) * 1.0f) + ((_849 * (-4.79510021209716796875f)) + 0.6417000293731689453125f.xxx)) * 1.0f) + ((_849 * 2.755199909210205078125f) + 0.69029998779296875f.xxx)) * 1.0f), float4(_2650.x ? 0.0f.xxxx.x : _2205.x, _2650.y ? 0.0f.xxxx.y : _2205.y, _2650.z ? 0.0f.xxxx.z : _2205.z, _2650.w ? 0.0f.xxxx.w : _2205.w).xyz + float4(_2650.x ? 0.0f.xxxx.x : _2208.x, _2650.y ? 0.0f.xxxx.y : _2208.y, _2650.z ? 0.0f.xxxx.z : _2208.z, _2650.w ? 0.0f.xxxx.w : _2208.w).xyz), _847 + (_849 * 0.449999988079071044921875f), View_View_UnlitViewmodeMask.xxx)) + _4632) * _4539.w) + _4539.xyz, _4631);
    float3 _4648 = min((_4642.xyz * View_View_PreExposure).xyz, 32256.0f.xxx);
    out_var_SV_Target0 = float4(_4648.x, _4648.y, _4648.z, _4642.w);
}

SPIRV_Cross_Output main(SPIRV_Cross_Input stage_input)
{
    gl_FragCoord = stage_input.gl_FragCoord;
    gl_FragCoord.w = 1.0 / gl_FragCoord.w;
    gl_FrontFacing = stage_input.gl_FrontFacing;
    in_var_TEXCOORD10_centroid = stage_input.in_var_TEXCOORD10_centroid;
    in_var_TEXCOORD11_centroid = stage_input.in_var_TEXCOORD11_centroid;
    in_var_TEXCOORD0 = stage_input.in_var_TEXCOORD0;
    in_var_TEXCOORD4 = stage_input.in_var_TEXCOORD4;
    in_var_PRIMITIVE_ID = stage_input.in_var_PRIMITIVE_ID;
    in_var_LIGHTMAP_ID = stage_input.in_var_LIGHTMAP_ID;
    in_var_TEXCOORD7 = stage_input.in_var_TEXCOORD7;
    in_var_TEXCOORD9 = stage_input.in_var_TEXCOORD9;
    frag_main();
    SPIRV_Cross_Output stage_output;
    stage_output.out_var_SV_Target0 = out_var_SV_Target0;
    return stage_output;
}
