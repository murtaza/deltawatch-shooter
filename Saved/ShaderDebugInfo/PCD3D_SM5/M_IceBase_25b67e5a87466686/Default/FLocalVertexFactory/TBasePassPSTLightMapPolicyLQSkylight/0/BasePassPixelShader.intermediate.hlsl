#pragma warning(disable : 3571) // pow() intrinsic suggested to be used with abs()
static uint _340 = 0u;
static float3 _366 = 0.0f.xxx;
static float4 _367 = 0.0f.xxxx;
static float4x4 _368 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
static bool4 _369 = bool4(false, false, false, false);
static float _371 = 0.0f;
static float4 _372 = 0.0f.xxxx;
static float2 _373 = 0.0f.xx;

cbuffer View
{
    row_major float4x4 View_View_TranslatedWorldToClip : packoffset(c0);
    row_major float4x4 View_View_RelativeWorldToClip : packoffset(c4);
    row_major float4x4 View_View_TranslatedWorldToView : packoffset(c12);
    row_major float4x4 View_View_ViewToClip : packoffset(c28);
    row_major float4x4 View_View_SVPositionToTranslatedWorld : packoffset(c44);
    float3 View_View_ViewTilePosition : packoffset(c60);
    float3 View_View_MatrixTilePosition : packoffset(c61);
    float3 View_View_ViewForward : packoffset(c62);
    float4 View_View_InvDeviceZToWorldZTransform : packoffset(c67);
    float4 View_View_ScreenPositionScaleBias : packoffset(c68);
    float3 View_View_RelativeWorldCameraOrigin : packoffset(c69);
    float3 View_View_TranslatedWorldCameraOrigin : packoffset(c70);
    float3 View_View_RelativePreViewTranslation : packoffset(c72);
    row_major float4x4 View_View_ClipToPrevClip : packoffset(c113);
    float4 View_View_ViewRectMin : packoffset(c124);
    float4 View_View_ViewSizeAndInvSize : packoffset(c125);
    float4 View_View_LightProbeSizeRatioAndInvSizeRatio : packoffset(c127);
    float View_View_PreExposure : packoffset(c132.z);
    float View_View_OneOverPreExposure : packoffset(c132.w);
    float4 View_View_DiffuseOverrideParameter : packoffset(c133);
    float4 View_View_SpecularOverrideParameter : packoffset(c134);
    float4 View_View_NormalOverrideParameter : packoffset(c135);
    float2 View_View_RoughnessOverrideParameter : packoffset(c136);
    float View_View_OutOfBoundsMask : packoffset(c137);
    float View_View_CullingSign : packoffset(c138.w);
    float View_View_MaterialTextureMipBias : packoffset(c140);
    uint View_View_StateFrameIndexMod8 : packoffset(c141.y);
    float View_View_CameraCut : packoffset(c142.y);
    float View_View_UnlitViewmodeMask : packoffset(c142.z);
    float3 View_View_PrecomputedIndirectLightingColorScale : packoffset(c155);
    float3 View_View_PrecomputedIndirectSpecularColorScale : packoffset(c156);
    float View_View_RenderingReflectionCaptureMask : packoffset(c179.w);
    float4 View_View_SkyLightColor : packoffset(c183);
    float View_View_ReflectionCubemapMaxMip : packoffset(c192.z);
    float3 View_View_ReflectionEnvironmentRoughnessMixingScaleBiasAndLargestWeight : packoffset(c194);
    int View_View_StereoPassIndex : packoffset(c194.w);
    float3 View_View_VolumetricFogInvGridSize : packoffset(c225);
    float3 View_View_VolumetricFogGridZParams : packoffset(c226);
    float2 View_View_VolumetricFogScreenToResourceUV : packoffset(c229);
    float2 View_View_VolumetricFogUVMax : packoffset(c229.z);
    float View_View_MinRoughness : packoffset(c243.z);
};

StructuredBuffer<float4> View_SkyIrradianceEnvironmentMap;
StructuredBuffer<float4> Scene_GPUScene_GPUScenePrimitiveSceneData;
StructuredBuffer<float4> Scene_GPUScene_GPUSceneLightmapData;
cbuffer TranslucentBasePass
{
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumLocalLights : packoffset(c0);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumReflectionCaptures : packoffset(c0.y);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_HasDirectionalLight : packoffset(c0.z);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumGridCells : packoffset(c0.w);
    int3 TranslucentBasePass_TranslucentBasePass_Shared_Forward_CulledGridSize : packoffset(c1);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridPixelSizeShift : packoffset(c2);
    float3 TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridZParams : packoffset(c3);
    float3 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDirection : packoffset(c4);
    float TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius : packoffset(c4.w);
    float3 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightColor : packoffset(c5);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowMapChannelMask : packoffset(c6);
    float2 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDistanceFadeMAD : packoffset(c6.z);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumDirectionalLightCascades : packoffset(c7);
    int TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightVSM : packoffset(c7.y);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_Forward_CascadeEndDepths : packoffset(c8);
    row_major float4x4 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightTranslatedWorldToShadowMatrix[4] : packoffset(c9);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapMinMax[4] : packoffset(c25);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapAtlasBufferSize : packoffset(c29);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightUseStaticShadowing : packoffset(c30.y);
    row_major float4x4 TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightTranslatedWorldToStaticShadow : packoffset(c33);
    uint TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectLightingShowFlag : packoffset(c37);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_Reflection_SkyLightParameters : packoffset(c90);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ReflectionPlane : packoffset(c95);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionOrigin : packoffset(c96);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionXAxis : packoffset(c97);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionYAxis : packoffset(c98);
    row_major float3x4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_InverseTransposeMirrorMatrix : packoffset(c99);
    float3 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters : packoffset(c102);
    float2 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters2 : packoffset(c103);
    row_major float4x4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ProjectionWithExtraFOV[2] : packoffset(c104);
    float4 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenScaleBias[2] : packoffset(c112);
    float2 TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenBound : packoffset(c114);
    uint TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_bIsStereo : packoffset(c114.z);
    float TranslucentBasePass_TranslucentBasePass_Shared_Fog_ApplyVolumetricFog : packoffset(c124.w);
    float TranslucentBasePass_TranslucentBasePass_Shared_Fog_VolumetricFogStartDistance : packoffset(c125);
    uint TranslucentBasePass_TranslucentBasePass_Shared_UseBasePassSkylight : packoffset(c140);
    float4 TranslucentBasePass_TranslucentBasePass_HZBUvFactorAndInvFactor : packoffset(c161);
    float4 TranslucentBasePass_TranslucentBasePass_PrevScreenPositionScaleBias : packoffset(c162);
    float2 TranslucentBasePass_TranslucentBasePass_PrevSceneColorBilinearUVMin : packoffset(c163);
    float2 TranslucentBasePass_TranslucentBasePass_PrevSceneColorBilinearUVMax : packoffset(c163.z);
    float TranslucentBasePass_TranslucentBasePass_PrevSceneColorPreExposureInv : packoffset(c164);
    int TranslucentBasePass_TranslucentBasePass_SSRQuality : packoffset(c164.y);
    float TranslucentBasePass_TranslucentBasePass_ReprojectionRadiusScale : packoffset(c171);
    float TranslucentBasePass_TranslucentBasePass_InvClipmapFadeSize : packoffset(c171.w);
    uint TranslucentBasePass_TranslucentBasePass_RadianceProbeClipmapResolution : packoffset(c172.z);
    uint TranslucentBasePass_TranslucentBasePass_NumRadianceProbeClipmaps : packoffset(c172.w);
    uint TranslucentBasePass_TranslucentBasePass_RadianceProbeResolution : packoffset(c173);
    uint TranslucentBasePass_TranslucentBasePass_FinalProbeResolution : packoffset(c173.y);
    uint TranslucentBasePass_TranslucentBasePass_FinalRadianceAtlasMaxMip : packoffset(c173.z);
    float4 TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[6] : packoffset(c178);
    float4 TranslucentBasePass_TranslucentBasePass_PaddedWorldPositionToRadianceProbeCoordBias[6] : packoffset(c184);
    float4 TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[6] : packoffset(c190);
    float2 TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution : packoffset(c196);
    uint TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask : packoffset(c198);
    uint TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionDivideShift : packoffset(c198.y);
    uint TranslucentBasePass_TranslucentBasePass_Enabled : packoffset(c200.z);
    float TranslucentBasePass_TranslucentBasePass_RelativeDepthThreshold : packoffset(c200.w);
    float TranslucentBasePass_TranslucentBasePass_SpecularScale : packoffset(c201);
    float TranslucentBasePass_TranslucentBasePass_Contrast : packoffset(c201.y);
    float3 TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridZParams : packoffset(c205);
    int3 TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridSize : packoffset(c206);
};

StructuredBuffer<float4> TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer;
StructuredBuffer<uint> TranslucentBasePass_Shared_Forward_NumCulledLightsGrid;
StructuredBuffer<float4> TranslucentBasePass_ProbeWorldOffset;
cbuffer ReflectionCaptureSM5
{
    float4 ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[341] : packoffset(c0);
    float4 ReflectionCaptureSM5_ReflectionCaptureSM5_TilePosition[341] : packoffset(c341);
    float4 ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureProperties[341] : packoffset(c682);
    float4 ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureOffsetAndAverageBrightness[341] : packoffset(c1023);
    row_major float4x4 ReflectionCaptureSM5_ReflectionCaptureSM5_BoxTransform[341] : packoffset(c1364);
    float4 ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[341] : packoffset(c2728);
};

ByteAddressBuffer VirtualShadowMap_ProjectionData;
StructuredBuffer<uint> VirtualShadowMap_PageTable;
cbuffer Material
{
    float4 Material_Material_PreshaderBuffer[28] : packoffset(c0);
};

SamplerState View_SharedPointClampedSampler;
SamplerState View_SharedBilinearClampedSampler;
Texture2D<float4> TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapAtlas;
SamplerState TranslucentBasePass_Shared_Forward_ShadowmapSampler;
Texture2D<float4> TranslucentBasePass_Shared_Forward_DirectionalLightStaticShadowmap;
SamplerState TranslucentBasePass_Shared_Forward_StaticShadowmapSampler;
Buffer<uint4> TranslucentBasePass_Shared_Forward_CulledLightDataGrid16Bit;
TextureCube<float4> TranslucentBasePass_Shared_Reflection_SkyLightCubemap;
SamplerState TranslucentBasePass_Shared_Reflection_SkyLightCubemapSampler;
TextureCubeArray<float4> TranslucentBasePass_Shared_Reflection_ReflectionCubemap;
SamplerState TranslucentBasePass_Shared_Reflection_ReflectionCubemapSampler;
Texture2D<float4> TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionTexture;
Texture3D<float4> TranslucentBasePass_Shared_Fog_IntegratedLightScattering;
Texture3D<uint4> TranslucentBasePass_RadianceProbeIndirectionTexture;
Texture2D<float4> TranslucentBasePass_RadianceCacheFinalRadianceAtlas;
Texture2D<float4> TranslucentBasePass_Radiance;
Texture2D<float4> TranslucentBasePass_SceneDepth;
Texture3D<float4> TranslucentBasePass_TranslucencyGIVolumeHistory0;
Texture3D<float4> TranslucentBasePass_TranslucencyGIVolumeHistory1;
SamplerState TranslucentBasePass_TranslucencyGIVolumeSampler;
Texture2D<float4> TranslucentBasePass_HZBTexture;
SamplerState TranslucentBasePass_HZBSampler;
Texture2D<float4> TranslucentBasePass_PrevSceneColor;
SamplerState TranslucentBasePass_PrevSceneColorSampler;
Texture2D<float4> LightmapResourceCluster_LightMapTexture;
SamplerState LightmapResourceCluster_LightMapSampler;
Texture2DArray<uint4> VirtualShadowMap_PhysicalPagePool;
Texture2D<float4> Material_Texture2D_0;
SamplerState Material_Texture2D_0Sampler;
Texture2D<float4> Material_Texture2D_1;
SamplerState Material_Texture2D_1Sampler;
Texture2D<float4> Material_Texture2D_2;
SamplerState Material_Texture2D_2Sampler;
Texture2D<float4> Material_Texture2D_3;
SamplerState Material_Texture2D_3Sampler;
Texture2D<float4> Material_Texture2D_4;
SamplerState Material_Texture2D_4Sampler;
Texture2D<float4> Material_Texture2D_5;
SamplerState Material_Texture2D_5Sampler;
Texture2D<float4> Material_Texture2D_6;
SamplerState Material_Texture2D_6Sampler;

static float4 gl_FragCoord;
static bool gl_FrontFacing;
static float4 in_var_TEXCOORD10_centroid;
static float4 in_var_TEXCOORD11_centroid;
static float4 in_var_TEXCOORD0[1];
static float4 in_var_TEXCOORD4;
static uint in_var_PRIMITIVE_ID;
static uint in_var_LIGHTMAP_ID;
static float4 in_var_TEXCOORD7;
static float3 in_var_TEXCOORD9;
static float4 out_var_SV_Target0;

struct SPIRV_Cross_Input
{
    float4 in_var_TEXCOORD10_centroid : TEXCOORD10_centroid;
    float4 in_var_TEXCOORD11_centroid : TEXCOORD11_centroid;
    float4 in_var_TEXCOORD0[1] : TEXCOORD0;
    float4 in_var_TEXCOORD4 : TEXCOORD4;
    nointerpolation uint in_var_PRIMITIVE_ID : PRIMITIVE_ID;
    nointerpolation uint in_var_LIGHTMAP_ID : LIGHTMAP_ID;
    float4 in_var_TEXCOORD7 : TEXCOORD7;
    float3 in_var_TEXCOORD9 : TEXCOORD9;
    float4 gl_FragCoord : SV_Position;
    bool gl_FrontFacing : SV_IsFrontFace;
};

struct SPIRV_Cross_Output
{
    float4 out_var_SV_Target0 : SV_Target0;
};

uint spvPackHalf2x16(float2 value)
{
    uint2 Packed = f32tof16(value);
    return Packed.x | (Packed.y << 16);
}

float2 spvUnpackHalf2x16(uint value)
{
    return f16tof32(uint2(value & 0xffff, value >> 16));
}

void frag_main()
{
    float _423 = 1.0f / gl_FragCoord.w;
    float3 _461 = -View_View_MatrixTilePosition;
    float3 _462 = -View_View_ViewTilePosition;
    float3x3 _471 = float3x3(in_var_TEXCOORD10_centroid.xyz, cross(in_var_TEXCOORD11_centroid.xyz, in_var_TEXCOORD10_centroid.xyz) * in_var_TEXCOORD11_centroid.w, in_var_TEXCOORD11_centroid.xyz);
    float2 _474 = gl_FragCoord.xy - View_View_ViewRectMin.xy;
    float4 _481 = float4(mad(_474, View_View_ViewSizeAndInvSize.zw, (-0.5f).xx) * float2(2.0f, -2.0f), _371, 1.0f) * _423;
    float4 _486 = mul(float4(gl_FragCoord.xyz, 1.0f), View_View_SVPositionToTranslatedWorld);
    float3 _490 = _486.xyz / _486.w.xxx;
    float3 _491 = _490 - View_View_RelativePreViewTranslation;
    float _493 = _481.w;
    float2 _498 = mad(_481.xy / _493.xx, View_View_ScreenPositionScaleBias.xy, View_View_ScreenPositionScaleBias.wz);
    float3 _507 = 0.0f.xxx;
    if (View_View_ViewToClip[3].w >= 1.0f)
    {
        _507 = -View_View_ViewForward;
    }
    else
    {
        _507 = normalize(-_490);
    }
    uint _508 = in_var_PRIMITIVE_ID * 41u;
    float4 _525 = Material_Texture2D_0.SampleBias(Material_Texture2D_0Sampler, float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y), View_View_MaterialTextureMipBias);
    float _526 = _525.x;
    float2 _532 = mul(_471, _507).xy;
    float2 _534 = mad(_532, mad(0.0500000007450580596923828125f, _526 * Material_Material_PreshaderBuffer[1].x, -0.02500000037252902984619140625f).xx, float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y));
    float2 _540 = mad(Material_Texture2D_1.SampleBias(Material_Texture2D_1Sampler, _534, View_View_MaterialTextureMipBias).xy, 2.0f.xx, (-1.0f).xx);
    float2 _551 = float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y) * Material_Material_PreshaderBuffer[1].y.xx;
    float2 _554 = Material_Material_PreshaderBuffer[1].z.xx;
    float2 _561 = mad(Material_Texture2D_2.SampleBias(Material_Texture2D_2Sampler, _551 * _554, View_View_MaterialTextureMipBias).xy, 2.0f.xx, (-1.0f).xx);
    float2 _574 = mad(Material_Texture2D_2.SampleBias(Material_Texture2D_2Sampler, (_551 * 1.618000030517578125f.xx) * _554, View_View_MaterialTextureMipBias).xy, 2.0f.xx, (-1.0f).xx);
    float3 _601 = normalize(mul(normalize((lerp(float4(_540, sqrt(clamp(1.0f - dot(_540, _540), 0.0f, 1.0f)), 1.0f).xyz, (float4(_561, sqrt(clamp(1.0f - dot(_561, _561), 0.0f, 1.0f)), 1.0f).xyz + float4(_574, sqrt(clamp(1.0f - dot(_574, _574), 0.0f, 1.0f)), 1.0f).xyz) * float3(1.0f, 1.0f, 0.5f), Material_Texture2D_3.SampleBias(Material_Texture2D_3Sampler, float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y), View_View_MaterialTextureMipBias).x.xxx) * View_View_NormalOverrideParameter.w) + View_View_NormalOverrideParameter.xyz), _471)) * ((View_View_CullingSign * (((asuint(Scene_GPUScene_GPUScenePrimitiveSceneData[_508].x) & 64u) != 0u) ? (-1.0f) : 1.0f)) * float(gl_FrontFacing ? 1 : (-1)));
    float3 _629 = Material_Texture2D_5.SampleBias(Material_Texture2D_5Sampler, _534, View_View_MaterialTextureMipBias).xyz * Material_Material_PreshaderBuffer[4].w.xxx;
    float _632 = _629.x;
    float _636 = _629.y;
    float _640 = _629.z;
    float3 _644 = float3((_632 <= 0.0f) ? 0.0f : pow(_632, Material_Material_PreshaderBuffer[5].x), (_636 <= 0.0f) ? 0.0f : pow(_636, Material_Material_PreshaderBuffer[5].x), (_640 <= 0.0f) ? 0.0f : pow(_640, Material_Material_PreshaderBuffer[5].x));
    float2 _694 = float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y) * Material_Material_PreshaderBuffer[13].w.xx;
    float2 _700 = mad(_532, mad(0.0500000007450580596923828125f, _526 * Material_Material_PreshaderBuffer[14].x, -0.02500000037252902984619140625f).xx, _694);
    float3 _717 = Material_Texture2D_5.SampleBias(Material_Texture2D_5Sampler, _700, View_View_MaterialTextureMipBias).xyz * Material_Material_PreshaderBuffer[16].w.xxx;
    float _720 = _717.x;
    float _724 = _717.y;
    float _728 = _717.z;
    float3 _732 = float3((_720 <= 0.0f) ? 0.0f : pow(_720, Material_Material_PreshaderBuffer[17].x), (_724 <= 0.0f) ? 0.0f : pow(_724, Material_Material_PreshaderBuffer[17].x), (_728 <= 0.0f) ? 0.0f : pow(_728, Material_Material_PreshaderBuffer[17].x));
    float3 _793 = clamp(lerp(mad(Material_Texture2D_4.SampleBias(Material_Texture2D_4Sampler, _534, View_View_MaterialTextureMipBias).xyz * Material_Material_PreshaderBuffer[2].w.xxx, Material_Material_PreshaderBuffer[4].xyz, lerp(_644, dot(_644, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f)).xxx, Material_Material_PreshaderBuffer[5].y.xxx) * Material_Material_PreshaderBuffer[7].xyz) + mad(Material_Material_PreshaderBuffer[10].xyz, (Material_Texture2D_6.SampleBias(Material_Texture2D_6Sampler, mad(_532, mad(0.0500000007450580596923828125f, Material_Material_PreshaderBuffer[9].x, -0.02500000037252902984619140625f).xx, float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y)), View_View_MaterialTextureMipBias).y * Material_Material_PreshaderBuffer[9].y).xxx, Material_Material_PreshaderBuffer[13].xyz * (Material_Texture2D_6.SampleBias(Material_Texture2D_6Sampler, mad(_532, mad(0.0500000007450580596923828125f, Material_Material_PreshaderBuffer[12].x, -0.02500000037252902984619140625f).xx, float2(in_var_TEXCOORD0[0].x, in_var_TEXCOORD0[0].y)), View_View_MaterialTextureMipBias).x * Material_Material_PreshaderBuffer[12].y).xxx), mad(Material_Texture2D_4.SampleBias(Material_Texture2D_4Sampler, _700, View_View_MaterialTextureMipBias).xyz * Material_Material_PreshaderBuffer[14].y.xxx, Material_Material_PreshaderBuffer[16].xyz, lerp(_732, dot(_732, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f)).xxx, Material_Material_PreshaderBuffer[17].y.xxx) * Material_Material_PreshaderBuffer[19].xyz) + mad(Material_Material_PreshaderBuffer[22].xyz, (Material_Texture2D_6.SampleBias(Material_Texture2D_6Sampler, mad(_532, mad(0.0500000007450580596923828125f, Material_Material_PreshaderBuffer[21].x, -0.02500000037252902984619140625f).xx, _694), View_View_MaterialTextureMipBias).y * Material_Material_PreshaderBuffer[21].y).xxx, Material_Material_PreshaderBuffer[25].xyz * (Material_Texture2D_6.SampleBias(Material_Texture2D_6Sampler, mad(_532, mad(0.0500000007450580596923828125f, Material_Material_PreshaderBuffer[24].x, -0.02500000037252902984619140625f).xx, _694), View_View_MaterialTextureMipBias).x * Material_Material_PreshaderBuffer[24].y).xxx), (length(((View_View_ViewTilePosition - View_View_ViewTilePosition) * 2097152.0f) + (_491 - View_View_RelativeWorldCameraOrigin)) * Material_Material_PreshaderBuffer[26].x).xxx), 0.0f.xxx, 1.0f.xxx);
    float _794 = clamp(Material_Material_PreshaderBuffer[26].y, 0.0f, 1.0f);
    float _799 = mad(clamp(Material_Material_PreshaderBuffer[26].w, 0.0f, 1.0f), View_View_RoughnessOverrideParameter.y, View_View_RoughnessOverrideParameter.x);
    float3 _812 = ((_793 - (_793 * _794)) * View_View_DiffuseOverrideParameter.w) + View_View_DiffuseOverrideParameter.xyz;
    float3 _819 = (lerp((0.07999999821186065673828125f * clamp(Material_Material_PreshaderBuffer[26].z, 0.0f, 1.0f)).xxx, _793, _794.xxx) * View_View_SpecularOverrideParameter.w) + View_View_SpecularOverrideParameter.xyz;
    bool _822 = View_View_RenderingReflectionCaptureMask != 0.0f;
    float3 _827 = 0.0f.xxx;
    if (_822)
    {
        _827 = _812 + (_819 * 0.449999988079071044921875f);
    }
    else
    {
        _827 = _812;
    }
    bool3 _828 = _822.xxx;
    float3 _829 = float3(_828.x ? 0.0f.xxx.x : _819.x, _828.y ? 0.0f.xxx.y : _819.y, _828.z ? 0.0f.xxx.z : _819.z);
    float4 _836 = LightmapResourceCluster_LightMapTexture.Sample(LightmapResourceCluster_LightMapSampler, in_var_TEXCOORD4.xy * float2(1.0f, 0.5f));
    float4 _838 = LightmapResourceCluster_LightMapTexture.Sample(LightmapResourceCluster_LightMapSampler, mad(in_var_TEXCOORD4.xy, float2(1.0f, 0.5f), float2(0.0f, 0.5f)));
    uint _840 = in_var_LIGHTMAP_ID * 15u;
    float3 _849 = mad(_836.xyz, Scene_GPUScene_GPUSceneLightmapData[_840 + 4u].xyz, Scene_GPUScene_GPUSceneLightmapData[_840 + 6u].xyz);
    float _850 = dot(_849, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f));
    float _861 = _601.y;
    float _863 = _601.x;
    float3 _1008 = 0.0f.xxx;
    if (TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridSize.z > 0)
    {
        float4 _941 = mul(((float4(View_View_ViewTilePosition, 0.0f) + float4(_461, 0.0f)) * 2097152.0f) + float4(_491, 1.0f), View_View_RelativeWorldToClip);
        float _942 = _941.w;
        float3 _961 = float3(mad((_941.xy / _942.xx).xy, float2(0.5f, -0.5f), 0.5f.xx), (log2(mad(_942, TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridZParams.x, TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridZParams.y)) * TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridZParams.z) / float(TranslucentBasePass_TranslucentBasePass_TranslucencyGIGridSize.z));
        float4 _965 = TranslucentBasePass_TranslucencyGIVolumeHistory0.SampleLevel(TranslucentBasePass_TranslucencyGIVolumeSampler, _961, 0.0f);
        float3 _966 = _965.xyz;
        float3 _970 = TranslucentBasePass_TranslucencyGIVolumeHistory1.SampleLevel(TranslucentBasePass_TranslucencyGIVolumeSampler, _961, 0.0f).xyz;
        float4 _972 = 0.0f.xxxx;
        _972.x = _965.x;
        float4 _974 = 0.0f.xxxx;
        _974.x = _965.y;
        float4 _976 = 0.0f.xxxx;
        _976.x = _965.z;
        float3 _980 = _966 / (dot(_966, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f)) + 9.9999997473787516355514526367188e-06f).xxx;
        float3 _982 = _970 * _980.x;
        float3 _985 = _970 * _980.y;
        float3 _988 = _970 * _980.z;
        float4 _991 = 0.0f.xxxx;
        _991.y = (-0.48860299587249755859375f) * _861;
        _991.z = 0.48860299587249755859375f * _601.z;
        _991.w = (-0.48860299587249755859375f) * _863;
        _991.x = 0.886227548122406005859375f;
        float3 _998 = _991.yzw * 2.094395160675048828125f;
        float4 _999 = float4(_991.x, _998.x, _998.y, _998.z);
        float3 _1001 = 0.0f.xxx;
        _1001.x = dot(float4(_972.x, _982.x, _982.y, _982.z), _999);
        _1001.y = dot(float4(_974.x, _985.x, _985.y, _985.z), _999);
        _1001.z = dot(float4(_976.x, _988.x, _988.y, _988.z), _999);
        _1008 = max(0.0f.xxx, _1001) * 0.3183098733425140380859375f.xxx;
    }
    else
    {
        float3 _925 = 0.0f.xxx;
        if (TranslucentBasePass_TranslucentBasePass_Shared_UseBasePassSkylight > 0u)
        {
            float4 _884 = float4(_863, _861, _601.z, 1.0f);
            float3 _888 = 0.0f.xxx;
            _888.x = dot(View_SkyIrradianceEnvironmentMap[0u], _884);
            _888.y = dot(View_SkyIrradianceEnvironmentMap[1u], _884);
            _888.z = dot(View_SkyIrradianceEnvironmentMap[2u], _884);
            float4 _899 = _884.xyzz * _884.yzzx;
            float3 _903 = 0.0f.xxx;
            _903.x = dot(View_SkyIrradianceEnvironmentMap[3u], _899);
            _903.y = dot(View_SkyIrradianceEnvironmentMap[4u], _899);
            _903.z = dot(View_SkyIrradianceEnvironmentMap[5u], _899);
            _925 = (max(0.0f.xxx, (_888 + _903) + (View_SkyIrradianceEnvironmentMap[6u].xyz * mad(_863, _863, -(_861 * _861)))) * View_View_SkyLightColor.xyz) * 1.0f;
        }
        else
        {
            _925 = 0.0f.xxx;
        }
        _1008 = _925;
    }
    float3 _1009 = mad(_849 * (((exp2(mad(_850, 16.0f, -8.0f)) - 0.00390625f) * max(0.0f, dot(mad(_838, Scene_GPUScene_GPUSceneLightmapData[_840 + 5u], Scene_GPUScene_GPUSceneLightmapData[_840 + 7u]), float4(_861, _601.z, _863, 1.0f)))) / max(9.9999997473787516355514526367188e-06f, _850)), View_View_PrecomputedIndirectLightingColorScale, _1008);
    uint2 _1049 = uint2(_474 * View_View_LightProbeSizeRatioAndInvSizeRatio.zw) >> (TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridPixelSizeShift.xx & uint2(31u, 31u));
    uint _1059 = (((min(uint(max(0.0f, log2(mad(_423, TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridZParams.x, TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridZParams.y)) * TranslucentBasePass_TranslucentBasePass_Shared_Forward_LightGridZParams.z)), uint(TranslucentBasePass_TranslucentBasePass_Shared_Forward_CulledGridSize.z - 1)) * uint(TranslucentBasePass_TranslucentBasePass_Shared_Forward_CulledGridSize.y)) + _1049.y) * uint(TranslucentBasePass_TranslucentBasePass_Shared_Forward_CulledGridSize.x)) + _1049.x;
    uint _1062 = asuint(Scene_GPUScene_GPUScenePrimitiveSceneData[_508].x);
    uint _1075 = (uint((_1062 & 2048u) != 0u) | (uint((_1062 & 4096u) != 0u) << 1u)) | (uint((_1062 & 8192u) != 0u) << 2u);
    float4 _2136 = 0.0f.xxxx;
    float4 _2137 = 0.0f.xxxx;
    float4 _2138 = 0.0f.xxxx;
    [branch]
    if (TranslucentBasePass_TranslucentBasePass_Shared_Forward_HasDirectionalLight != 0u)
    {
        float4 _1100 = float4(_371, float((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowMapChannelMask & 2u) >> 1u), float((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowMapChannelMask & 4u) >> 2u), float((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowMapChannelMask & 8u) >> 3u));
        _1100.x = 1.0f;
        float _1143 = 0.0f;
        [branch]
        if (TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightUseStaticShadowing > 0u)
        {
            float4 _1115 = mul(float4(_490, 1.0f), TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightTranslatedWorldToStaticShadow);
            float2 _1119 = _1115.xy / _1115.w.xx;
            bool2 _1120 = bool2(_1119.x >= 0.0f.xx.x, _1119.y >= 0.0f.xx.y);
            bool2 _1121 = bool2(_1119.x <= 1.0f.xx.x, _1119.y <= 1.0f.xx.y);
            float _1142 = 0.0f;
            if (all(bool2(_1120.x && _1121.x, _1120.y && _1121.y)))
            {
                float4 _1135 = TranslucentBasePass_Shared_Forward_DirectionalLightStaticShadowmap.SampleLevel(TranslucentBasePass_Shared_Forward_StaticShadowmapSampler, _1119, 0.0f);
                float _1136 = _1135.x;
                _1142 = float((_1115.z < _1136) || (_1136 > 0.9900000095367431640625f));
            }
            else
            {
                _1142 = 1.0f;
            }
            _1143 = _1142;
        }
        else
        {
            _1143 = 1.0f;
        }
        float4 _1144 = 0.0f.xxxx;
        _1144.x = _1143;
        float _1223 = 0.0f;
        if (TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumDirectionalLightCascades > 0u)
        {
            float4 _1152 = _493.xxxx;
            float4 _1154 = float4(bool4(_1152.x >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_CascadeEndDepths.x, _1152.y >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_CascadeEndDepths.y, _1152.z >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_CascadeEndDepths.z, _1152.w >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_CascadeEndDepths.w));
            uint _1162 = uint(((_1154.x + _1154.y) + _1154.z) + _1154.w);
            float _1222 = 0.0f;
            if (_1162 < TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumDirectionalLightCascades)
            {
                float4 _1172 = mul(float4(_490, 1.0f), TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightTranslatedWorldToShadowMatrix[_1162]);
                float2 _1176 = _1172.xy / _1172.w.xx;
                bool2 _1180 = bool2(_1176.x >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapMinMax[_1162].xy.x, _1176.y >= TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapMinMax[_1162].xy.y);
                bool2 _1182 = bool2(_1176.x <= TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapMinMax[_1162].zw.x, _1176.y <= TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapMinMax[_1162].zw.y);
                float _1221 = 0.0f;
                if (all(bool2(_1180.x && _1182.x, _1180.y && _1182.y)))
                {
                    float2 _1200 = mad(_1176, TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapAtlasBufferSize.xy, (-0.5f).xx);
                    float2 _1201 = frac(_1200);
                    float4 _1212 = clamp((TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapAtlas.GatherRed(TranslucentBasePass_Shared_Forward_ShadowmapSampler, (floor(_1200) + 1.0f.xx) * TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowmapAtlasBufferSize.zw) * 4000.0f) - mad(1.0f - _1172.z, 4000.0f, -1.0f).xxxx, 0.0f.xxxx, 1.0f.xxxx);
                    float2 _1216 = lerp(_1212.wx, _1212.zy, _1201.xx);
                    _1221 = lerp(_1216.x, _1216.y, _1201.y);
                }
                else
                {
                    _1221 = 1.0f;
                }
                _1222 = _1221;
            }
            else
            {
                _1222 = 1.0f;
            }
            _1223 = _1222;
        }
        else
        {
            _1223 = 1.0f;
        }
        float _1909 = 0.0f;
        [branch]
        if (true && (TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightVSM != (-1)))
        {
            float _1907 = 0.0f;
            do
            {
                float _1232 = max(0.0f, 0.0f);
                uint _1233 = uint(TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightVSM);
                uint _1234 = _1233 * 336u;
                uint _1236 = (_1234 + 96u) >> 2u;
                float4x4 _1250 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                _1250[2] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1236 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1236 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1236 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1236 + 3u) * 4 + 0)));
                uint _1252 = (_1234 + 128u) >> 2u;
                float4x4 _1266 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                _1266[0] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1252 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1252 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1252 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1252 + 3u) * 4 + 0)));
                uint _1268 = (_1234 + 144u) >> 2u;
                _1266[1] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1268 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1268 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1268 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1268 + 3u) * 4 + 0)));
                uint _1284 = (_1234 + 160u) >> 2u;
                _1266[2] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1284 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1284 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1284 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1284 + 3u) * 4 + 0)));
                uint _1300 = (_1234 + 176u) >> 2u;
                _1266[3] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1300 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1300 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1300 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1300 + 3u) * 4 + 0)));
                uint _1316 = (_1234 + 256u) >> 2u;
                float3 _1326 = asfloat(uint3(VirtualShadowMap_ProjectionData.Load(_1316 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1316 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1316 + 2u) * 4 + 0)));
                uint _1328 = (_1234 + 268u) >> 2u;
                uint _1332 = (_1234 + 272u) >> 2u;
                uint _1344 = (_1234 + 288u) >> 2u;
                if (VirtualShadowMap_ProjectionData.Load(_1328 * 4 + 0) == 0u)
                {
                    int _1579 = max(0, (int(floor(log2(length(((View_View_ViewTilePosition - (-_1326)) * 2097152.0f) + (_491 - (-asfloat(uint3(VirtualShadowMap_ProjectionData.Load(_1344 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1344 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1344 + 2u) * 4 + 0))))))) + asfloat(VirtualShadowMap_ProjectionData.Load(((_1234 + 300u) >> 2u) * 4 + 0)))) - int(VirtualShadowMap_ProjectionData.Load(((_1234 + 312u) >> 2u) * 4 + 0))));
                    if (_1579 < int(VirtualShadowMap_ProjectionData.Load(((_1234 + 316u) >> 2u) * 4 + 0)))
                    {
                        int _1583 = TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightVSM + _1579;
                        uint _1584 = uint(_1583);
                        uint _1585 = _1584 * 336u;
                        uint _1587 = (_1585 + 96u) >> 2u;
                        uint _1602 = (_1585 + 112u) >> 2u;
                        uint _1611 = (_1585 + 128u) >> 2u;
                        float4x4 _1625 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                        _1625[0] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1611 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1611 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1611 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1611 + 3u) * 4 + 0)));
                        uint _1627 = (_1585 + 144u) >> 2u;
                        _1625[1] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1627 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1627 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1627 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1627 + 3u) * 4 + 0)));
                        uint _1643 = (_1585 + 160u) >> 2u;
                        _1625[2] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1643 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1643 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1643 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1643 + 3u) * 4 + 0)));
                        uint _1659 = (_1585 + 176u) >> 2u;
                        _1625[3] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1659 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1659 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1659 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1659 + 3u) * 4 + 0)));
                        uint _1675 = (_1585 + 256u) >> 2u;
                        uint _1687 = (_1585 + 272u) >> 2u;
                        float4 _1706 = mul(float4(((View_View_ViewTilePosition + asfloat(uint3(VirtualShadowMap_ProjectionData.Load(_1675 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1675 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1675 + 2u) * 4 + 0)))) * 2097152.0f) + (_491 + asfloat(uint3(VirtualShadowMap_ProjectionData.Load(_1687 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1687 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1687 + 2u) * 4 + 0)))), 1.0f), _1625);
                        float2 _1707 = _1706.xy;
                        uint2 _1709 = uint2(_1707 * 128.0f);
                        uint _1725 = 0u;
                        do
                        {
                            if (uint(int(_1584)) < 8192u)
                            {
                                _1725 = _1584;
                                break;
                            }
                            _1725 = (8192u + ((_1584 - 8192u) * 21845u)) + (_1709.x + (_1709.y << 7u));
                            break;
                        } while(false);
                        uint _1733 = (VirtualShadowMap_PageTable[_1725] >> 20u) & 63u;
                        bool _1735 = (VirtualShadowMap_PageTable[_1725] & 134217728u) != 0u;
                        float _1898 = 0.0f;
                        bool _1899 = false;
                        if (_1735)
                        {
                            float _1874 = 0.0f;
                            float _1875 = 0.0f;
                            uint2 _1876 = uint2(0u, 0u);
                            uint2 _1877 = uint2(0u, 0u);
                            bool _1878 = false;
                            if (_1733 > 0u)
                            {
                                uint _1748 = (_1585 + 304u) >> 2u;
                                uint _1751 = _1748 + 1u;
                                uint _1756 = uint(int(_1584 + _1733));
                                uint _1759 = ((_1756 * 336u) + 304u) >> 2u;
                                int _1771 = int(_1733);
                                uint2 _1779 = ((_1709 - uint2(int2(32, 32) * int2(uint2(VirtualShadowMap_ProjectionData.Load(_1748 * 4 + 0), VirtualShadowMap_ProjectionData.Load(_1751 * 4 + 0))))) + uint2((int2(32, 32) * int2(uint2(VirtualShadowMap_ProjectionData.Load(_1759 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1759 + 1u) * 4 + 0)))) << (_1771.xx & int2(31, 31)))) >> (_1733.xx & uint2(31u, 31u));
                                uint2 _1780 = _1779 * uint2(128u, 128u);
                                uint _1794 = uint(_1583 + _1771) * 336u;
                                uint _1796 = (_1794 + 112u) >> 2u;
                                uint _1811 = (_1794 + 304u) >> 2u;
                                float _1832 = (_1771 >= 0) ? (1.0f / float(1u << (uint(_1771) & 31u))) : float(1u << (uint(-_1771) & 31u));
                                uint _1861 = 0u;
                                do
                                {
                                    if (uint(int(_1756)) < 8192u)
                                    {
                                        _1861 = _1756;
                                        break;
                                    }
                                    _1861 = (8192u + ((_1756 - 8192u) * 21845u)) + (_1779.x + (_1779.y << 7u));
                                    break;
                                } while(false);
                                _1874 = _1832;
                                _1875 = mad(-_1832, asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1602 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1602 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1602 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1602 + 3u) * 4 + 0))).z, asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1796 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1796 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1796 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1796 + 3u) * 4 + 0))).z);
                                _1876 = clamp(uint2(((_1707 * _1832) + ((float2(int2(uint2(VirtualShadowMap_ProjectionData.Load(_1811 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1811 + 1u) * 4 + 0)))) - (float2(int2(uint2(VirtualShadowMap_ProjectionData.Load(_1748 * 4 + 0), VirtualShadowMap_ProjectionData.Load(_1751 * 4 + 0)))) * _1832)) * 0.25f).xy) * 16384.0f), _1780, _1780 + uint2(127u, 127u));
                                _1877 = uint2(VirtualShadowMap_PageTable[_1861] & 1023u, (VirtualShadowMap_PageTable[_1861] >> 10u) & 1023u);
                                _1878 = ((VirtualShadowMap_PageTable[_1861] & 134217728u) != 0u) && (((VirtualShadowMap_PageTable[_1861] >> 20u) & 63u) == 0u);
                            }
                            else
                            {
                                _1874 = 1.0f;
                                _1875 = 0.0f;
                                _1876 = uint2(_1707 * 16384.0f);
                                _1877 = uint2(VirtualShadowMap_PageTable[_1725] & 1023u, (VirtualShadowMap_PageTable[_1725] >> 10u) & 1023u);
                                _1878 = _1735 && (_1733 == 0u);
                            }
                            float _1896 = 0.0f;
                            if (_1878)
                            {
                                int4 _1887 = int4(uint4((_1877 * uint2(128u, 128u)) + (_1876 & uint2(127u, 127u)), 0u, 0u));
                                _1896 = (asfloat(VirtualShadowMap_PhysicalPagePool.Load(int4(_1887.xyz, _1887.w)).x) - _1875) / _1874;
                            }
                            else
                            {
                                _1896 = 0.0f;
                            }
                            _1898 = _1896;
                            _1899 = _1878 ? true : false;
                        }
                        else
                        {
                            _1898 = 0.0f;
                            _1899 = false;
                        }
                        if (_1899)
                        {
                            _1907 = (mad(_1232, asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1587 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1587 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1587 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1587 + 3u) * 4 + 0))).z, _1898) > _1706.z) ? 0.0f : 1.0f;
                            break;
                        }
                    }
                }
                else
                {
                    float3 _1379 = ((View_View_ViewTilePosition + _1326) * 2097152.0f) + (_491 + asfloat(uint3(VirtualShadowMap_ProjectionData.Load(_1332 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1332 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1332 + 2u) * 4 + 0))));
                    float4x4 _1492 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                    int _1493 = 0;
                    float4x4 _1494 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                    if (VirtualShadowMap_ProjectionData.Load(_1328 * 4 + 0) != 2u)
                    {
                        uint _1407 = 0u;
                        do
                        {
                            float _1385 = _1379.x;
                            float _1386 = abs(_1385);
                            float _1387 = _1379.y;
                            float _1388 = abs(_1387);
                            float _1390 = _1379.z;
                            float _1391 = abs(_1390);
                            if ((_1386 >= _1388) && (_1386 >= _1391))
                            {
                                _1407 = (_1385 > 0.0f) ? 0u : 1u;
                                break;
                            }
                            else
                            {
                                if (_1388 > _1391)
                                {
                                    _1407 = (_1387 > 0.0f) ? 2u : 3u;
                                    break;
                                }
                                else
                                {
                                    _1407 = (_1390 > 0.0f) ? 4u : 5u;
                                    break;
                                }
                                break; // unreachable workaround
                            }
                            break; // unreachable workaround
                        } while(false);
                        int _1409 = int(_1233 + _1407);
                        uint _1411 = uint(_1409) * 336u;
                        uint _1413 = (_1411 + 96u) >> 2u;
                        float4x4 _1427 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                        _1427[2] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1413 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1413 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1413 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1413 + 3u) * 4 + 0)));
                        uint _1429 = (_1411 + 128u) >> 2u;
                        float4x4 _1443 = float4x4(0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx, 0.0f.xxxx);
                        _1443[0] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1429 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1429 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1429 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1429 + 3u) * 4 + 0)));
                        uint _1445 = (_1411 + 144u) >> 2u;
                        _1443[1] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1445 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1445 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1445 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1445 + 3u) * 4 + 0)));
                        uint _1461 = (_1411 + 160u) >> 2u;
                        _1443[2] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1461 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1461 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1461 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1461 + 3u) * 4 + 0)));
                        uint _1477 = (_1411 + 176u) >> 2u;
                        _1443[3] = asfloat(uint4(VirtualShadowMap_ProjectionData.Load(_1477 * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1477 + 1u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1477 + 2u) * 4 + 0), VirtualShadowMap_ProjectionData.Load((_1477 + 3u) * 4 + 0)));
                        _1492 = _1427;
                        _1493 = _1409;
                        _1494 = _1443;
                    }
                    else
                    {
                        _1492 = _1250;
                        _1493 = TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightVSM;
                        _1494 = _1266;
                    }
                    float4 _1499 = mul(float4(_1379, 1.0f), _1494);
                    float _1500 = _1499.w;
                    float3 _1503 = _1499.xyz / _1500.xxx;
                    float2 _1504 = _1503.xy;
                    float _1557 = 0.0f;
                    bool _1558 = false;
                    do
                    {
                        bool _1514 = false;
                        uint _1507 = uint(_1493);
                        uint2 _1509 = uint2(_1504 * 128.0f);
                        uint _1525 = 0u;
                        do
                        {
                            _1514 = uint(int(_1507)) < 8192u;
                            if (_1514)
                            {
                                _1525 = _1507;
                                break;
                            }
                            _1525 = (8192u + ((_1507 - 8192u) * 21845u)) + (_1509.x + (_1509.y << 7u));
                            break;
                        } while(false);
                        if ((VirtualShadowMap_PageTable[_1525] & 134217728u) != 0u)
                        {
                            int4 _1550 = int4(uint4((uint2(VirtualShadowMap_PageTable[_1525] & 1023u, (VirtualShadowMap_PageTable[_1525] >> 10u) & 1023u) * uint2(128u, 128u)) + (uint2(_1504 * float(16384u >> ((_1514 ? 7u : ((VirtualShadowMap_PageTable[_1525] >> 20u) & 63u)) & 31u))) & uint2(127u, 127u)), 0u, 0u));
                            _1557 = asfloat(VirtualShadowMap_PhysicalPagePool.Load(int4(_1550.xyz, _1550.w)).x);
                            _1558 = true;
                            break;
                        }
                        _1557 = 0.0f;
                        _1558 = false;
                        break;
                    } while(false);
                    if (_1558)
                    {
                        _1907 = ((_1557 - (((-_1232) * _1492[2].z) / _1500)) > _1503.z) ? 0.0f : 1.0f;
                        break;
                    }
                }
                _1907 = 1.0f;
                break;
            } while(false);
            _1909 = _1223 * _1907;
        }
        else
        {
            _1909 = _1223;
        }
        float _1916 = clamp(mad(_493, TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDistanceFadeMAD.x, TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDistanceFadeMAD.y), 0.0f, 1.0f);
        float _1918 = lerp(_1909, lerp(1.0f, dot(_1144, _1100), dot(_1100, 1.0f.xxxx)), _1916 * _1916);
        float3 _2118 = 0.0f.xxx;
        float3 _2119 = 0.0f.xxx;
        [branch]
        if ((_1918 + min(_1918, 1.0f)) > 0.0f)
        {
            float _1926 = max(_799, View_View_MinRoughness);
            float _1927 = dot(TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDirection, TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDirection);
            float _1930 = rsqrt(_1927);
            float3 _1931 = TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightDirection * _1930;
            float _1932 = dot(_601, _1931);
            float _1950 = 0.0f;
            if (TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius > 0.0f)
            {
                float _1939 = sqrt(clamp((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius * TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius) * (1.0f / (_1927 + 1.0f)), 0.0f, 1.0f));
                float _1949 = 0.0f;
                if (_1932 < _1939)
                {
                    float _1945 = _1939 + max(_1932, -_1939);
                    _1949 = (_1945 * _1945) / (4.0f * _1939);
                }
                else
                {
                    _1949 = _1932;
                }
                _1950 = _1949;
            }
            else
            {
                _1950 = _1932;
            }
            float _1951 = clamp(_1950, 0.0f, 1.0f);
            float _1952 = max(_1926, View_View_MinRoughness);
            float _1957 = clamp((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius * _1930) * mad(-_1952, _1952, 1.0f), 0.0f, 1.0f);
            float _1965 = dot(_601, _507);
            float _1966 = dot(_507, _1931);
            float _1968 = rsqrt(mad(2.0f, _1966, 2.0f));
            bool _1974 = _1957 > 0.0f;
            float _2053 = 0.0f;
            float _2054 = 0.0f;
            if (_1974)
            {
                float _1979 = sqrt(mad(-_1957, _1957, 1.0f));
                float _1980 = 2.0f * _1932;
                float _1981 = -_1966;
                float _1982 = mad(_1980, _1965, _1981);
                float _2051 = 0.0f;
                float _2052 = 0.0f;
                if (_1982 >= _1979)
                {
                    _2051 = 1.0f;
                    _2052 = abs(_1965);
                }
                else
                {
                    float _1987 = -_1982;
                    float _1990 = _1957 * rsqrt(mad(_1987, _1982, 1.0f));
                    float _1991 = mad(_1987, _1932, _1965);
                    float _1995 = mad(_1987, _1966, mad(2.0f * _1965, _1965, -1.0f));
                    float _2006 = _1990 * sqrt(clamp(mad(_1980 * _1965, _1966, mad(_1981, _1966, mad(-_1965, _1965, mad(-_1932, _1932, 1.0f)))), 0.0f, 1.0f));
                    float _2008 = (_2006 * 2.0f) * _1965;
                    float _2009 = mad(_1932, _1979, _1965);
                    float _2010 = mad(_1990, _1991, _2009);
                    float _2012 = mad(_1990, _1995, mad(_1966, _1979, 1.0f));
                    float _2013 = _2006 * _2012;
                    float _2014 = _2010 * _2012;
                    float _2019 = _2014 * mad(-0.5f, _2013, (0.25f * _2008) * _2010);
                    float _2029 = mad(_2010, mad(_2009, _2012 * _2012, _2014 * mad(-0.5f, mad(_1966, _1979, _2012), -0.5f)), mad(_2013, _2013, (_2008 * _2010) * mad(_2008, _2010, _2013 * (-2.0f))));
                    float _2033 = (2.0f * _2019) / mad(_2029, _2029, _2019 * _2019);
                    float _2034 = _2033 * _2029;
                    float _2036 = mad(-_2033, _2019, 1.0f);
                    float _2042 = mad(_1966, _1979, mad(_2036, _1990 * _1995, _2034 * _2008));
                    float _2044 = rsqrt(mad(2.0f, _2042, 2.0f));
                    _2051 = clamp((mad(_1932, _1979, mad(_2036, _1990 * _1991, _2034 * _2006)) + _1965) * _2044, 0.0f, 1.0f);
                    _2052 = clamp(mad(_2044, _2042, _2044), 0.0f, 1.0f);
                }
                _2053 = _2051;
                _2054 = _2052;
            }
            else
            {
                _2053 = clamp((_1932 + _1965) * _1968, 0.0f, 1.0f);
                _2054 = clamp(mad(_1968, _1966, _1968), 0.0f, 1.0f);
            }
            float _2057 = clamp(abs(_1965) + 9.9999997473787516355514526367188e-06f, 0.0f, 1.0f);
            float3 _2059 = 1.0f.xxx * _1951;
            float3 _2112 = 0.0f.xxx;
            if (((0u | (asuint(clamp(mad(-max(0.0f, TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightSourceRadius), 0.0500000007450580596923828125f, 1.0f), 0.0f, 1.0f)) << 1u)) & 1u) == 1u)
            {
                _2112 = 0.0f.xxx;
            }
            else
            {
                float _2066 = _1926 * _1926;
                float _2067 = _2066 * _2066;
                float _2081 = 0.0f;
                if (_1974)
                {
                    _2081 = _2067 / mad(_2066, _2066, ((0.25f * _1957) * mad(3.0f, asfloat(532487669 + (asint(_2067) >> 1)), _1957)) / (_2054 + 0.001000000047497451305389404296875f));
                }
                else
                {
                    _2081 = 1.0f;
                }
                float _2084 = mad(mad(_2053, _2067, -_2053), _2053, 1.0f);
                float _2089 = sqrt(_2067);
                float _2090 = 1.0f - _2089;
                float _2096 = 1.0f - _2054;
                float _2097 = _2096 * _2096;
                float _2098 = _2097 * _2097;
                _2112 = _2059 * (((clamp(50.0f * _829.y, 0.0f, 1.0f) * (_2098 * _2096)).xxx + (_829 * mad(-_2098, _2096, 1.0f))) * (((_2067 / ((3.1415927410125732421875f * _2084) * _2084)) * _2081) * (0.5f / mad(_1951, mad(_2057, _2090, _2089), _2057 * mad(_1951, _2090, _2089)))));
            }
            float3 _2115 = TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightColor * _1918;
            _2118 = mad(((_827 * 0.3183098733425140380859375f) * _2059) * 1.0f, _2115, 0.0f.xxx);
            _2119 = (_2112 * 1.0f) * _2115;
        }
        else
        {
            _2118 = 0.0f.xxx;
            _2119 = 0.0f.xxx;
        }
        float4 _2123 = float4(_2118, 0.0f);
        float4 _2127 = float4(_2119, 0.0f);
        float4 _2134 = 0.0f.xxxx;
        float4 _2135 = 0.0f.xxxx;
        [flatten]
        if ((((TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectionalLightShadowMapChannelMask >> 8u) & 7u) & _1075) != 0u)
        {
            _2134 = float4(_2123.x, _2123.y, _2123.z, _2123.w);
            _2135 = float4(_2127.x, _2127.y, _2127.z, _2127.w);
        }
        else
        {
            _2134 = 0.0f.xxxx;
            _2135 = 0.0f.xxxx;
        }
        _2136 = _1144;
        _2137 = _2134;
        _2138 = _2135;
    }
    else
    {
        _2136 = 0.0f.xxxx;
        _2137 = 0.0f.xxxx;
        _2138 = 0.0f.xxxx;
    }
    uint _2139 = _1059 * 2u;
    uint _2145 = _2139 + 1u;
    uint _2148 = min(min(TranslucentBasePass_Shared_Forward_NumCulledLightsGrid[_2139], TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumLocalLights), TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumLocalLights);
    float4 _2150 = 0.0f.xxxx;
    float4 _2153 = 0.0f.xxxx;
    _2150 = _2137;
    _2153 = _2138;
    float4 _2151 = 0.0f.xxxx;
    float4 _2154 = 0.0f.xxxx;
    [loop]
    for (uint _2155 = 0u; _2155 < _2148; _2150 = _2151, _2153 = _2154, _2155++)
    {
        uint _2164 = TranslucentBasePass_Shared_Forward_CulledLightDataGrid16Bit.Load(TranslucentBasePass_Shared_Forward_NumCulledLightsGrid[_2145] + _2155).x * 6u;
        uint _2167 = _2164 + 1u;
        uint _2170 = _2164 + 2u;
        uint _2173 = _2164 + 3u;
        uint _2176 = _2164 + 4u;
        uint _2180 = asuint(TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2170].w);
        uint _2186 = asuint(TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2167].y);
        uint _2202 = asuint(TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2173].z);
        float2 _2204 = spvUnpackHalf2x16(_2202 & 65535u);
        float _2205 = _2204.x;
        float2 _2208 = spvUnpackHalf2x16(asuint(TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2173].w));
        float _2209 = _2208.x;
        bool _2214 = TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2167].w == 0.0f;
        float4 _2235 = float4(float(_2180 & 1u), float((_2180 & 2u) >> 1u), float((_2180 & 4u) >> 2u), float((_2180 & 8u) >> 3u));
        uint _2236 = _2180 >> 4u;
        float3 _2252 = TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2164].xyz - _490;
        float _2253 = dot(_2252, _2252);
        float _2270 = 0.0f;
        if (_2214)
        {
            float _2265 = _2253 * (TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2164].w * TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2164].w);
            float _2268 = clamp(mad(-_2265, _2265, 1.0f), 0.0f, 1.0f);
            _2270 = _2268 * _2268;
        }
        else
        {
            float3 _2259 = _2252 * TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2164].w;
            _2270 = pow(1.0f - clamp(dot(_2259, _2259), 0.0f, 1.0f), TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2167].w);
        }
        float _2281 = 0.0f;
        if (((_2180 >> 16u) & 3u) == 2u)
        {
            float _2278 = clamp((dot(_2252 * rsqrt(_2253), TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2170].xyz) - TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2173].x) * TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2173].y, 0.0f, 1.0f);
            _2281 = _2270 * (_2278 * _2278);
        }
        else
        {
            _2281 = _2270;
        }
        float3 _2576 = 0.0f.xxx;
        float3 _2577 = 0.0f.xxx;
        [branch]
        if (_2281 > 0.0f)
        {
            float _2293 = 0.0f;
            [branch]
            if (uint((_2180 & 255u) != 0u) != 0u)
            {
                _2293 = dot(float4(float(_2236 & 1u), float((_2236 & 2u) >> 1u), float((_2236 & 4u) >> 2u), float((_2236 & 8u) >> 3u)), 1.0f.xxxx) * lerp(1.0f, dot(_2136, _2235), dot(_2235, 1.0f.xxxx));
            }
            else
            {
                _2293 = 1.0f;
            }
            float3 _2574 = 0.0f.xxx;
            float3 _2575 = 0.0f.xxx;
            [branch]
            if ((_2293 + _2293) > 0.0f)
            {
                float3 _2299 = TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2176].xyz * (0.5f * _2209);
                float3 _2300 = _2252 - _2299;
                float3 _2301 = _2252 + _2299;
                float _2304 = max(_799, View_View_MinRoughness);
                bool _2305 = _2209 > 0.0f;
                float _2330 = 0.0f;
                float _2331 = 0.0f;
                float _2332 = 0.0f;
                [branch]
                if (_2305)
                {
                    float _2317 = rsqrt(dot(_2300, _2300));
                    float _2318 = rsqrt(dot(_2301, _2301));
                    float _2319 = _2317 * _2318;
                    float _2321 = dot(_2300, _2301) * _2319;
                    _2330 = _2321;
                    _2331 = 0.5f * mad(dot(_601, _2300), _2317, dot(_601, _2301) * _2318);
                    _2332 = _2319 / mad(_2317, _2318, mad(_2321, 0.5f, 0.5f));
                }
                else
                {
                    float _2309 = dot(_2300, _2300);
                    _2330 = 1.0f;
                    _2331 = dot(_601, _2300 * rsqrt(_2309));
                    _2332 = 1.0f / (_2309 + 1.0f);
                }
                float _2350 = 0.0f;
                if (_2205 > 0.0f)
                {
                    float _2339 = sqrt(clamp((_2205 * _2205) * _2332, 0.0f, 1.0f));
                    float _2349 = 0.0f;
                    if (_2331 < _2339)
                    {
                        float _2345 = _2339 + max(_2331, -_2339);
                        _2349 = (_2345 * _2345) / (4.0f * _2339);
                    }
                    else
                    {
                        _2349 = _2331;
                    }
                    _2350 = _2349;
                }
                else
                {
                    _2350 = _2331;
                }
                float _2351 = clamp(_2350, 0.0f, 1.0f);
                float3 _2369 = 0.0f.xxx;
                if (_2305)
                {
                    float3 _2356 = reflect(-_507, _601);
                    float3 _2357 = _2301 - _2300;
                    float _2358 = dot(_2356, _2357);
                    _2369 = _2300 + (_2357 * clamp(dot(_2300, (_2356 * _2358) - _2357) / mad(_2209, _2209, -(_2358 * _2358)), 0.0f, 1.0f));
                }
                else
                {
                    _2369 = _2300;
                }
                float _2371 = rsqrt(dot(_2369, _2369));
                float3 _2372 = _2369 * _2371;
                float _2373 = max(_2304, View_View_MinRoughness);
                float _2378 = clamp((_2205 * _2371) * mad(-_2373, _2373, 1.0f), 0.0f, 1.0f);
                float _2380 = clamp(spvUnpackHalf2x16(_2202 >> 16u).x * _2371, 0.0f, 1.0f);
                float _2388 = dot(_601, _2372);
                float _2389 = dot(_601, _507);
                float _2390 = dot(_507, _2372);
                float _2392 = rsqrt(mad(2.0f, _2390, 2.0f));
                bool _2398 = _2378 > 0.0f;
                float _2477 = 0.0f;
                float _2478 = 0.0f;
                if (_2398)
                {
                    float _2403 = sqrt(mad(-_2378, _2378, 1.0f));
                    float _2404 = 2.0f * _2388;
                    float _2405 = -_2390;
                    float _2406 = mad(_2404, _2389, _2405);
                    float _2475 = 0.0f;
                    float _2476 = 0.0f;
                    if (_2406 >= _2403)
                    {
                        _2475 = 1.0f;
                        _2476 = abs(_2389);
                    }
                    else
                    {
                        float _2411 = -_2406;
                        float _2414 = _2378 * rsqrt(mad(_2411, _2406, 1.0f));
                        float _2415 = mad(_2411, _2388, _2389);
                        float _2419 = mad(_2411, _2390, mad(2.0f * _2389, _2389, -1.0f));
                        float _2430 = _2414 * sqrt(clamp(mad(_2404 * _2389, _2390, mad(_2405, _2390, mad(-_2389, _2389, mad(-_2388, _2388, 1.0f)))), 0.0f, 1.0f));
                        float _2432 = (_2430 * 2.0f) * _2389;
                        float _2433 = mad(_2388, _2403, _2389);
                        float _2434 = mad(_2414, _2415, _2433);
                        float _2436 = mad(_2414, _2419, mad(_2390, _2403, 1.0f));
                        float _2437 = _2430 * _2436;
                        float _2438 = _2434 * _2436;
                        float _2443 = _2438 * mad(-0.5f, _2437, (0.25f * _2432) * _2434);
                        float _2453 = mad(_2434, mad(_2433, _2436 * _2436, _2438 * mad(-0.5f, mad(_2390, _2403, _2436), -0.5f)), mad(_2437, _2437, (_2432 * _2434) * mad(_2432, _2434, _2437 * (-2.0f))));
                        float _2457 = (2.0f * _2443) / mad(_2453, _2453, _2443 * _2443);
                        float _2458 = _2457 * _2453;
                        float _2460 = mad(-_2457, _2443, 1.0f);
                        float _2466 = mad(_2390, _2403, mad(_2460, _2414 * _2419, _2458 * _2432));
                        float _2468 = rsqrt(mad(2.0f, _2466, 2.0f));
                        _2475 = clamp((mad(_2388, _2403, mad(_2460, _2414 * _2415, _2458 * _2430)) + _2389) * _2468, 0.0f, 1.0f);
                        _2476 = clamp(mad(_2468, _2466, _2468), 0.0f, 1.0f);
                    }
                    _2477 = _2475;
                    _2478 = _2476;
                }
                else
                {
                    _2477 = clamp((_2388 + _2389) * _2392, 0.0f, 1.0f);
                    _2478 = clamp(mad(_2392, _2390, _2392), 0.0f, 1.0f);
                }
                float _2481 = clamp(abs(_2389) + 9.9999997473787516355514526367188e-06f, 0.0f, 1.0f);
                float3 _2484 = 1.0f.xxx * ((_2214 ? _2332 : 1.0f) * _2351);
                float3 _2568 = 0.0f.xxx;
                if (((0u | (asuint(clamp(mad(-max(_2209, _2205), 0.0500000007450580596923828125f, 1.0f), 0.0f, 1.0f)) << 1u)) & 1u) == 1u)
                {
                    _2568 = 0.0f.xxx;
                }
                else
                {
                    float _2491 = _2304 * _2304;
                    float _2501 = 0.0f;
                    if (_2380 > 0.0f)
                    {
                        _2501 = clamp(mad(_2491, _2491, (_2380 * _2380) / mad(_2478, 3.599999904632568359375f, 0.4000000059604644775390625f)), 0.0f, 1.0f);
                    }
                    else
                    {
                        _2501 = _2491 * _2491;
                    }
                    float _2515 = 0.0f;
                    float _2516 = 0.0f;
                    if (_2398)
                    {
                        float _2513 = _2501 + (((0.25f * _2378) * mad(3.0f, asfloat(532487669 + (asint(_2501) >> 1)), _2378)) / (_2478 + 0.001000000047497451305389404296875f));
                        _2515 = _2501 / _2513;
                        _2516 = _2513;
                    }
                    else
                    {
                        _2515 = 1.0f;
                        _2516 = _2501;
                    }
                    float _2537 = 0.0f;
                    if (_2330 < 1.0f)
                    {
                        float _2523 = sqrt((1.00010001659393310546875f - _2330) / (1.0f + _2330));
                        _2537 = _2515 * sqrt(_2516 / (_2516 + (((0.25f * _2523) * mad(3.0f, asfloat(532487669 + (asint(_2516) >> 1)), _2523)) / (_2478 + 0.001000000047497451305389404296875f))));
                    }
                    else
                    {
                        _2537 = _2515;
                    }
                    float _2540 = mad(mad(_2477, _2501, -_2477), _2477, 1.0f);
                    float _2545 = sqrt(_2501);
                    float _2546 = 1.0f - _2545;
                    float _2552 = 1.0f - _2478;
                    float _2553 = _2552 * _2552;
                    float _2554 = _2553 * _2553;
                    _2568 = _2484 * (((clamp(50.0f * _829.y, 0.0f, 1.0f) * (_2554 * _2552)).xxx + (_829 * mad(-_2554, _2552, 1.0f))) * (((_2501 / ((3.1415927410125732421875f * _2540) * _2540)) * _2537) * (0.5f / mad(_2351, mad(_2481, _2546, _2545), _2481 * mad(_2351, _2546, _2545)))));
                }
                float3 _2571 = ((float3(float((_2186 >> 0u) & 1023u), float((_2186 >> 10u) & 1023u), float((_2186 >> 20u) & 1023u)) * TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2167].x) * _2281) * _2293;
                _2574 = mad(((_827 * 0.3183098733425140380859375f) * _2484) * 1.0f, _2571, 0.0f.xxx);
                _2575 = (_2568 * spvUnpackHalf2x16(asuint(TranslucentBasePass_Shared_Forward_ForwardLocalLightBuffer[_2176].w) & 65535u).x) * _2571;
            }
            else
            {
                _2574 = 0.0f.xxx;
                _2575 = 0.0f.xxx;
            }
            _2576 = _2574;
            _2577 = _2575;
        }
        else
        {
            _2576 = 0.0f.xxx;
            _2577 = 0.0f.xxx;
        }
        [flatten]
        if ((((_2180 >> 8u) & 7u) & _1075) != 0u)
        {
            _2151 = _2150 + float4(_2576, 0.0f);
            _2154 = _2153 + float4(_2577, 0.0f);
        }
        else
        {
            _2151 = _2150;
            _2154 = _2153;
        }
    }
    bool4 _2595 = (TranslucentBasePass_TranslucentBasePass_Shared_Forward_DirectLightingShowFlag == 0u).xxxx;
    float3 _2604 = (_601 * (2.0f * dot(_507, _601))) - _507;
    bool _2643 = false;
    if (TranslucentBasePass_TranslucentBasePass_Enabled > 0u)
    {
        float _2636 = 0.0f;
        do
        {
            [flatten]
            if (asuint(View_View_ViewToClip[3].w) != 0u)
            {
                _2636 = mad(_493, View_View_ViewToClip[2u].z, View_View_ViewToClip[3u].z);
                break;
            }
            else
            {
                _2636 = 1.0f / ((_493 + View_View_InvDeviceZToWorldZTransform.w) * View_View_InvDeviceZToWorldZTransform.z);
                break;
            }
            break; // unreachable workaround
        } while(false);
        _2643 = (abs(TranslucentBasePass_SceneDepth.SampleLevel(View_SharedPointClampedSampler, _498, 0.0f).x - _2636) < TranslucentBasePass_TranslucentBasePass_RelativeDepthThreshold) ? true : false;
    }
    else
    {
        _2643 = false;
    }
    bool _2647 = !_2643;
    uint _2713 = 0u;
    bool _2714 = false;
    if ((TranslucentBasePass_TranslucentBasePass_FinalProbeResolution > 0u) && _2647)
    {
        uint _2669 = 0u;
        float _2659 = frac(52.98291778564453125f * frac(dot(gl_FragCoord.xy + (float2(32.66500091552734375f, 11.81499958038330078125f) * float(View_View_StateFrameIndexMod8)), float2(0.067110560834407806396484375f, 0.005837149918079376220703125f))));
        float3 _2661 = (View_View_ViewTilePosition * 2097152.0f) + _491;
        uint _2710 = 0u;
        do
        {
            uint _2707 = 0u;
            bool _2708 = false;
            uint _2665 = 0u;
            for (;;)
            {
                _2669 = TranslucentBasePass_TranslucentBasePass_NumRadianceProbeClipmaps;
                if (_2665 < _2669)
                {
                    float3 _2679 = (_2661 * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2665].y) + TranslucentBasePass_TranslucentBasePass_PaddedWorldPositionToRadianceProbeCoordBias[_2665].xyz;
                    float3 _2684 = clamp((_2679 - 0.5f.xxx) * TranslucentBasePass_TranslucentBasePass_InvClipmapFadeSize, 0.0f.xxx, 1.0f.xxx);
                    float3 _2692 = clamp(((float(TranslucentBasePass_TranslucentBasePass_RadianceProbeClipmapResolution).xxx - 0.5f.xxx) - _2679) * TranslucentBasePass_TranslucentBasePass_InvClipmapFadeSize, 0.0f.xxx, 1.0f.xxx);
                    if (min(min(_2684.x, min(_2684.y, _2684.z)), min(_2692.x, min(_2692.y, _2692.z))) > _2659)
                    {
                        _2707 = _2665;
                        _2708 = true;
                        break;
                    }
                    _2665++;
                    continue;
                }
                else
                {
                    _2707 = _340;
                    _2708 = false;
                    break;
                }
            }
            if (_2708)
            {
                _2710 = _2707;
                break;
            }
            _2710 = _2669;
            break;
        } while(false);
        _2713 = _2710;
        _2714 = (_2710 < _2669) ? true : false;
    }
    else
    {
        _2713 = 0u;
        _2714 = false;
    }
    float3 _3930 = 0.0f.xxx;
    if (_2643)
    {
        _3930 = (pow((TranslucentBasePass_Radiance.SampleLevel(View_SharedPointClampedSampler, _498, 0.0f).xyz * View_View_OneOverPreExposure) * 5.5555553436279296875f, TranslucentBasePass_TranslucentBasePass_Contrast.xxx) * 0.180000007152557373046875f) * TranslucentBasePass_TranslucentBasePass_SpecularScale;
    }
    else
    {
        float3 _3912 = 0.0f.xxx;
        if (_2714)
        {
            float3 _3004 = (View_View_ViewTilePosition * 2097152.0f) + _491;
            float3 _3012 = ((_3004 * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2713].y) + TranslucentBasePass_TranslucentBasePass_PaddedWorldPositionToRadianceProbeCoordBias[_2713].xyz) - 0.5f.xxx;
            int3 _3014 = int3(floor(_3012));
            float3 _3015 = frac(_3012);
            uint3 _3016 = uint3(_3014);
            uint _3022 = _2713 * TranslucentBasePass_TranslucentBasePass_RadianceProbeClipmapResolution;
            int4 _3027 = int4(uint4(_3016.x + _3022, _3016.yz, 0u));
            uint4 _3031 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3027.xyz, _3027.w));
            uint _3032 = _3031.x;
            float3 _3044 = ((float3(_3016) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2713].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2713].xyz) + TranslucentBasePass_ProbeWorldOffset[_3032].xyz;
            float _3047 = TranslucentBasePass_TranslucentBasePass_ReprojectionRadiusScale * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2713].x;
            float3 _3053 = _3004 - float4(_3044, _3047).xyz;
            float _3055 = dot(_2604, _2604);
            float _3056 = dot(_2604, _3053);
            float _3057 = 2.0f * _3056;
            float _3058 = -_3047;
            float _3060 = 4.0f * _3055;
            float _3063 = mad(_3057, _3057, -(_3060 * mad(_3058, _3047, dot(_3053, _3053))));
            float2 _3075 = 0.0f.xx;
            [flatten]
            if (_3063 >= 0.0f)
            {
                _3075 = ((_3056 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3063))) / (2.0f * _3055).xx;
            }
            else
            {
                _3075 = (-1.0f).xx;
            }
            float3 _3079 = (_3004 + (_2604 * _3075.y)) - _3044;
            float3 _3084 = normalize(_3079);
            float3 _3085 = abs(_3084);
            float _3088 = sqrt(1.0f - _3085.z);
            float _3089 = _3085.x;
            float _3090 = _3085.y;
            float _3094 = min(_3089, _3090) / (max(_3089, _3090) + 5.4210108624275221700372640043497e-20f);
            float _3100 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3094, 0.0419038832187652587890625f), _3094, 0.08817707002162933349609375f), _3094, -0.2473337352275848388671875f), _3094, 0.006157201714813709259033203125f), _3094, 0.63622653484344482421875f), _3094, 4.0675854506844189018011093139648e-06f);
            float _3103 = (_3089 < _3090) ? (1.0f - _3100) : _3100;
            float2 _3107 = float2(mad(-_3103, _3088, _3088), _3103 * _3088);
            bool2 _3110 = (_3084.z < 0.0f).xx;
            float2 _3112 = 1.0f.xx - _3107.yx;
            uint2 _3122 = TranslucentBasePass_TranslucentBasePass_FinalProbeResolution.xx;
            uint _3128 = TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionDivideShift & 31u;
            float _3134 = float(TranslucentBasePass_TranslucentBasePass_RadianceProbeResolution);
            float2 _3141 = float(1u << (TranslucentBasePass_TranslucentBasePass_FinalRadianceAtlasMaxMip & 31u)).xx;
            bool3 _3149 = (_3032 == 4294967295u).xxx;
            float3 _3154 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3122 * uint2(_3032 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3032 >> _3128)) + ((((asfloat(asuint(float2(_3110.x ? _3112.x : _3107.x, _3110.y ? _3112.y : _3107.y)) ^ (asuint(_3084.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3134) + _3141)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3158 = uint3(_3014 + int3(0, 0, 1));
            int4 _3164 = int4(uint4(_3158.x + _3022, _3158.yz, 0u));
            uint4 _3167 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3164.xyz, _3164.w));
            uint _3168 = _3167.x;
            float3 _3175 = ((float3(_3158) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2713].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2713].xyz) + TranslucentBasePass_ProbeWorldOffset[_3168].xyz;
            float3 _3181 = _3004 - float4(_3175, _3047).xyz;
            float _3183 = dot(_2604, _3181);
            float _3184 = 2.0f * _3183;
            float _3188 = mad(_3184, _3184, -(_3060 * mad(_3058, _3047, dot(_3181, _3181))));
            float2 _3200 = 0.0f.xx;
            [flatten]
            if (_3188 >= 0.0f)
            {
                _3200 = ((_3183 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3188))) / (2.0f * _3055).xx;
            }
            else
            {
                _3200 = (-1.0f).xx;
            }
            float3 _3204 = (_3004 + (_2604 * _3200.y)) - _3175;
            float3 _3209 = normalize(_3204);
            float3 _3210 = abs(_3209);
            float _3213 = sqrt(1.0f - _3210.z);
            float _3214 = _3210.x;
            float _3215 = _3210.y;
            float _3219 = min(_3214, _3215) / (max(_3214, _3215) + 5.4210108624275221700372640043497e-20f);
            float _3225 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3219, 0.0419038832187652587890625f), _3219, 0.08817707002162933349609375f), _3219, -0.2473337352275848388671875f), _3219, 0.006157201714813709259033203125f), _3219, 0.63622653484344482421875f), _3219, 4.0675854506844189018011093139648e-06f);
            float _3228 = (_3214 < _3215) ? (1.0f - _3225) : _3225;
            float2 _3232 = float2(mad(-_3228, _3213, _3213), _3228 * _3213);
            bool2 _3235 = (_3209.z < 0.0f).xx;
            float2 _3237 = 1.0f.xx - _3232.yx;
            bool3 _3257 = (_3168 == 4294967295u).xxx;
            float3 _3260 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3122 * uint2(_3168 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3168 >> _3128)) + ((((asfloat(asuint(float2(_3235.x ? _3237.x : _3232.x, _3235.y ? _3237.y : _3232.y)) ^ (asuint(_3209.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3134) + _3141)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3264 = uint3(_3014 + int3(0, 1, 0));
            int4 _3270 = int4(uint4(_3264.x + _3022, _3264.yz, 0u));
            uint4 _3273 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3270.xyz, _3270.w));
            uint _3274 = _3273.x;
            float3 _3281 = ((float3(_3264) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2713].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2713].xyz) + TranslucentBasePass_ProbeWorldOffset[_3274].xyz;
            float3 _3287 = _3004 - float4(_3281, _3047).xyz;
            float _3289 = dot(_2604, _3287);
            float _3290 = 2.0f * _3289;
            float _3294 = mad(_3290, _3290, -(_3060 * mad(_3058, _3047, dot(_3287, _3287))));
            float2 _3306 = 0.0f.xx;
            [flatten]
            if (_3294 >= 0.0f)
            {
                _3306 = ((_3289 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3294))) / (2.0f * _3055).xx;
            }
            else
            {
                _3306 = (-1.0f).xx;
            }
            float3 _3310 = (_3004 + (_2604 * _3306.y)) - _3281;
            float3 _3315 = normalize(_3310);
            float3 _3316 = abs(_3315);
            float _3319 = sqrt(1.0f - _3316.z);
            float _3320 = _3316.x;
            float _3321 = _3316.y;
            float _3325 = min(_3320, _3321) / (max(_3320, _3321) + 5.4210108624275221700372640043497e-20f);
            float _3331 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3325, 0.0419038832187652587890625f), _3325, 0.08817707002162933349609375f), _3325, -0.2473337352275848388671875f), _3325, 0.006157201714813709259033203125f), _3325, 0.63622653484344482421875f), _3325, 4.0675854506844189018011093139648e-06f);
            float _3334 = (_3320 < _3321) ? (1.0f - _3331) : _3331;
            float2 _3338 = float2(mad(-_3334, _3319, _3319), _3334 * _3319);
            bool2 _3341 = (_3315.z < 0.0f).xx;
            float2 _3343 = 1.0f.xx - _3338.yx;
            bool3 _3363 = (_3274 == 4294967295u).xxx;
            float3 _3366 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3122 * uint2(_3274 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3274 >> _3128)) + ((((asfloat(asuint(float2(_3341.x ? _3343.x : _3338.x, _3341.y ? _3343.y : _3338.y)) ^ (asuint(_3315.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3134) + _3141)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3370 = uint3(_3014 + int3(0, 1, 1));
            int4 _3376 = int4(uint4(_3370.x + _3022, _3370.yz, 0u));
            uint4 _3379 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3376.xyz, _3376.w));
            uint _3380 = _3379.x;
            float3 _3387 = ((float3(_3370) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2713].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2713].xyz) + TranslucentBasePass_ProbeWorldOffset[_3380].xyz;
            float3 _3393 = _3004 - float4(_3387, _3047).xyz;
            float _3395 = dot(_2604, _3393);
            float _3396 = 2.0f * _3395;
            float _3400 = mad(_3396, _3396, -(_3060 * mad(_3058, _3047, dot(_3393, _3393))));
            float2 _3412 = 0.0f.xx;
            [flatten]
            if (_3400 >= 0.0f)
            {
                _3412 = ((_3395 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3400))) / (2.0f * _3055).xx;
            }
            else
            {
                _3412 = (-1.0f).xx;
            }
            float3 _3416 = (_3004 + (_2604 * _3412.y)) - _3387;
            float3 _3421 = normalize(_3416);
            float3 _3422 = abs(_3421);
            float _3425 = sqrt(1.0f - _3422.z);
            float _3426 = _3422.x;
            float _3427 = _3422.y;
            float _3431 = min(_3426, _3427) / (max(_3426, _3427) + 5.4210108624275221700372640043497e-20f);
            float _3437 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3431, 0.0419038832187652587890625f), _3431, 0.08817707002162933349609375f), _3431, -0.2473337352275848388671875f), _3431, 0.006157201714813709259033203125f), _3431, 0.63622653484344482421875f), _3431, 4.0675854506844189018011093139648e-06f);
            float _3440 = (_3426 < _3427) ? (1.0f - _3437) : _3437;
            float2 _3444 = float2(mad(-_3440, _3425, _3425), _3440 * _3425);
            bool2 _3447 = (_3421.z < 0.0f).xx;
            float2 _3449 = 1.0f.xx - _3444.yx;
            bool3 _3469 = (_3380 == 4294967295u).xxx;
            float3 _3472 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3122 * uint2(_3380 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3380 >> _3128)) + ((((asfloat(asuint(float2(_3447.x ? _3449.x : _3444.x, _3447.y ? _3449.y : _3444.y)) ^ (asuint(_3421.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3134) + _3141)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3476 = uint3(_3014 + int3(1, 0, 0));
            int4 _3482 = int4(uint4(_3476.x + _3022, _3476.yz, 0u));
            uint4 _3485 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3482.xyz, _3482.w));
            uint _3486 = _3485.x;
            float3 _3493 = ((float3(_3476) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2713].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2713].xyz) + TranslucentBasePass_ProbeWorldOffset[_3486].xyz;
            float3 _3499 = _3004 - float4(_3493, _3047).xyz;
            float _3501 = dot(_2604, _3499);
            float _3502 = 2.0f * _3501;
            float _3506 = mad(_3502, _3502, -(_3060 * mad(_3058, _3047, dot(_3499, _3499))));
            float2 _3518 = 0.0f.xx;
            [flatten]
            if (_3506 >= 0.0f)
            {
                _3518 = ((_3501 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3506))) / (2.0f * _3055).xx;
            }
            else
            {
                _3518 = (-1.0f).xx;
            }
            float3 _3522 = (_3004 + (_2604 * _3518.y)) - _3493;
            float3 _3527 = normalize(_3522);
            float3 _3528 = abs(_3527);
            float _3531 = sqrt(1.0f - _3528.z);
            float _3532 = _3528.x;
            float _3533 = _3528.y;
            float _3537 = min(_3532, _3533) / (max(_3532, _3533) + 5.4210108624275221700372640043497e-20f);
            float _3543 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3537, 0.0419038832187652587890625f), _3537, 0.08817707002162933349609375f), _3537, -0.2473337352275848388671875f), _3537, 0.006157201714813709259033203125f), _3537, 0.63622653484344482421875f), _3537, 4.0675854506844189018011093139648e-06f);
            float _3546 = (_3532 < _3533) ? (1.0f - _3543) : _3543;
            float2 _3550 = float2(mad(-_3546, _3531, _3531), _3546 * _3531);
            bool2 _3553 = (_3527.z < 0.0f).xx;
            float2 _3555 = 1.0f.xx - _3550.yx;
            bool3 _3575 = (_3486 == 4294967295u).xxx;
            float3 _3578 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3122 * uint2(_3486 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3486 >> _3128)) + ((((asfloat(asuint(float2(_3553.x ? _3555.x : _3550.x, _3553.y ? _3555.y : _3550.y)) ^ (asuint(_3527.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3134) + _3141)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3582 = uint3(_3014 + int3(1, 0, 1));
            int4 _3588 = int4(uint4(_3582.x + _3022, _3582.yz, 0u));
            uint4 _3591 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3588.xyz, _3588.w));
            uint _3592 = _3591.x;
            float3 _3599 = ((float3(_3582) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2713].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2713].xyz) + TranslucentBasePass_ProbeWorldOffset[_3592].xyz;
            float3 _3605 = _3004 - float4(_3599, _3047).xyz;
            float _3607 = dot(_2604, _3605);
            float _3608 = 2.0f * _3607;
            float _3612 = mad(_3608, _3608, -(_3060 * mad(_3058, _3047, dot(_3605, _3605))));
            float2 _3624 = 0.0f.xx;
            [flatten]
            if (_3612 >= 0.0f)
            {
                _3624 = ((_3607 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3612))) / (2.0f * _3055).xx;
            }
            else
            {
                _3624 = (-1.0f).xx;
            }
            float3 _3628 = (_3004 + (_2604 * _3624.y)) - _3599;
            float3 _3633 = normalize(_3628);
            float3 _3634 = abs(_3633);
            float _3637 = sqrt(1.0f - _3634.z);
            float _3638 = _3634.x;
            float _3639 = _3634.y;
            float _3643 = min(_3638, _3639) / (max(_3638, _3639) + 5.4210108624275221700372640043497e-20f);
            float _3649 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3643, 0.0419038832187652587890625f), _3643, 0.08817707002162933349609375f), _3643, -0.2473337352275848388671875f), _3643, 0.006157201714813709259033203125f), _3643, 0.63622653484344482421875f), _3643, 4.0675854506844189018011093139648e-06f);
            float _3652 = (_3638 < _3639) ? (1.0f - _3649) : _3649;
            float2 _3656 = float2(mad(-_3652, _3637, _3637), _3652 * _3637);
            bool2 _3659 = (_3633.z < 0.0f).xx;
            float2 _3661 = 1.0f.xx - _3656.yx;
            bool3 _3681 = (_3592 == 4294967295u).xxx;
            float3 _3684 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3122 * uint2(_3592 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3592 >> _3128)) + ((((asfloat(asuint(float2(_3659.x ? _3661.x : _3656.x, _3659.y ? _3661.y : _3656.y)) ^ (asuint(_3633.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3134) + _3141)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3688 = uint3(_3014 + int3(1, 1, 0));
            int4 _3694 = int4(uint4(_3688.x + _3022, _3688.yz, 0u));
            uint4 _3697 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3694.xyz, _3694.w));
            uint _3698 = _3697.x;
            float3 _3705 = ((float3(_3688) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2713].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2713].xyz) + TranslucentBasePass_ProbeWorldOffset[_3698].xyz;
            float3 _3711 = _3004 - float4(_3705, _3047).xyz;
            float _3713 = dot(_2604, _3711);
            float _3714 = 2.0f * _3713;
            float _3718 = mad(_3714, _3714, -(_3060 * mad(_3058, _3047, dot(_3711, _3711))));
            float2 _3730 = 0.0f.xx;
            [flatten]
            if (_3718 >= 0.0f)
            {
                _3730 = ((_3713 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3718))) / (2.0f * _3055).xx;
            }
            else
            {
                _3730 = (-1.0f).xx;
            }
            float3 _3734 = (_3004 + (_2604 * _3730.y)) - _3705;
            float3 _3739 = normalize(_3734);
            float3 _3740 = abs(_3739);
            float _3743 = sqrt(1.0f - _3740.z);
            float _3744 = _3740.x;
            float _3745 = _3740.y;
            float _3749 = min(_3744, _3745) / (max(_3744, _3745) + 5.4210108624275221700372640043497e-20f);
            float _3755 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3749, 0.0419038832187652587890625f), _3749, 0.08817707002162933349609375f), _3749, -0.2473337352275848388671875f), _3749, 0.006157201714813709259033203125f), _3749, 0.63622653484344482421875f), _3749, 4.0675854506844189018011093139648e-06f);
            float _3758 = (_3744 < _3745) ? (1.0f - _3755) : _3755;
            float2 _3762 = float2(mad(-_3758, _3743, _3743), _3758 * _3743);
            bool2 _3765 = (_3739.z < 0.0f).xx;
            float2 _3767 = 1.0f.xx - _3762.yx;
            bool3 _3787 = (_3698 == 4294967295u).xxx;
            float3 _3790 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3122 * uint2(_3698 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3698 >> _3128)) + ((((asfloat(asuint(float2(_3765.x ? _3767.x : _3762.x, _3765.y ? _3767.y : _3762.y)) ^ (asuint(_3739.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3134) + _3141)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            uint3 _3794 = uint3(_3014 + int3(1, 1, 1));
            int4 _3800 = int4(uint4(_3794.x + _3022, _3794.yz, 0u));
            uint4 _3803 = TranslucentBasePass_RadianceProbeIndirectionTexture.Load(int4(_3800.xyz, _3800.w));
            uint _3804 = _3803.x;
            float3 _3811 = ((float3(_3794) * TranslucentBasePass_TranslucentBasePass_RadianceProbeSettings[_2713].z) + TranslucentBasePass_TranslucentBasePass_PaddedRadianceProbeCoordToWorldPositionBias[_2713].xyz) + TranslucentBasePass_ProbeWorldOffset[_3804].xyz;
            float3 _3817 = _3004 - float4(_3811, _3047).xyz;
            float _3819 = dot(_2604, _3817);
            float _3820 = 2.0f * _3819;
            float _3824 = mad(_3820, _3820, -(_3060 * mad(_3058, _3047, dot(_3817, _3817))));
            float2 _3836 = 0.0f.xx;
            [flatten]
            if (_3824 >= 0.0f)
            {
                _3836 = ((_3819 * (-2.0f)).xx + (float2(-1.0f, 1.0f) * sqrt(_3824))) / (2.0f * _3055).xx;
            }
            else
            {
                _3836 = (-1.0f).xx;
            }
            float3 _3840 = (_3004 + (_2604 * _3836.y)) - _3811;
            float3 _3845 = normalize(_3840);
            float3 _3846 = abs(_3845);
            float _3849 = sqrt(1.0f - _3846.z);
            float _3850 = _3846.x;
            float _3851 = _3846.y;
            float _3855 = min(_3850, _3851) / (max(_3850, _3851) + 5.4210108624275221700372640043497e-20f);
            float _3861 = mad(mad(mad(mad(mad(mad(-0.0251390971243381500244140625f, _3855, 0.0419038832187652587890625f), _3855, 0.08817707002162933349609375f), _3855, -0.2473337352275848388671875f), _3855, 0.006157201714813709259033203125f), _3855, 0.63622653484344482421875f), _3855, 4.0675854506844189018011093139648e-06f);
            float _3864 = (_3850 < _3851) ? (1.0f - _3861) : _3861;
            float2 _3868 = float2(mad(-_3864, _3849, _3849), _3864 * _3849);
            bool2 _3871 = (_3845.z < 0.0f).xx;
            float2 _3873 = 1.0f.xx - _3868.yx;
            bool3 _3893 = (_3804 == 4294967295u).xxx;
            float3 _3896 = TranslucentBasePass_RadianceCacheFinalRadianceAtlas.SampleLevel(View_SharedBilinearClampedSampler, (float2(_3122 * uint2(_3804 & TranslucentBasePass_TranslucentBasePass_ProbeAtlasResolutionModuloMask, _3804 >> _3128)) + ((((asfloat(asuint(float2(_3871.x ? _3873.x : _3868.x, _3871.y ? _3873.y : _3868.y)) ^ (asuint(_3845.xy) & uint2(2147483648u, 2147483648u))) * 0.5f) + 0.5f.xx) * _3134) + _3141)) * TranslucentBasePass_TranslucentBasePass_InvProbeFinalRadianceAtlasResolution, 0.0f).xyz;
            float3 _3900 = _3015.z.xxx;
            float3 _3906 = _3015.y.xxx;
            _3912 = lerp(lerp(lerp(float3(_3149.x ? 0.0f.xxx.x : _3154.x, _3149.y ? 0.0f.xxx.y : _3154.y, _3149.z ? 0.0f.xxx.z : _3154.z) * ((_3075.y * _3075.y) / (_3047 * dot(_3079, _2604))), float3(_3257.x ? 0.0f.xxx.x : _3260.x, _3257.y ? 0.0f.xxx.y : _3260.y, _3257.z ? 0.0f.xxx.z : _3260.z) * ((_3200.y * _3200.y) / (_3047 * dot(_3204, _2604))), _3900), lerp(float3(_3363.x ? 0.0f.xxx.x : _3366.x, _3363.y ? 0.0f.xxx.y : _3366.y, _3363.z ? 0.0f.xxx.z : _3366.z) * ((_3306.y * _3306.y) / (_3047 * dot(_3310, _2604))), float3(_3469.x ? 0.0f.xxx.x : _3472.x, _3469.y ? 0.0f.xxx.y : _3472.y, _3469.z ? 0.0f.xxx.z : _3472.z) * ((_3412.y * _3412.y) / (_3047 * dot(_3416, _2604))), _3900), _3906), lerp(lerp(float3(_3575.x ? 0.0f.xxx.x : _3578.x, _3575.y ? 0.0f.xxx.y : _3578.y, _3575.z ? 0.0f.xxx.z : _3578.z) * ((_3518.y * _3518.y) / (_3047 * dot(_3522, _2604))), float3(_3681.x ? 0.0f.xxx.x : _3684.x, _3681.y ? 0.0f.xxx.y : _3684.y, _3681.z ? 0.0f.xxx.z : _3684.z) * ((_3624.y * _3624.y) / (_3047 * dot(_3628, _2604))), _3900), lerp(float3(_3787.x ? 0.0f.xxx.x : _3790.x, _3787.y ? 0.0f.xxx.y : _3790.y, _3787.z ? 0.0f.xxx.z : _3790.z) * ((_3730.y * _3730.y) / (_3047 * dot(_3734, _2604))), float3(_3893.x ? 0.0f.xxx.x : _3896.x, _3893.y ? 0.0f.xxx.y : _3896.y, _3893.z ? 0.0f.xxx.z : _3896.z) * ((_3836.y * _3836.y) / (_3047 * dot(_3840, _2604))), _3900), _3906), _3015.x.xxx);
        }
        else
        {
            uint _2724 = (TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumGridCells + _1059) * 2u;
            uint _2729 = min(TranslucentBasePass_Shared_Forward_NumCulledLightsGrid[_2724], TranslucentBasePass_TranslucentBasePass_Shared_Forward_NumReflectionCaptures);
            uint _2730 = _2724 + 1u;
            float _2737 = mad(-1.2000000476837158203125f, log2(max(_799, 0.001000000047497451305389404296875f)), 1.0f);
            float _2739 = (View_View_ReflectionCubemapMaxMip - 1.0f) - _2737;
            float2 _2741 = 0.0f.xx;
            float4 _2744 = 0.0f.xxxx;
            _2741 = float2(0.0f, 1.0f);
            _2744 = float4(0.0f, 0.0f, 0.0f, 1.0f);
            float2 _2742 = 0.0f.xx;
            float4 _2745 = 0.0f.xxxx;
            [loop]
            for (uint _2746 = 0u; _2746 < _2729; _2741 = _2742, _2744 = _2745, _2746++)
            {
                [branch]
                if (_2744.w < 0.001000000047497451305389404296875f)
                {
                    break;
                }
                uint4 _2757 = TranslucentBasePass_Shared_Forward_CulledLightDataGrid16Bit.Load(TranslucentBasePass_Shared_Forward_NumCulledLightsGrid[_2730] + _2746);
                uint _2758 = _2757.x;
                float3 _2768 = ((ReflectionCaptureSM5_ReflectionCaptureSM5_TilePosition[_2758].xyz + _462) * 2097152.0f) + (ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2758].xyz + View_View_RelativePreViewTranslation);
                float3 _2773 = _490 - _2768;
                float _2775 = sqrt(dot(_2773, _2773));
                [branch]
                if (_2775 < ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2758].w)
                {
                    float _2894 = 0.0f;
                    float3 _2895 = 0.0f.xxx;
                    [branch]
                    if (ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureProperties[_2758].z > 0.0f)
                    {
                        float3 _2826 = float4(_2768, ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2758].w).xyz;
                        float3 _2827 = _490 - _2826;
                        float3 _2833 = mul(float4(_2827, 1.0f), ReflectionCaptureSM5_ReflectionCaptureSM5_BoxTransform[_2758]).xyz;
                        float3 _2839 = mul(float4(_2604, 0.0f), ReflectionCaptureSM5_ReflectionCaptureSM5_BoxTransform[_2758]).xyz;
                        float3 _2840 = 1.0f.xxx / _2839;
                        float3 _2842 = -_2833;
                        float3 _2845 = max(mad(_2842, _2840, (-1.0f).xxx / _2839), mad(_2842, _2840, _2840));
                        float3 _2859 = ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2758].xyz - (0.5f * ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2758].w).xxx;
                        float3 _2860 = -_2859;
                        float3 _2861 = _2833 * ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2758].xyz;
                        bool3 _2862 = bool3(_2861.x < _2860.x, _2861.y < _2860.y, _2861.z < _2860.z);
                        float3 _2864 = abs(mad(_2833, ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2758].xyz, _2859));
                        bool3 _2875 = bool3(_2861.x > _2859.x, _2861.y > _2859.y, _2861.z > _2859.z);
                        float3 _2877 = abs(mad(_2833, ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2758].xyz, _2860));
                        _2894 = 1.0f - smoothstep(0.0f, 0.699999988079071044921875f * ReflectionCaptureSM5_ReflectionCaptureSM5_BoxScales[_2758].w, dot(float3(_2862.x ? _2864.x : 0.0f, _2862.y ? _2864.y : 0.0f, _2862.z ? _2864.z : 0.0f), 1.0f.xxx) + dot(float3(_2875.x ? _2877.x : 0.0f, _2875.y ? _2877.y : 0.0f, _2875.z ? _2877.z : 0.0f), 1.0f.xxx));
                        _2895 = (_490 + (_2604 * min(_2845.x, min(_2845.y, _2845.z)))) - (_2826 + ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureOffsetAndAverageBrightness[_2758].xyz);
                    }
                    else
                    {
                        float3 _2794 = _490 - float4(_2768, ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2758].w).xyz;
                        float _2796 = dot(_2604, _2794);
                        float _2800 = mad(_2796, _2796, -mad(-ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2758].w, ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2758].w, dot(_2794, _2794)));
                        float _2815 = 0.0f;
                        float3 _2816 = 0.0f.xxx;
                        [flatten]
                        if (_2800 >= 0.0f)
                        {
                            float _2810 = clamp(mad(2.5f, clamp(_2775 / ReflectionCaptureSM5_ReflectionCaptureSM5_PositionAndRadius[_2758].w, 0.0f, 1.0f), -1.5f), 0.0f, 1.0f);
                            _2815 = mad(-(_2810 * _2810), mad(-2.0f, _2810, 3.0f), 1.0f);
                            _2816 = (_2794 + (_2604 * (sqrt(_2800) - _2796))) - ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureOffsetAndAverageBrightness[_2758].xyz;
                        }
                        else
                        {
                            _2815 = 0.0f;
                            _2816 = _2604;
                        }
                        _2894 = _2815;
                        _2895 = _2816;
                    }
                    float4 _2904 = TranslucentBasePass_Shared_Reflection_ReflectionCubemap.SampleLevel(TranslucentBasePass_Shared_Reflection_ReflectionCubemapSampler, float4(_2895, ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureProperties[_2758].y), _2739);
                    float3 _2907 = _2904.xyz * ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureProperties[_2758].x;
                    float4 _2909 = float4(_2907.x, _2907.y, _2907.z, _2904.w) * _2894;
                    float3 _2914 = _2744.xyz + ((_2909.xyz * _2744.w) * 1.0f);
                    float4 _2915 = float4(_2914.x, _2914.y, _2914.z, _2744.w);
                    _2915.w = _2744.w * (1.0f - _2909.w);
                    float2 _2925 = 0.0f.xx;
                    _2925.x = mad(ReflectionCaptureSM5_ReflectionCaptureSM5_CaptureOffsetAndAverageBrightness[_2758].w * _2894, _2741.y, _2741.x);
                    _2925.y = _2741.y * (1.0f - _2894);
                    _2742 = _2925;
                    _2745 = _2915;
                }
                else
                {
                    _2742 = _2741;
                    _2745 = _2744;
                }
            }
            float3 _2932 = _2744.xyz * View_View_PrecomputedIndirectSpecularColorScale;
            float4 _2933 = float4(_2932.x, _2932.y, _2932.z, _2744.w);
            float _2936 = _2741.x * dot(View_View_PrecomputedIndirectSpecularColorScale, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f));
            float2 _2937 = 0.0f.xx;
            _2937.x = _2936;
            float4 _2979 = 0.0f.xxxx;
            float2 _2980 = 0.0f.xx;
            float3 _2981 = 0.0f.xxx;
            [branch]
            if ((TranslucentBasePass_TranslucentBasePass_Shared_Reflection_SkyLightParameters.y > 0.0f) && true)
            {
                float3 _2958 = TranslucentBasePass_Shared_Reflection_SkyLightCubemap.SampleLevel(TranslucentBasePass_Shared_Reflection_SkyLightCubemapSampler, _2604, (TranslucentBasePass_TranslucentBasePass_Shared_Reflection_SkyLightParameters.x - 1.0f) - _2737).xyz * View_View_SkyLightColor.xyz;
                float4 _2976 = 0.0f.xxxx;
                float2 _2977 = 0.0f.xx;
                float3 _2978 = 0.0f.xxx;
                [flatten]
                if ((TranslucentBasePass_TranslucentBasePass_Shared_Reflection_SkyLightParameters.z < 1.0f) && true)
                {
                    float3 _2971 = _2932.xyz + ((_2958 * _2744.w) * 1.0f);
                    float2 _2975 = 0.0f.xx;
                    _2975.x = mad(View_SkyIrradianceEnvironmentMap[7u].x * dot(View_View_SkyLightColor.xyz, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f)), _2741.y, _2936);
                    _2976 = float4(_2971.x, _2971.y, _2971.z, _2744.w);
                    _2977 = _2975;
                    _2978 = 0.0f.xxx;
                }
                else
                {
                    _2976 = _2933;
                    _2977 = _2937;
                    _2978 = _2958 * 1.0f;
                }
                _2979 = _2976;
                _2980 = _2977;
                _2981 = _2978;
            }
            else
            {
                _2979 = _2933;
                _2980 = _2937;
                _2981 = 0.0f.xxx;
            }
            _3912 = ((_2979.xyz * lerp(1.0f, min(dot(_1009, float3(0.300000011920928955078125f, 0.589999973773956298828125f, 0.10999999940395355224609375f)) / max(_2980.x, 9.9999997473787516355514526367188e-05f), View_View_ReflectionEnvironmentRoughnessMixingScaleBiasAndLargestWeight.z), smoothstep(0.0f, 1.0f, clamp(mad(_799, View_View_ReflectionEnvironmentRoughnessMixingScaleBiasAndLargestWeight.x, View_View_ReflectionEnvironmentRoughnessMixingScaleBiasAndLargestWeight.y), 0.0f, 1.0f)))).xyz + (_2981 * _2979.w)).xyz;
        }
        _3930 = _3912;
    }
    float3 _4237 = 0.0f.xxx;
    if (((View_View_CameraCut == 0.0f) && (TranslucentBasePass_TranslucentBasePass_SSRQuality > 0)) && _2647)
    {
        float _3955 = min(_493, 1000000.0f);
        float4 _3960 = mul(float4(_2604, 0.0f), View_View_TranslatedWorldToView);
        float _3961 = _3960.z;
        float _3966 = (_3961 < 0.0f) ? min(((-0.949999988079071044921875f) * _493) / _3961, _3955) : _3955;
        float4 _3975 = mul(float4(_490, 1.0f), View_View_TranslatedWorldToClip);
        float4 _3980 = mul(float4(_490 + (_2604 * _3966), 1.0f), View_View_TranslatedWorldToClip);
        float3 _3984 = _3975.xyz * (1.0f / _3975.w);
        float4 _3991 = _3975 + mul(float4(0.0f, 0.0f, _3966, 0.0f), View_View_ViewToClip);
        float3 _3995 = _3991.xyz * (1.0f / _3991.w);
        float3 _3996 = (_3980.xyz * (1.0f / _3980.w)) - _3984;
        float2 _3997 = _3984.xy;
        float2 _3998 = _3996.xy;
        float _4000 = 0.5f * length(_3998);
        float2 _4009 = 1.0f.xx - (max(abs(_3998 + (_3997 * _4000)) - _4000.xx, 0.0f.xx) / abs(_3998));
        float3 _4014 = _3996 * (min(_4009.x, _4009.y) / _4000);
        float _4032 = 0.0f;
        if (asuint(View_View_ViewToClip[3].w) != 0u)
        {
            _4032 = max(0.0f, (_3984.z - _3995.z) * 4.0f);
        }
        else
        {
            _4032 = max(abs(_4014.z), (_3984.z - _3995.z) * 4.0f);
        }
        float _4047 = _4032 * 0.083333335816860198974609375f;
        float3 _4048 = float3((_4014.xy * float2(0.5f, -0.5f)) * TranslucentBasePass_TranslucentBasePass_HZBUvFactorAndInvFactor.xy, _4014.z) * 0.083333335816860198974609375f;
        float3 _4050 = float3(mad(_3997, float2(0.5f, -0.5f), 0.5f.xx) * TranslucentBasePass_TranslucentBasePass_HZBUvFactorAndInvFactor.xy, _3984.z) + (_4048 * (frac(52.98291778564453125f * frac(dot(gl_FragCoord.xy + (float2(32.66500091552734375f, 11.81499958038330078125f) * float(View_View_StateFrameIndexMod8)), float2(0.067110560834407806396484375f, 0.005837149918079376220703125f)))) - 0.5f));
        bool4 _4052 = bool4(false, false, false, false);
        float4 _4055 = 0.0f.xxxx;
        uint _4061 = 0u;
        float _4063 = 0.0f;
        _4052 = _369;
        _4055 = _367;
        _4061 = 0u;
        _4063 = 0.0f;
        bool4 _4053 = bool4(false, false, false, false);
        float4 _4056 = 0.0f.xxxx;
        bool _4058 = false;
        float _4060 = 0.0f;
        float _4064 = 0.0f;
        bool4 _4123 = bool4(false, false, false, false);
        float4 _4124 = 0.0f.xxxx;
        bool _4125 = false;
        bool _4057 = false;
        float _4059 = 1.0f;
        [loop]
        for (;;)
        {
            if (_4061 < 12u)
            {
                float2 _4068 = _4050.xy;
                float2 _4069 = _4048.xy;
                float _4070 = float(_4061);
                float _4071 = _4070 + 1.0f;
                float _4074 = _4050.z;
                float _4075 = _4048.z;
                float4 _4077 = 0.0f.xxxx;
                _4077.x = mad(_4071, _4075, _4074);
                float _4078 = _4070 + 2.0f;
                _4077.y = mad(_4078, _4075, _4074);
                float _4083 = _4070 + 3.0f;
                _4077.z = mad(_4083, _4075, _4074);
                float _4088 = _4070 + 4.0f;
                _4077.w = mad(_4088, _4075, _4074);
                float _4093 = mad(0.666666686534881591796875f, _799, _4059);
                _4060 = mad(0.666666686534881591796875f, _799, _4093);
                float4 _4097 = 0.0f.xxxx;
                _4097.x = TranslucentBasePass_HZBTexture.SampleLevel(TranslucentBasePass_HZBSampler, _4068 + (_4069 * _4071), _4059).x;
                _4097.y = TranslucentBasePass_HZBTexture.SampleLevel(TranslucentBasePass_HZBSampler, _4068 + (_4069 * _4078), _4059).x;
                _4097.z = TranslucentBasePass_HZBTexture.SampleLevel(TranslucentBasePass_HZBSampler, _4068 + (_4069 * _4083), _4093).x;
                _4097.w = TranslucentBasePass_HZBTexture.SampleLevel(TranslucentBasePass_HZBSampler, _4068 + (_4069 * _4088), _4093).x;
                _4056 = _4077 - _4097;
                float4 _4110 = _4047.xxxx;
                float4 _4112 = abs(_4056 + _4110);
                _4053 = bool4(_4112.x < _4110.x, _4112.y < _4110.y, _4112.z < _4110.z, _4112.w < _4110.w);
                _4058 = (((_4057 || _4053.x) || _4053.y) || _4053.z) || _4053.w;
                [branch]
                if (_4058 || false)
                {
                    _4123 = _4053;
                    _4124 = _4056;
                    _4125 = _4058;
                    break;
                }
                _4064 = _4056.w;
                _4052 = _4053;
                _4055 = _4056;
                _4057 = _4058;
                _4059 = _4060;
                _4061 += 4u;
                _4063 = _4064;
                continue;
            }
            else
            {
                _4123 = _4052;
                _4124 = _4055;
                _4125 = _4057;
                break;
            }
        }
        float3 _4164 = 0.0f.xxx;
        [branch]
        if (_4125)
        {
            float _4138 = 0.0f;
            [flatten]
            if (_4123.z)
            {
                _4138 = _4124.y;
            }
            else
            {
                _4138 = _4124.z;
            }
            float _4146 = 0.0f;
            float _4147 = 0.0f;
            [flatten]
            if (_4123.y)
            {
                _4146 = _4124.y;
                _4147 = _4124.x;
            }
            else
            {
                _4146 = _4123.z ? _4124.z : _4124.w;
                _4147 = _4138;
            }
            float _4153 = 0.0f;
            [flatten]
            if (_4123.x)
            {
                _4153 = _4124.x;
            }
            else
            {
                _4153 = _4146;
            }
            float _4154 = _4123.x ? _4063 : _4147;
            _4164 = _4050 + (_4048 * (((_4123.x ? 0.0f : (_4123.y ? 1.0f : (_4123.z ? 2.0f : 3.0f))) + float(_4061)) + clamp(_4154 / (_4154 - _4153), 0.0f, 1.0f)));
        }
        else
        {
            _4164 = _4050 + (_4048 * float(_4061));
        }
        float3 _4236 = 0.0f.xxx;
        [branch]
        if (_4125)
        {
            float2 _4178 = (mad(mad((_4164.xy * TranslucentBasePass_TranslucentBasePass_HZBUvFactorAndInvFactor.zw).xy, float2(2.0f, -2.0f), float2(-1.0f, 1.0f)).xy, View_View_ScreenPositionScaleBias.xy, View_View_ScreenPositionScaleBias.wz).xy - View_View_ScreenPositionScaleBias.wz) / View_View_ScreenPositionScaleBias.xy;
            float4 _4185 = mul(float4(_4178, _4164.z, 1.0f), View_View_ClipToPrevClip);
            float2 _4189 = _4185.xy / _4185.w.xx;
            float2 _4196 = clamp((abs(_4178) * 5.0f) - 4.0f.xx, 0.0f.xx, 1.0f.xx);
            float2 _4203 = clamp((abs(_4189) * 5.0f) - 4.0f.xx, 0.0f.xx, 1.0f.xx);
            float3 _4220 = -min(-TranslucentBasePass_PrevSceneColor.SampleLevel(TranslucentBasePass_PrevSceneColorSampler, clamp(mad(_4189, TranslucentBasePass_TranslucentBasePass_PrevScreenPositionScaleBias.xy, TranslucentBasePass_TranslucentBasePass_PrevScreenPositionScaleBias.zw), TranslucentBasePass_TranslucentBasePass_PrevSceneColorBilinearUVMin, TranslucentBasePass_TranslucentBasePass_PrevSceneColorBilinearUVMax), 0.0f).xyz, 0.0f.xxx);
            float4 _4221 = float4(_4220.x, _4220.y, _4220.z, _367.w);
            _4221.w = 1.0f;
            float4 _4226 = _4221 * (min(clamp(1.0f - dot(_4196, _4196), 0.0f, 1.0f), clamp(1.0f - dot(_4203, _4203), 0.0f, 1.0f)) * clamp(mad(-6.599999904632568359375f, _799, 2.0f), 0.0f, 1.0f));
            _4236 = (_3930 * (1.0f - _4226.w)) + (_4226.xyz * TranslucentBasePass_TranslucentBasePass_PrevSceneColorPreExposureInv).xyz;
        }
        else
        {
            _4236 = _3930;
        }
        _4237 = _4236;
    }
    else
    {
        _4237 = _3930;
    }
    float3 _4362 = 0.0f.xxx;
    [branch]
    if (abs(dot(TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ReflectionPlane.xyz, 1.0f.xxx)) > 9.9999997473787516355514526367188e-05f)
    {
        float3 _4263 = _490 - TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionOrigin.xyz;
        float _4296 = 1.0f - clamp((_799 - 0.20000000298023223876953125f) * 10.0f, 0.0f, 1.0f);
        float _4298 = (((1.0f - clamp(mad(abs(dot(TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ReflectionPlane, float4(_490, -1.0f))), TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters.x, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters.y), 0.0f, 1.0f)) * (clamp((TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionXAxis.w - abs(dot(_4263, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionXAxis.xyz))) * TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters.x, 0.0f, 1.0f) * clamp((TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionYAxis.w - abs(dot(_4263, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionYAxis.xyz))) * TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters.x, 0.0f, 1.0f))) * clamp(mad(dot(TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ReflectionPlane.xyz, _601), TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters2.x, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters2.y), 0.0f, 1.0f)) * _4296;
        float4 _4356 = 0.0f.xxxx;
        [branch]
        if (_4298 > 0.0f)
        {
            float4 _4326 = mul(float4(mul(float4(_490 + (reflect(reflect(normalize(_490 - View_View_TranslatedWorldCameraOrigin), -TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ReflectionPlane.xyz), mul(_601, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_InverseTransposeMirrorMatrix).xyz) * TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionParameters.z), 1.0f), View_View_TranslatedWorldToView).xyz, 1.0f), TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_ProjectionWithExtraFOV[View_View_StereoPassIndex]);
            uint _4333 = 0u;
            if (TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_bIsStereo != 0u)
            {
                _4333 = uint(View_View_StereoPassIndex);
            }
            else
            {
                _4333 = 0u;
            }
            float4 _4349 = TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionTexture.SampleLevel(TranslucentBasePass_Shared_Reflection_ReflectionCubemapSampler, mad(clamp(_4326.xy / _4326.w.xx, -TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenBound, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenBound), TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenScaleBias[_4333].xy, TranslucentBasePass_TranslucentBasePass_Shared_PlanarReflection_PlanarReflectionScreenScaleBias[_4333].zw), 0.0f);
            float3 _4353 = _4349.xyz * _4296;
            float4 _4354 = float4(_4353.x, _4353.y, _4353.z, 0.0f.xxxx.w);
            _4354.w = _4298 * _4349.w;
            _4356 = _4354;
        }
        else
        {
            _4356 = 0.0f.xxxx;
        }
        _4362 = _4356.xyz + (_4237 * (1.0f - _4356.w));
    }
    else
    {
        _4362 = _4237;
    }
    float4 _4364 = (float4(-1.0f, -0.0274999998509883880615234375f, -0.572000026702880859375f, 0.02199999988079071044921875f) * _799) + float4(1.0f, 0.0425000004470348358154296875f, 1.03999996185302734375f, -0.039999999105930328369140625f);
    float _4365 = _4364.x;
    float2 _4374 = (float2(-1.03999996185302734375f, 1.03999996185302734375f) * mad(min(_4365 * _4365, exp2((-9.27999973297119140625f) * clamp(dot(_601, _507), 0.0f, 1.0f))), _4365, _4364.y)) + _4364.zw;
    bool _4401 = TranslucentBasePass_TranslucentBasePass_Shared_Fog_ApplyVolumetricFog > 0.0f;
    float4 _4484 = 0.0f.xxxx;
    if (_4401)
    {
        float4 _4419 = mul(((float4(View_View_ViewTilePosition, 0.0f) + float4(_461, 0.0f)) * 2097152.0f) + float4(_491, 1.0f), View_View_RelativeWorldToClip);
        float _4420 = _4419.w;
        float4 _4466 = 0.0f.xxxx;
        float _4467 = 0.0f;
        if (_4401)
        {
            float4 _4460 = TranslucentBasePass_Shared_Fog_IntegratedLightScattering.SampleLevel(View_SharedBilinearClampedSampler, min(float3(mad((_4419.xy / _4420.xx).xy, float2(0.5f, -0.5f), 0.5f.xx), (log2(mad(_4420, View_View_VolumetricFogGridZParams.x, View_View_VolumetricFogGridZParams.y)) * View_View_VolumetricFogGridZParams.z) * View_View_VolumetricFogInvGridSize.z) * float3(View_View_VolumetricFogScreenToResourceUV, 1.0f), float3(View_View_VolumetricFogUVMax, 1.0f)), 0.0f);
            float3 _4464 = _4460.xyz * View_View_OneOverPreExposure;
            _4466 = float4(_4464.x, _4464.y, _4464.z, _4460.w);
            _4467 = TranslucentBasePass_TranslucentBasePass_Shared_Fog_VolumetricFogStartDistance;
        }
        else
        {
            _4466 = float4(0.0f, 0.0f, 0.0f, 1.0f);
            _4467 = 0.0f;
        }
        float4 _4472 = lerp(float4(0.0f, 0.0f, 0.0f, 1.0f), _4466, clamp((_493 - _4467) * 100000000.0f, 0.0f, 1.0f).xxxx);
        float _4475 = _4472.w;
        _4484 = float4(_4472.xyz + (in_var_TEXCOORD7.xyz * _4475), _4475 * in_var_TEXCOORD7.w);
    }
    else
    {
        _4484 = in_var_TEXCOORD7;
    }
    float3 _4491 = max(lerp(0.0f.xxx, Material_Material_PreshaderBuffer[2].xyz, Material_Material_PreshaderBuffer[1].w.xxx), 0.0f.xxx);
    float _4576 = 0.0f;
    float3 _4577 = 0.0f.xxx;
    [branch]
    if (View_View_OutOfBoundsMask > 0.0f)
    {
        uint _4517 = _508 + 31u;
        float3 _4526 = abs(((View_View_ViewTilePosition - Scene_GPUScene_GPUScenePrimitiveSceneData[_508 + 1u].xyz) * 2097152.0f) + (_491 - Scene_GPUScene_GPUScenePrimitiveSceneData[_508 + 18u].xyz));
        float3 _4527 = float3(Scene_GPUScene_GPUScenePrimitiveSceneData[_508 + 17u].w, Scene_GPUScene_GPUScenePrimitiveSceneData[_508 + 24u].w, Scene_GPUScene_GPUScenePrimitiveSceneData[_508 + 25u].w) + 1.0f.xxx;
        float _4574 = 0.0f;
        float3 _4575 = 0.0f.xxx;
        if (any(bool3(_4526.x > _4527.x, _4526.y > _4527.y, _4526.z > _4527.z)))
        {
            float3 _4554 = View_View_ViewTilePosition * 0.57700002193450927734375f.xxx;
            float3 _4555 = _491 * 0.57700002193450927734375f.xxx;
            float3 _4570 = frac(mad((_4555.x + _4555.y) + _4555.z, 0.00200000009499490261077880859375f, frac(((_4554.x + _4554.y) + _4554.z) * 4194.30419921875f))).xxx;
            _4574 = 1.0f;
            _4575 = lerp(float3(1.0f, 1.0f, 0.0f), float3(0.0f, 1.0f, 1.0f), float3(bool3(_4570.x > 0.5f.xxx.x, _4570.y > 0.5f.xxx.y, _4570.z > 0.5f.xxx.z)));
        }
        else
        {
            float _4552 = 0.0f;
            float3 _4553 = 0.0f.xxx;
            if (Scene_GPUScene_GPUScenePrimitiveSceneData[_4517].x > 0.0f)
            {
                float3 _4537 = abs(_490 - in_var_TEXCOORD9);
                float _4547 = 1.0f - clamp(abs(max(_4537.x, max(_4537.y, _4537.z)) - Scene_GPUScene_GPUScenePrimitiveSceneData[_4517].x) * 20.0f, 0.0f, 1.0f);
                _4552 = float(int(sign(_4547)));
                _4553 = float3(1.0f, 0.0f, 1.0f) * _4547;
            }
            else
            {
                _4552 = 1.0f;
                _4553 = _4491;
            }
            _4574 = _4552;
            _4575 = _4553;
        }
        _4576 = _4574;
        _4577 = _4575;
    }
    else
    {
        _4576 = 1.0f;
        _4577 = _4491;
    }
    float4 _4587 = float4(((mad(_1009 * _827, max(1.0f.xxx, ((((((_793 * 2.040400028228759765625f) - 0.3323999941349029541015625f.xxx) * 1.0f) + ((_793 * (-4.79510021209716796875f)) + 0.6417000293731689453125f.xxx)) * 1.0f) + ((_793 * 2.755199909210205078125f) + 0.69029998779296875f.xxx)) * 1.0f), lerp(mad((_4362 * ((_829 * _4374.x) + (clamp(50.0f * _829.y, 0.0f, 1.0f) * _4374.y).xxx)) * 1.0f, max(1.0f.xxx, ((((((_829 * 2.040400028228759765625f) - 0.3323999941349029541015625f.xxx) * 1.0f) + ((_829 * (-4.79510021209716796875f)) + 0.6417000293731689453125f.xxx)) * 1.0f) + ((_829 * 2.755199909210205078125f) + 0.69029998779296875f.xxx)) * 1.0f), float4(_2595.x ? 0.0f.xxxx.x : _2150.x, _2595.y ? 0.0f.xxxx.y : _2150.y, _2595.z ? 0.0f.xxxx.z : _2150.z, _2595.w ? 0.0f.xxxx.w : _2150.w).xyz + float4(_2595.x ? 0.0f.xxxx.x : _2153.x, _2595.y ? 0.0f.xxxx.y : _2153.y, _2595.z ? 0.0f.xxxx.z : _2153.z, _2595.w ? 0.0f.xxxx.w : _2153.w).xyz), _827 + (_829 * 0.449999988079071044921875f), View_View_UnlitViewmodeMask.xxx)) + _4577) * _4484.w) + _4484.xyz, _4576);
    float3 _4593 = min((_4587.xyz * View_View_PreExposure).xyz, 32256.0f.xxx);
    out_var_SV_Target0 = float4(_4593.x, _4593.y, _4593.z, _4587.w);
}

SPIRV_Cross_Output main(SPIRV_Cross_Input stage_input)
{
    gl_FragCoord = stage_input.gl_FragCoord;
    gl_FragCoord.w = 1.0 / gl_FragCoord.w;
    gl_FrontFacing = stage_input.gl_FrontFacing;
    in_var_TEXCOORD10_centroid = stage_input.in_var_TEXCOORD10_centroid;
    in_var_TEXCOORD11_centroid = stage_input.in_var_TEXCOORD11_centroid;
    in_var_TEXCOORD0 = stage_input.in_var_TEXCOORD0;
    in_var_TEXCOORD4 = stage_input.in_var_TEXCOORD4;
    in_var_PRIMITIVE_ID = stage_input.in_var_PRIMITIVE_ID;
    in_var_LIGHTMAP_ID = stage_input.in_var_LIGHTMAP_ID;
    in_var_TEXCOORD7 = stage_input.in_var_TEXCOORD7;
    in_var_TEXCOORD9 = stage_input.in_var_TEXCOORD9;
    frag_main();
    SPIRV_Cross_Output stage_output;
    stage_output.out_var_SV_Target0 = out_var_SV_Target0;
    return stage_output;
}
